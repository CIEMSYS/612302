//----------------------------------ciemsys-------------------------------------
/*
 * File:        Nombre.c
 * ProjectName: NombreProyecto
 * Course:      Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:       Tema - Subtema
 * Processor:   PIC18F45K22
 * Compiler:    XC8, Ver.
 * Author:      Sebasti�n Fernando Puente Reyes, M.Sc.
 * E-mail:      sebastian.puente@unillanos.edu.co
 * Date:        Mes de A�o
 */
//----------------------------------ciemsys-------------------------------------
/*
 * REQUERIMIENTO
 * 
 * Ver Descripcion.txt
 */
//----------------------------------ciemsys-------------------------------------

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>

/**
 * Secci�n: Definiciones (macros)
 */

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * Secci�n: Variables Globales
 */

/**
 * Secci�n: Funci�n Principal (main)
 */

void main(void)
{
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

//----------------------------------ciemsys-------------------------------------


/* *****************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Profesor: Sebastian Puente Reyes, M.Sc.
 * - CIEMSYS -
 * - Grupo de Estudio en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 * *****************************************************************************
 */