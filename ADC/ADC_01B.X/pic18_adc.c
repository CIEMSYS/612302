
/**
  Section: Included Files
*/

#include "pic18_adc.h"

/**
  Section: ADC Module APIs
*/
//******************************************************************************
void ADC_Initialize(void)
{
    uint8_t FrecOsc;
    FrecOsc = (uint8_t)OSCCONbits.IRCF;
    
    //ADCS<2:0>: A/D Conversion Clock Select bits
    if(FrecOsc == 0b00000111) //Si FOSC = 16 MHz
    {
        ADCON2bits.ADCS = 0b101; //TAD = 16/FOSC = 1.0 useg
    }
    
    /* 
     * ACQT<2:0>: A/D Acquisition time select bits
     * Acquisition time is the duration that the A/D charge holding capacitor
     * remains connected to A/D channel from the instant the GO/DONE bit
     * is set until conversions begins.
     */
    ADCON2bits.ACQT = 0b010; //ACQT = 4*TAD
    
    //ADFM: A/D Conversion Result Format Select bit
    ADCON2bits.ADFM = 1; //Right justified
    
    //PVCFG<1:0>: Positive Voltage Reference Configuration bits
    ADCON1bits.PVCFG = 0b00; //A/D VREF+ connected to internal signal, AVDD
    
    //NVCFG<1:0>: Negative Voltage Reference Configuration bits
    ADCON1bits.NVCFG = 0b00; //A/D VREF- connected to internal signal, AVSS  
   
    //CHS<4:0>: Analog Channel Select bits
    ADCON0bits.CHS = 0b00001; //Canal AN1 seleccinado
    
    //ADON: ADC Enable bit
    ADCON0bits.ADON = 1; //ADC is enabled
    
    //Inicialización de los resgistros que almacenan el resultado
    ADRESL = 0x00;
    ADRESH = 0x00; 
}
//******************************************************************************
void ADC_SelectChannel(uint8_t Channel)
{
    // Seleccionar el canal del ADC
    ADCON0bits.CHS = Channel;
}
//******************************************************************************
void ADC_StartConversion(void)
{
    // Iniciar la conversión
    ADCON0bits.GO_nDONE = 1;
}
//******************************************************************************
bool ADC_IsConversionDone(void)
{
    return (!ADCON0bits.GO_nDONE);
}
//******************************************************************************
uint16_t ADC_GetConversionResult(void)
{
    // Conversion finalizada, retornar el resultado
    return ((ADRESH << 8) + ADRESL);
}
//******************************************************************************
uint16_t ADC_GetConversion(uint8_t Channel)
{
    // Seleccionar el canal del ADC
    ADCON0bits.CHS = Channel;

    // Encender el módulo ADC
    ADCON0bits.ADON = 1;

    // Iniciar la conversión
    ADCON0bits.GO_nDONE = 1;

    // Esperar a que termine la conversión
    while (ADCON0bits.GO_nDONE)
    {
    }
    // Conversión finalizada, retornar el resultado
    return ((ADRESH << 8) + ADRESL);
}
//******************************************************************************
void ADC_On(void)
{
    //ADON: ADC Enable bit
    ADCON0bits.ADON = 1; // ADC is enabled
}

void ADC_Off(void)
{
    //ADON: ADC Enable bit
    ADCON0bits.ADON = 0; // ADC is disabled and consumes no operating current
}
//******************************************************************************