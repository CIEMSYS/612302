
/**
 * Secci�n: Archivos Incluidos
 */

#include "SystemInitialize.h"

/**
 * Secci�n: Desarrollo de Funciones
 */

void SysInitialize(void)
{
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b111; //HFINTOSC = 16 MHz, Fosc = 16 MHz
    
    //--A.2--Habilitaci�n PLL
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports
    DISPLAY_LAT = 0; //Inicializar PORT para el display
    DISPLAY_EN_LAT = 0; //Inicializar PORT para las l�neas de activaci�n de displays

    //--B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
    //(Seccion 10.7 Datasheet: Port Analog Control)
    //ANSELX: PORTX Analog Select Register
    //S�lo es necesaria cuando se desee configurar dichos pines como entradas digitales

    //--B.3--Sentido I/O Ports
    DISPLAY_TRIS = 0x00; //l�neas para los 7 segementos como salidas
    DISPLAY_EN_TRIS &= 0b11000011; //RC<5:2> como salidas (l�neas activaci�n displays)
    
    TRISAbits.TRISA1 = 1; //RA1(AN1) como entrada
    ANSELAbits.ANSA1 = 1; //Buffer de entrada digital deshabilitado

    return;
}

void PrintDisplay(uint16_t Valor)
{
    static uint8_t u8_Unidades = 0;
    static uint8_t u8_Decenas = 0;
    static uint8_t u8_Centenas = 0;
    static uint8_t u8_UniMil = 0;
    
    u8_UniMil = Valor/1000;
    u8_Centenas = (Valor/100)%10;
    u8_Decenas = (Valor/10)%10;
    u8_Unidades = (Valor/1)%10;
    
    //Aprox. se muestra un valor en los display por medio segundo
    for(uint8_t u8_n = 0; u8_n < 31; u8_n++)
    {
        ENDISP_UNIMIL = 1;
        ENDISP_CENTENAS = 0;
        ENDISP_DECENAS = 0;
        ENDISP_UNIDADES = 0;
        DISP(u8_UniMil);
        __delay_ms(4); 
    
        ENDISP_UNIMIL = 0;
        ENDISP_CENTENAS = 1;
        ENDISP_DECENAS = 0;
        ENDISP_UNIDADES = 0;
        DISP(u8_Centenas);
        __delay_ms(4);
    
        ENDISP_UNIMIL = 0;
        ENDISP_CENTENAS = 0;
        ENDISP_DECENAS = 1;
        ENDISP_UNIDADES = 0;
        DISP(u8_Decenas);
        __delay_ms(4);
    
        ENDISP_UNIMIL = 0;
        ENDISP_CENTENAS = 0;
        ENDISP_DECENAS = 0;
        ENDISP_UNIDADES = 1;
        DISP(u8_Unidades);
        __delay_ms(4);  
    }

    return;
}