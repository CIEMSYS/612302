/* 
 * File:   SystemInitialize.h
 * Author: Lenovo
 *
 * Created on 4 de abril de 2017, 11:20 PM
 */

#ifndef SYSTEMINITIALIZE_H
#define	SYSTEMINITIALIZE_H

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>
#include <stdint.h>
#include "ConfigurationBits.h"
#include "pic18_adc.h"

/**
 * Secci�n: Macros
 */

#define FOSC_16_MHZ (16000000)
#define _XTAL_FREQ (FOSC_16_MHZ)

#define DISPLAY         (LATD)
#define DISPLAY_TRIS    (TRISD)
#define DISPLAY_LAT     (LATD)
#define DISPLAY_EN_LAT  (LATC)
#define DISPLAY_EN_TRIS (TRISC)

#define ENDISP_UNIDADES (LATCbits.LATC5) //Enable display unidades
#define ENDISP_DECENAS  (LATCbits.LATC4) //Enable display decenas
#define ENDISP_CENTENAS (LATCbits.LATC3) //Enable display centenas
#define ENDISP_UNIMIL   (LATCbits.LATC2) //Enable display unidades de mil

#define DISP(x)         (DISPLAY = u8_Disp7SegCC[x])
#define DISPLAY_OFF()   (DISPLAY_EN_LAT &= 0b11000011) 

/**
 * Arreglo para el manejo de un display 7 segmentos de c�todo com�n
 */
uint8_t u8_Disp7SegCC[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

/**
 * Secci�n: Prototipos de funciones
 */

/**
 * @Resumen
 *  A. Configuraci�n Oscilador
 *  B. Configuraci�n Puertos I/O
 */
void SysInitialize(void);

/**
 * 
 * @param Valor
 */
void PrintDisplay(uint16_t Valor);

#endif	/* SYSTEMINITIALIZE_H */

