/*******************************************************************************
 * FileName:        adc_01b.c
 * ProjectName:     ADC_01B
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           ADC Module - ADC Test
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.05
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Marzo de 2019
 *******************************************************************************
 * REQUERIMIENTO
 * -Ver Descripcion.txt
 ******************************************************************************/

/**
 * Secci�n: Archivos incluidos
 */

#include "SystemInitialize.h"

/**
 * Secci�n: Variables globales
 */

/**
 * Funci�n Principal
 */
void main(void)
{
    //Variable para almacenar los 10 bits de la conversi�n
    uint16_t ResultadoConversion = 0; 
    
    //--Configutaciones Iniciales
    
    SysInitialize();
    ADC_Initialize(); //Inicializaci�n del ADC
    DISPLAY_OFF(); //Displays desactivados
    
    //--Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1)
    {
        ResultadoConversion = ADC_GetConversion(1);
        PrintDisplay(ResultadoConversion);
    }
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebast�n Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/