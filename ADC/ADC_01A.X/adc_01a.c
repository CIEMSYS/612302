/*******************************************************************************
 * FileName:        adc_01a.c
 * ProjectName:     ADC_01A
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           ADC Module - ADC Test
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.05
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Marzo de 2019
 *******************************************************************************
 * REQUERIMIENTO
 * -Ver Descripcion.txt
 *******************************************************************************
 */

/**
 * Secci�n: Archivos incluidos
 */

#include <xc.h>
#include <stdint.h> //Lib est�ndar para enteros
#include "ConfigurationBits.h" //Bits de configuraci�n

/**
 * Secci�n: Macros
 */

#define _XTAL_FREQ (16000000) //Macro necesario para __delay_ms(), etc

#define DISPLAY         (LATD)
#define DISPLAY_TRIS    (TRISD)
#define DISPLAY_LAT     (LATD)
#define DISPLAY_EN_LAT  (LATC)
#define DISPLAY_EN_TRIS (TRISC)

#define ENDISP_UNIDADES (LATCbits.LATC5) //Enable display unidades
#define ENDISP_DECENAS  (LATCbits.LATC4) //Enable display decenas
#define ENDISP_CENTENAS (LATCbits.LATC3) //Enable display centenas
#define ENDISP_UNIMIL   (LATCbits.LATC2) //Enable display unidades de mil

#define DISP(x)         (DISPLAY = u8_Disp7SegCC[x])
#define DISPLAY_OFF()   (DISPLAY_EN_LAT &= 0b11000011)   

/**
 * Secci�n: Prototipos de funciones
 */

/**
 * @Resumen
 *  A. Configuraci�n oscilador interno
 *  B. Configuraci�n Puertos I/O
 */
void SetUp(void);

/**
 * @Resumen
 *  Configuraci�n inicial del m�dulo ADC
 */
void ADC_Initialize(void);

/**
 * 
 * @param Valor
 */
void PrintDisplay(uint16_t Valor);

/**
 * Secci�n: Variables globales
 */

/**
 * Arreglo para el manejo de un display 7 segmentos de c�todo com�n
 */
uint8_t u8_Disp7SegCC[] = {0x3F, 0x06, 0x5B, 0x4F, 0x66, 0x6D, 0x7D, 0x07, 0x7F, 0x6F};

/**
 * Funci�n Principal
 */
void main(void)
{
    //Variable para almacenar los 10 bits de la conversi�n
    uint16_t ResultadoConversion = 0; 
    
    //--Configutaciones Iniciales
    
    SetUp();
    ADC_Initialize(); //Inicialic�zaci�n del ADC
    DISPLAY_OFF(); //Displays desactivados
    
    //--Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while (1)
    {
        ADCON0bits.GO_nDONE = 1; //Iniciar conversion (AN1)
        // Wait for the conversion to finish
        while (ADCON0bits.GO_nDONE)
        {
        }
        ResultadoConversion = ((uint16_t)ADRESH << 8) | ADRESL;
        PrintDisplay(ResultadoConversion);
    }
}

/**
 * Secci�n: Desarrollo de funciones
 */

void ADC_Initialize(void)
{
    //ADCON2: A/D CONTROL REGISTER 2
    //ADCS<2:0>: A/D Conversion Clock Select bits
    ADCON2bits.ADCS = 0b101; //TAD = 16/FOSC = 1 useg
    //ACQT<2:0>: A/D Acquisition time select bits
    ADCON2bits.ACQT = 0b010; //ACQT = 4*TAD = 4 useg
    //ADFM: A/D Conversion Result Format Select bit
    ADCON2bits.ADFM = 1; //Right justified
    
    //ADCON1: A/D CONTROL REGISTER 1
    //PVCFG<1:0>: Positive Voltage Reference Configuration bits
    ADCON1bits.PVCFG = 0b00; //A/D VREF+ connected to internal signal, AVDD
    //NVCFG<1:0>: Negative Voltage Reference Configuration bits
    ADCON1bits.NVCFG = 0b00; //A/D VREF- connected to internal signal, AVSS
    
    //ADCON0: A/D CONTROL REGISTER 0
    //CHS<4:0>: Analog Channel Select bits
    ADCON0bits.CHS = 0b00001; //Canal AN1 seleccinado
    //ADON: ADC Enable bit
    ADCON0bits.ADON = 1; //ADC is enabled
    
    //Inicializaci�n de los resgistros que almacenan el resultado
    ADRESL = 0x00;
    ADRESH = 0x00; 
    
    return;
}

void SetUp(void)
{
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b111; //HFINTOSC = 16 MHz, Fosc = 16 MHz
    
    //--A.2--Habilitaci�n PLL
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports
    DISPLAY_LAT = 0; //Inicializar PORT para el display
    DISPLAY_EN_LAT = 0; //Inicializar PORT para las l�neas de activaci�n de displays

    //--B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
    //(Seccion 10.7 Datasheet: Port Analog Control)
    //ANSELX: PORTX Analog Select Register
    //S�lo es necesaria cuando se desee configurar dichos pines como entradas digitales

    //--B.3--Sentido I/O Ports
    DISPLAY_TRIS = 0x00; //l�neas para los 7 segementos como salidas
    DISPLAY_EN_TRIS &= 0b11000011; //RC<5:2> como salidas (l�neas activaci�n displays)
    
    TRISAbits.TRISA1 = 1; //RA1(AN1) como entrada
    ANSELAbits.ANSA1 = 1; //Buffer de entrada digital deshabilitado

    return;
}

void PrintDisplay(uint16_t Valor)
{
    static uint8_t u8_Unidades = 0;
    static uint8_t u8_Decenas = 0;
    static uint8_t u8_Centenas = 0;
    static uint8_t u8_UniMil = 0;
    
    u8_UniMil = Valor/1000;
    u8_Centenas = (Valor/100)%10;
    u8_Decenas = (Valor/10)%10;
    u8_Unidades = (Valor/1)%10;
    
    //Aprox. se muestra un valor en los display por medio segundo
    for(uint8_t u8_n = 0; u8_n < 31; u8_n++)
    {
        ENDISP_UNIMIL = 1;
        ENDISP_CENTENAS = 0;
        ENDISP_DECENAS = 0;
        ENDISP_UNIDADES = 0;
        DISP(u8_UniMil);
        __delay_ms(4); 
    
        ENDISP_UNIMIL = 0;
        ENDISP_CENTENAS = 1;
        ENDISP_DECENAS = 0;
        ENDISP_UNIDADES = 0;
        DISP(u8_Centenas);
        __delay_ms(4);
    
        ENDISP_UNIMIL = 0;
        ENDISP_CENTENAS = 0;
        ENDISP_DECENAS = 1;
        ENDISP_UNIDADES = 0;
        DISP(u8_Decenas);
        __delay_ms(4);
    
        ENDISP_UNIMIL = 0;
        ENDISP_CENTENAS = 0;
        ENDISP_DECENAS = 0;
        ENDISP_UNIDADES = 1;
        DISP(u8_Unidades);
        __delay_ms(4);  
    }

    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebast�n Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/