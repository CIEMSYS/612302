/* 
 * File:   pic18_lcd.h
 * Author: Sebasti�n Puente Reyes <sebastian.puente@unillanos.edu.co>
 *
 * Created on 25 de febrero de 2019, 11:59 AM
 */

#ifndef PIC18_LCD_H
#define	PIC18_LCD_H

#include "pic18_chip.h"


//Macros para los pines de conexión de la LCD
#define LCD_RD7     LATDbits.LATD7       // D7
#define TRISRD7     TRISDbits.TRISD7

#define LCD_RD6     LATDbits.LATD6       // D6
#define TRISRD6     TRISDbits.TRISD6

#define LCD_RD5     LATDbits.LATD5       // D5
#define TRISRD5     TRISDbits.TRISD5

#define LCD_RD4     LATDbits.LATD4       // D4
#define TRISRD4     TRISDbits.TRISD4

#define LCD_EN      LATEbits.LATE0       // EN
#define TRISEN      TRISEbits.TRISE0

#define LCD_RS      LATEbits.LATE1       // RS
#define TRISRS      TRISEbits.TRISE1

//Macros para los comandos disponibles
#define      LCD_FIRST_ROW           128
#define      LCD_SECOND_ROW          192
#define      LCD_THIRD_ROW           148
#define      LCD_FOURTH_ROW          212
#define      LCD_CLEAR               1
#define      LCD_RETURN_HOME         2
#define      LCD_CURSOR_OFF          12
#define      LCD_BLINK_CURSOR_OFF    14
#define      LCD_BLINK_CURSOR_ON     15
#define      LCD_MOVE_CURSOR_LEFT    16
#define      LCD_MOVE_CURSOR_RIGHT   20
#define      LCD_TURN_OFF            0
#define      LCD_TURN_ON             8
#define      LCD_SHIFT_LEFT          24
#define      LCD_SHIFT_RIGHT         28

//******************************************************************************
//Prototipos de funciones
void Lcd_Init(void);
void Lcd_Out(unsigned char y, unsigned char x, const char *buffer);
void Lcd_Out2(unsigned char y, unsigned char x, char *buffer);
void Lcd_Chr_CP(char data);
void Lcd_Cmd(unsigned char data);
//******************************************************************************

#endif	/* PIC18_LCD_H */