/* 
 * File:   pic18_adc.h
 * Author: Sebastián F. Puente Reyes
 *
 * Created on 27 de febrero de 2019, 08:40 PM
 */

#ifndef PIC18_ADC_H
#define	PIC18_ADC_H

/**
  Section: Included Files
*/

#include <xc.h>
#include <stdint.h>

/**
  Section: ADC Module APIs
*/
void ADC_Initialize(void);
void ADC_SelectChannel(uint8_t channel);
void ADC_StartConversion(void);
bool ADC_IsConversionDone(void);
uint16_t ADC_GetConversionResult(void);
uint16_t ADC_GetConversion(uint8_t channel);


#endif	/* PIC18_ADC_H */

