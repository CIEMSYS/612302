/* 
 * File:   pic18_adc.c
 * Author: Sebastián F. Puente Reyes
 *
 * Created on 28 de febrero de 2019, 01:28 PM
 */

/**
  Section: Included Files
*/
#include "pic18_adc.h"


/**
  Section: ADC Module APIs
*/
void ADC_Initialize(void)
{
    //A/D Conversion Clock Select bits
    ADCON2bits.ADCS = 0b101; // TAD = 16/FOSC => Si FOSC = 11 useg
    
    //A/D Acquisition time select bits.
    /*Acquisition time is the duration that the A/D charge holding capacitor
     * remains connected to A/D channel from the instant the GO/DONE bit
     * is set until conversions begins.
     */
    ADCON2bits.ACQT = 0b010; //ACQT = 4*TAD = 4 useg
    
    /*A/D Conversion Result Format Select bit
     * 1 = Right justified
     * 0 = Left justified
     */
    ADCON2bits.ADFM = 1; //Right justified
    
    
}
