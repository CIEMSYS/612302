/**
  EEPROM Lab Source File

  Company:
    Microchip Technology Inc.

  File Name:
    EEPROM.c

  Summary:
    This is the source file for the EEPROM lab

  Description:
    This source file contains the code on how the EEPROM lab works.
   Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.65
        Device            :  PIC18F45K22
        Driver Version    :  2.00
    The generated drivers are tested against the following:
        Compiler          :  XC8 v1.45
        MPLAB             :  MPLAB X v4.15
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
 */

#include "../../mcc_generated_files/memory.h"
#include "../../mcc_generated_files/pin_manager.h"
#include "../../mcc_generated_files/adc.h"
#include "../../mcc_generated_files/interrupt_manager.h"
#include "../../labs.h"
#include "../../lcd.h"


/**
  Section: Local Function Prototypes
 */
void Lab_EEPROM_ISR(void);

/**
  Section: Macro Declaration
 */
#define EEPROM_ADDR    0x7F00      // EEPROM starting address

/**
  Section: Local Variable Declarations
 */
uint16_t adcResult;

/*
                             Application    
 */
void EEPROM(void) {
    if (labState == NOT_RUNNING) {
        LEDs_SetLow();

        INTERRUPT_GlobalInterruptEnable();
        INTERRUPT_PeripheralInterruptEnable();
        INTERRUPT_InterruptOnChangeEnable();
        INTERRUPT_IOCPositiveEnable();

        INT0_SetInterruptHandler(Lab_EEPROM_ISR);

        LCD_GoTo(0, 0);
        LCD_WriteString("     EEPROM        ");

        adcResult = 0;

        labState = RUNNING;
    }

    if (labState == RUNNING) {
        uint8_t readData;
        
        // Display the stored data from the EEPROM onto the LEDs (MSB = LED_D8, LSB = LED_D6)   
        readData = DATAEE_ReadByte(EEPROM_ADDR);
        LEDs = readData << 1;

        // Display the stored data from the EEPROM onto the LCD
        LCD_GoTo(1, 0);
        LCD_WriteString("   Value =     ");
        LCD_GoTo(1, 11);
        LCD_WriteByte((readData) % 10 + '0');
    }

    if (switchEvent) {
        INTERRUPT_TMR0InterruptDisable();
        INTERRUPT_InterruptOnChangeDisable();
        INTERRUPT_IOCPositiveDisable();

        INTERRUPT_GlobalInterruptDisable();
        INTERRUPT_PeripheralInterruptDisable();

        labState = NOT_RUNNING;
    }

}

void Lab_EEPROM_ISR(void) {
    // Debounce by waiting and seeing if S1 is still held down
    __delay_ms(20);

    // Get the top 3 MSBs and display it in the LEDs
    adcResult = ADC_GetConversion(POT_CHANNEL) >> 7;
    LEDs = adcResult << 1;

    // Save the value to EEPROM and wait for the next IOC event
    DATAEE_WriteByte(EEPROM_ADDR, adcResult);

}
/**
 End of File
 */