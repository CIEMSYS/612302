/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 15 de junio de 2018, 11:56 PM
 */


#include <xc.h>
#include "ConfigurationBits.h"
#define _XTAL_FREQ 16000000

#define ON 1
#define OFF 0
#define LED2 PORTAbits.RA4
#define LED3 PORTAbits.RA5

void main(void)
{
    OSCCONbits.IRCF = 0b111; //Oscilador interno 16 MHz
    LATC = 0;
    TRISCbits.TRISC5 = 1; //RC5 como entrada
    ANSELCbits.ANSC5 = 0; //Digital input buffer enabled RC5
    LATA = 0;
    TRISAbits.TRISA4 = 0; //RA4 como salida
    TRISAbits.TRISA5 = 0; //RA5 como salida
    ANSELAbits.ANSA5 = 0;
    LED3 = OFF;
    
    while(1)
    {
        if(PORTCbits.RC5 == 0)
            LED2 = ON;
        else
            LED2 = OFF;
        
        LED3 = ~LED3;
        __delay_ms(1000);
        
    }
    return;
}
















