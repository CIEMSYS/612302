/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 21 de abril de 2017, 09:00 AM
 */

//******************************************************************************
//NOTA
//Para este ejemplo se trabaja con el oscilador interno a 8 MHz
//Ver el archivo de cabecera ConfigBits.h
//******************************************************************************

//Direcyivas de Preprocesamiento
#include <xc.h> //Libreria necesaria para el PIC escogido
#include "ConfigBits.h" //Artchivo de cabecera con los Bits de Configuraci�n

//Prototipos de funciones
void SetUp(void); //Prototipo para la funci�n SetUp()

/*
 * Funci�n Prinicipal
 */
void main(void)
{
    SetUp(); //Se llama a la funci�n SetUp() encargada de las configuraciones iniciales
    
    while(1) //Bucle infinito
    {
        PORTAbits.RA1 = PORTAbits.RA0;  //Lo que tenga el pin RA0 es sacado por la l�nea RA1;
    }
}

/*
 * FUNCI�N:     SetUp()
 * INPUTS:      None (no se reciben argumentos o par�metros)
 * OUTPUTS:     None (no se retorna ning�n valor)
 * DESCRIPCI�N: Configuraci�n Inicial necesaria
 */
void SetUp(void)
{
    /*
     * RCF2:IRCF0 (OSCCON Register): Bits de selecci�n de la Frecuencia del Oscilador Interno
     * 111 = 8 MHz (INTOSC drives clock directly)
     * 110 = 4 MHz
     * 101 = 2 MHz
     * 100 = 1 MHz(3)
     * 011 = 500 kHz
     * 010 = 250 kHz
     * 001 = 125 kHz
     * 000 = 31 kHz (from either INTOSC/256 or INTRC directly)(2)
     */
    OSCCONbits.IRCF = 0b111;    //Se escogen los 8 Mhz del oscilador interno como frecuencia de trabajo del MCU
    ADCON1bits.PCFG = 0b1111;   //Todos los pines asociados como canales anal�gicos son configurados como digitales
    
    //Configuraci�n Puertos (Capitulo 10 I/O Ports DataSheet)
    TRISAbits.TRISA0 = 1;   //L�nea RA0 como entrada
    TRISAbits.TRISA1 = 0;   //L�nea RA1 como salida
}