/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     LCD_03 
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F4550
 * Compiler:        XC8
 * Version:         1.42
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Mayo de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 8 MHz.
 *
 * 
 ******************************************************************************/
/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "BitsConfiguracion.h"
#include "lcd.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
//Macros LED
#define LEDTris TRISEbits.TRISE0
#define LED     PORTEbits.RE0
//Macros Botones A y B
#define BotonATris  TRISDbits.TRISD2
#define BotonA      PORTDbits.RD2
#define BotonBTris  TRISDbits.TRISD3
#define BotonB      PORTDbits.RD3
#define Oprimido    0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void Tarea1(void);
void Tarea2(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    char Mensaje1[] = "Valor: ";
    char Mensaje2[] = "A->++  B->--";
    char Valor = 0;
    
    SetUp();
    
    Lcd_Init(); 				//Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF);    //Se apaga el cursor
    Lcd_Out(1,0,Mensaje1);
    Lcd_Out(2,0,Mensaje2);
    
    while(1) //Ciclo infinito
    {

    }
    return;
}

/*******************************************************************************
 * FUNCTION:    SetUp()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz

    //Puertos
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    LEDTris = 0;
    BotonATris = 1; //L�nea de entrada para el Boton A
    BotonBTris = 1; //L�nea de entrada para el Boton B
    
    return;
}

/*******************************************************************************
 * FUNCTION:    Tarea1()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: 
 ******************************************************************************/
void Tarea1(void)
{
    char Mensaje[] = "Opcion A: ";
    
    Lcd_Cmd(LCD_CLEAR);
    Lcd_Out(1,0,Mensaje);
}




/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/