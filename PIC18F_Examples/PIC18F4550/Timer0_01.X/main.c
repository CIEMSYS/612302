/*******************************************************************************
 * FileName:         main.c
 * ProjectName:
 * Dependencies:     See INCLUDES section below
 * Processor:        PIC18F4550
 * Compiler:         XC8
 * Version:          1.42
 * Author:           Sebasti�n Fernando Puente Reyes
 * e-mail:           sebastian.puente@unillanos.edu.co
 * Date:             Mayo de 2017
 */
/*******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 4 MHz.
 * Se genera una se�al cuadrada por la l�nea RB0 con un periodo (T) de 800 mseg
 * con un ciclo de trabajo del 75%. Es decir, la se�al se mantiene en estado
 * ALTO por 600 mseg(0.75*800) y luego pasa a estado BAJO por 200 mseg(0.25*800)
 */

//NOTA: Ver el archivo llamado Descripci�n.txt

#include <xc.h>
#include "ConfigBits.h"

//Prototipos de funciones
void SetUp(void);
void SetupTimer0(void);
void delay_5ms(void);

//Funci�n principal
void main(void)
{   
    SetUp();
    SetupTimer0(); //Se configura el modulo Timer0

    //Generaci�n de la se�al cuadrada con un periodo de 10 mseg por la l�nea RB0
    while(1)
    {
        //Se pone en alto la l�nea RB0 durante 5 mseg
        PORTBbits.RB0 = 1;
        delay_5ms();
        //Se pone en bajo la l�nea RB0 durante 5 mseg
        PORTBbits.RB0 = 0;
        delay_5ms();
    }
}

//Funci�n para las configuraciones iniciales generales
void SetUp(void)
{
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital
    return;
}

//Funci�n para la confguraci�n del modulo Timer0
void SetupTimer0(void)
{
    T0CONbits.T0PS = 0b100; //Pre-escalar = 32
    T0CONbits.PSA = 0; //Pre-escalar activado
    T0CONbits.T0CS = 0; //Modo temporizador seleccionado
    T0CONbits.T08BIT = 1; //Modo 8 bits
    return;
}

//Funci�n que hace uso del Timer0 para generar un retardo de 5 mseg
void delay_5ms(void)
{
    TMR0 = 100; //Valor inicial del registro TMR0
    INTCONbits.TMR0IF = 0; //Borramos el bit que nos indica el desbordamiento
    T0CONbits.TMR0ON = 1; //Iniciamos el modulo Timer0
    /* Mientras el bit TMR0IF sea 0 significa que a�n no se desborda el TMR0.
     * Luego, debemos esperar a que se ponga en 1 (es decir que pasen los 5ms */
    while(INTCONbits.TMR0IF == 0);
    T0CONbits.TMR0ON = 0; //Desactivamos el modulo Timer0
    return;
}