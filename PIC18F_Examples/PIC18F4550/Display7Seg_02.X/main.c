/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 21 de marzo de 2015, 05:41 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"

//------------------------------------------------------------------------------
//Macros
#define _XTAL_FREQ 8000000 //Macro para el correcto funcionamiento del retardo __delay_ms(). Se pone la frecuencia de trabajo
#define TRIS_ENABLE_UNIDADES TRISEbits.RE0 //TRIS para la l�nea digital que habilita el Display de Unidades
#define TRIS_ENABLE_DECENAS TRISEbits.RE1 //TRIS para la l�nea digital que habilita el Display de Decenas
#define DISP_UNIDADES PORTEbits.RE0 //Macro para el display de unidades
#define DISP_DECENAS PORTEbits.RE1 //Macro para el display de decenas

#define DISP(x) PORTD = Disp7SegCC[x] //Macro para sacar un digito por el puerto D

//------------------------------------------------------------------------------
//Prototipos de funciones
void SetUp(); //Configuraciones iniciales
void PrintTwoDisp(unsigned char Valor); //Visualizaci�n de Valor en dos display 7 segmentos

//------------------------------------------------------------------------------
//Variables globales
//Arreglo para el manejo de un display 7 segmentos de c�todo com�n
//                    0    1    2    3    4    5    6    7    8    9
char Disp7SegCC[] = {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};

//------------------------------------------------------------------------------
//Funci�n principal
void main(void)
{
    unsigned char Fecha[] = {20,3,84}; //Areglo tipo char de 3 posiciones
    unsigned char i;

    SetUp(); //Llamado a la funci�n SetUp para la config. inicial

    while(1) //Bucle infinito
    {
        for(i = 0; i < 3; i++) //Bucle para direccionar el arreglo Fecha[]
        {
            //Visualizaci�n de un n�mero (0-99) en dos display 7 seg multiplexados
            PrintTwoDisp(Fecha[i]);
        }
    }
}

//------------------------------------------------------------------------------
//Desarrollo funci�n SetUp()
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados

    //Configuraci�n puertos E/S
    TRIS_ENABLE_UNIDADES = 0; //Establecer como salida la l�nea digital que habilita el Display de Unidades
    TRIS_ENABLE_DECENAS = 0; //Establecer como salida la l�nea digital que habilita el Display de Decenas
    TRISD = 0x00; //Todas las l�neas del puerto D como salidas para los 7 segmentos

    return;
}

//------------------------------------------------------------------------------
//Desarrollo funci�n PrintTwoDisp(unsigned char Valor)
//Esta funci�n se encarga de visualizar en dos display 7 segmentos, multiplexados
//en el puerto D, un valor n�merico de 0 a 99 el cual se encuentra
//en el parametro tipo char 'Valor'.
void PrintTwoDisp(unsigned char Valor)
{
    unsigned char Unidades = 0;
    unsigned char Decenas = 0;

    if(Valor > 9)
    {
        Unidades = Valor%10;
        Decenas = Valor/10;
    }
    else
    {
        Decenas = 0;
        Unidades = Valor;
    }

    //Aprox. se muestra un valor en los dos display por medio segundo
    for(char n = 0; n < 31; n++)
    {
        DISP_UNIDADES = 1; //Se activa el display de unidades
        DISP_DECENAS = 0; //Se desactiva el display de decenas
        DISP(Unidades); //Se visualizan las unidades
        __delay_ms(8); //delay 8 ms

        DISP_UNIDADES = 0; //Se desactiva display de unidades
        DISP_DECENAS = 1; //Se activa display de decenas
        DISP(Decenas); //Se visualizan las decenas
        __delay_ms(8); //delay 8 ms
    }
    return;
}