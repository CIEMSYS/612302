/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     Timer2_01
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F4550
 * Compiler:        XC8
 * Version:         1.42
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Junio de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 4 MHz.
 * 
 * 
 *
 ******************************************************************************/
/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "BitsConfiguracion.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define ONDA PORTBbits.RB0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void SetUpTimer0(void);
void SetUpTimer2(void);
void Delay2ms(void);
void Delay8ms(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();
    SetUpTimer0();
    SetUpTimer2();

    while(1) //Ciclo infinito
    {
        ONDA = 1;
        Delay2ms();
        ONDA = 0;
        Delay8ms();
    }
}

/*******************************************************************************
 * FUNCTION:	SetUp()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    LATB = 0;
    PORTB = 0;
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital
    return;
}

/*******************************************************************************
 * FUNCTION:	SetUpTimer0()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n m�dulo Timer0
 ******************************************************************************/
void SetUpTimer0(void)
{
    T0CONbits.T0PS = 0b011; //Pre-escalar = 16, Tiempo Incremento = 16 useg
    T0CONbits.PSA = 0;      //Pre-escalar activado
    T0CONbits.T0CS = 0;     //Modo temporizador seleccionado
    T0CONbits.T08BIT = 1;   //Modo 8 bits
    return;
}

/*******************************************************************************
 * FUNCTION:	SetUpTimer2()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n m�dulo Timer2
 ******************************************************************************/
void SetUpTimer2(void)
{
	//Time = (PR2+1)(4)(Tosc)(Prescaler)(Postcaler)
	//Se desea una temporizaci�n de 8 mseg, Prescaler = 16, Postcaler = 2
	//Se despeja PR2, PR2 = 249
    T2CONbits.T2CKPS = 0b11;    //Prescaler = 16
    T2CONbits.TOUTPS = 0b0001;  //Postcaler = 2
    T2CONbits.TMR2ON = 1;       //Timer2 On
    PR2 = 249;
    return;
}

/*******************************************************************************
 * FUNCTION:	Delay2ms()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Retardo de 2 mseg a trav�s del Timer0
 ******************************************************************************/
void Delay2ms(void)
{
    TMR0 = 131; //Valor inicial del registro TMR0
    INTCONbits.TMR0IF = 0; //Borramos el bit que nos indica el desbordamiento
    /* Mientras el bit TMR0IF sea 0 significa que a�n no se desborda el TMR0.
     * Luego, debemos esperar a que se ponga en 1 (es decir que pasen los 5ms */
    while(INTCONbits.TMR0IF == 0);
    return;
}

/*******************************************************************************
 * FUNCTION:	Delay8ms()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Retardo de 8 mseg a trav�s del Timer2
 ******************************************************************************/
void Delay8ms(void)
{
    TMR2 = 0;
    PIR1bits.TMR2IF = 0;
    while(PIR1bits.TMR2IF == 0);
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/