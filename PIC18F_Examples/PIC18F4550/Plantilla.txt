/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     Timer0_02 
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F4550
 * Compiler:        XC8
 * Version:         1.42
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Mayo de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 4 MHz.
 * Se genera una se�al cuadrada por la l�nea RB0 con un periodo (T) de 800 mseg
 * con un ciclo de trabajo del 75%. Es decir, la se�al se mantiene en estado
 * ALTO por 600 mseg(0.75*800) y luego pasa a estado BAJO por 200 mseg(0.25*800)
 ******************************************************************************/
/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();

    while(1) //Ciclo infinito
    {

    }
    return;
}

/*******************************************************************************
 * FUNCTION:	SetUp()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/