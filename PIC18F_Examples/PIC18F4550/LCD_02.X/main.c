/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 16 de noviembre de 2014, 12:10 AM
 */

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <stdio.h>
#include <math.h>
#include "BitsConfiguracion.h"
#include "lcd.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define _XTAL_FREQ 8000000

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void delayms(unsigned int milisegundos);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
char Texto[10];
char Mensaje[] = "C. Digitales II";
float i = 2.345;

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();                    //Configuraci�n inicial
    Lcd_Init(); 				//Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF);    //Se apaga el cursor
    
    sprintf(Texto, "r %.1f = %f\n", i, sqrt(i));
    Lcd_Out(1,0,Texto);     //Imprimir un string en la l�nea 1 desde la posici�n 0
    Lcd_Out(2,0,Mensaje);   //Imprimir un string en la l�nea 2 desde la posici�n 0

    while(1)
    {
        PORTBbits.RB0 = 1;
        delayms(1000);
        PORTBbits.RB0 = 0;
        delayms(1000);
    }
}

////////////////////////////////////////////////////////////////////////////////
void SetUp()
 {
    OSCCONbits.IRCF = 0b111;    //Oscilador interno a 8 MHz
    
    ADCON1bits.PCFG = 0b1111;   //Canales anal�gicos desactivados 
    TRISBbits.TRISB0 = 0;       //L�nea RB0 como salida dgital
    
    return;
 }
////////////////////////////////////////////////////////////////////////////////
void delayms(unsigned int milisegundos)
{
    int i = 0;
    for(i = 0; i < milisegundos/10; i++)
    {
        __delay_ms(10);
    }
}