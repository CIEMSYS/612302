/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 2 de mayo de 2014, 07:54 PM
 */
 //MICROCONTROLADOR: PIC16F84A
 

//Librerias
#include <xc.h>
#include "BitsConfiguracion.h"

//Macros
#define _XTAL_FREQ 4000000  //macro necesario para la funci�n __delay_ms

//Variables globales
unsigned char cuenta = 0;   //variable global

//Funci�n principal
void main()
{
    //Configuraci�n puertos
    TRISA = 0b00000;   //L�neas del puerto A como salidas digitales

    //Configuraci�n interrrupciones
    INTCONbits.INTE = 1;    //habilitamos la interrupcion externa por el pin RB0/INT
    OPTION_REGbits.INTEDG = 1;  //la interrupcion externa ser� por flanco de subida
    INTCONbits.RBIE = 1;    //habilitamos la interrupcion por el puerto B (cualquier cambio en las l�nea RB4:RB7)
    INTCONbits.GIE = 1; //habilitamos globalmente las interrupciones

    while(1)    //ciclo infinito
    {
        PORTA = cuenta;     //sacamos por el puerto A lo que tiene la variable cuenta
        cuenta++;           //incrementamos la variable cuenta
        __delay_ms(500);    //retardo de 500 ms (medio segundo)
    }
}

//Subrutina o funci�n para atender la interrupciones
//Esta subrutina es la que se ejecutar� cuando ocurran los eventos de las interrupciones habilitadas
//Luego, dentro de esta subrutina se debe averiguar c�al de las fuentes de interrupci�n fue la que gener� la interrupci�n
void interrupt AtencionInterrupciones()
{
    if(INTCONbits.INTF == 1)    //�la causante de la interrupcion fue la interrupcion externa por RB0/INT?
    {
        cuenta = 0;
        INTCONbits.INTF = 0;    //borramos el bit bandera para que pueda ponerse en alto posteriormente indicando una futura interrupci�n
    }

    if(INTCONbits.RBIF == 1)    //�la causante de la interrupci�n fue la interrupci�n por el puerto B (RB4:RB7)?
    {
        cuenta = 0xFF;
        PORTB = PORTB;          //es importante saber que, para que la interrrupci�n por el puerto B funcione correctamente, se debe
                                //realizar una lectura del registro PORTB
        INTCONbits.RBIF = 0;    //borramos el bit bandera para que pueda ponerse en alto posteriormente indicando una futura interrupci�n
    }
    else
        NOP();
}
////////////////////////////////////////////////////////////////////////////////
