/* 
 * File:   BitsConfiguracion.h
 * Author: Sebastian Puente R
 *
 * Created on 2 de mayo de 2014, 07:52 PM
 */

#ifndef BITSCONFIGURACION_H
#define	BITSCONFIGURACION_H

#include <xc.h>

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (Power-up Timer is enabled)
#pragma config CP = OFF         // Code Protection bit (Code protection disabled)

#endif	/* BITSCONFIGURACION_H */

