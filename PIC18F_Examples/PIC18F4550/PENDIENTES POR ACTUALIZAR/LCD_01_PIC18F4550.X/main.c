/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 18 de octubre de 2013, 05:47 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"
#include "DriveLCD.h" //Libreria para el manejo de la LCD

#define _XTAL_FREQ 4000000 //4 Mhz CristaL

////////////////////////////////////////////////////////////////////////////////
void IniciaLCD(void);
void ms_delay(unsigned int milisegundos);

////////////////////////////////////////////////////////////////////////////////
// Principal
void main(void)
 {
    ADCON1 = 0x0F;
    TRISBbits.TRISB0 = 0;

    char Mensaje[] = "LCD en XC8";

    IniciaLCD();
    putsXLCD(Mensaje);  while(BusyXLCD());
    SetDDRamAddr(64);   while(BusyXLCD());
    putsXLCD("Sebas Puente R.");

    while(1)
    {
        PORTBbits.RB0 = 1;
        ms_delay(1000);
        PORTBbits.RB0 = 0;
        ms_delay(1000);
    }
}

////////////////////////////////////////////////////////////////////////////////
void IniciaLCD()
 {
	OpenXLCD(FOUR_BIT & LINES_5X7); while( BusyXLCD() );
        WriteCmdXLCD(BLINK_OFF & CURSOR_OFF); while( BusyXLCD() );
        WriteCmdXLCD(SHIFT_DISP_LEFT); while( BusyXLCD() );
 }
////////////////////////////////////////////////////////////////////////////////
void ms_delay(unsigned int milisegundos)
{
    int i = 0;
    for(i = 0; i < milisegundos/10; i++)
    {
        __delay_ms(10);
    }
}

////////////////////////////////////////////////////////////////////////////////
//RETARDOS PARA LA LIBRERIA MYLCD
////////////////////////////////////////////////////////////////////////////////
void DelayFor18TCY(void)
{
    _delay(18);
    return;
}
////////////////////////////////////////////////////////////////////////////////
void DelayPORXLCD(void)
{
    __delay_ms(15);
    return;
}
////////////////////////////////////////////////////////////////////////////////
void DelayXLCD(void)
{
    __delay_ms(5);
    return;
}
////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

