/* 
 * File:   BitsDeConfiguracion.h
 * Author: Sebastian Puente R
 *
 * Created on 2 de abril de 2014, 01:24 PM
 */

#ifndef BITSDECONFIGURACION_H
#define	BITSDECONFIGURACION_H

#include <xc.h>

//Bits de Configuración
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer (WDT disabled)
#pragma config PWRTE = ON       // Power-up Timer Enable bit (Power-up Timer is enabled)
#pragma config CP = OFF         // Code Protection bit (Code protection disabled)

#endif	/* BITSDECONFIGURACION_H */

