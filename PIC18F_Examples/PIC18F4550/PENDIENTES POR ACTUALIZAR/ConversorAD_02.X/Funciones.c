/*
 * File:   Funciones.c
 * Author: Sebastian Puente R
 *
 * Created on 5 de octubre de 2014, 12:38 AM
 */

/******************************************************************************/
/* Archivos incluidos                                                         */
/******************************************************************************/

#include <xc.h>
#include "Funciones.h"

/******************************************************************************/
/* Funciones de Usuario                                                       */
/******************************************************************************/

void SetUp(void)
{
    /* En las siguientes l�neas se visualizaran los 10 bits de la conversion
     * AD */
    TRISD = 0;                  //L�neas puerto D como salidas digitales
    TRISEbits.TRISE0 = 0;       //L�nea RE0 como salida digital
    TRISEbits.TRISE1 = 0;       //L�nea RE1 como salida digital

    /* L�nea RB0 como entrada. En esa l�nea se conectar� un interruptor para la
     * selecci�n del canal a convertir.
     * Si RB0 = 0, se debe hacer la conversi�n del canal AN0
     * Si RB0 = 1, se debe hacer la conversi�n del canal AN1 */
    TRISBbits.RB0 = 1;

    return;
}

/******************************************************************************/
void ConfigurarADC(void)
{
    /* Configuraci�n canales anal�gicos a utilizar
     * Se trabajar� con los canales AN0(pin 2) y AN1(pin 3) los demas canales
     * quedan deshabilitados y se podran trabajar como digitales */
    ADCON1bits.PCFG = 0b1101;

    /* Configuraci�n  tensi�n de referencia
     * Vref- = Vss, Vref+ = se conecta en la l�nea f�sica RA3 (pin 5)
     * para este ejemplo conectamos 3V */
    ADCON1bits.VCFG = 0b01; 

    /* Configuraci�n reloj de conversi�n TAD
     * TOSC = 1/10000000 = 0.1 useg (TAD debe ser m�nimo 0.8us)
     * TAD = 8*TOSC = 0,8 useg */
    ADCON2bits.ADCS = 0b001;

    /* Configuraci�n del tiempo de adquisici�n
     * Seg�n Datasheet (pag. 407 Tabla 28-29) TACQ min debe ser 1,4us.
     * Seg�n la p�gina 272 del datasheet, en la ecuaci�n 21-3 se calcula un
     * TACQ m�nimo de 2,45 useg
     * De acuerdo a lo anterior entonces escogemos TACQ = 4*TAD = 3.2 useg */
    ADCON2bits.ACQT = 0b010;

    /* Configuraci�n del modo de almacenamiento de la conversi�n.
     * Justificado a derechas */
    ADCON2bits.ADFM = 1;

    //Activaci�n del conversor
    ADCON0bits.ADON = 1;

    return;
}
/******************************************************************************/
//Sebasti�n F. Puente Reyes, M.Sc.
//Programaci�n en C de Microcontroladores PIC usando MPLAB XC8
/******************************************************************************/