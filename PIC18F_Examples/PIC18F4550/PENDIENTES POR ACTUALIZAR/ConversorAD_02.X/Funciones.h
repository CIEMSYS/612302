/* 
 * File:   Funciones.h
 * Author: Sebastian Puente R
 *
 * Created on 5 de octubre de 2014, 12:38 AM
 */

/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/
#define _XTAL_FREQ 10000000
#define SW1 PORTBbits.RB0
#define IniciarConversion()   ADCON0bits.GO = 1
//SW1 hace referencia al switch conectado en la línea RB0

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/
void SetUp(void);
void ConfigurarADC(void);

/******************************************************************************/
//Sebastián F. Puente Reyes, M.Sc.
//Programación en C de Microcontroladores PIC usando MPLAB XC8
/******************************************************************************/