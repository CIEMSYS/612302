/*
 * File:   ConversorAD_02.c
 * Author: Sebastian Puente R
 * e-mail: sebastian.puente@unillanos.edu.co
 * Universidad de los Llanos
 * Villavicencio-Meta, Colombia
 * Created on 2 de octubre de 2014, 03:53 PM
 */

#include <xc.h>
#include "Funciones.h"

/*******************************************************************************
 * Función Principal
 ******************************************************************************/
void main(void)
{
    //Variable entera para guardar el resultado de la conversión A/D
    unsigned int ResultadoConversion = 0;

    //Se llama a la función SetUp();
    SetUp();
    //Se llama la función SetUp_ADC(); la cual configura el Conversor AD
    ConfigurarADC();

    while(1) //Bucle infinito
    {
        //Selección del canal a convertir
        switch(SW1)
        {
            case 0:
                //Se selecciona el canal AN0 para la conversión A/D
                ADCON0bits.CHS = 0;
                break;
            case 1:
                //Se selecciona el canal AN0 para la conversión A/D
                ADCON0bits.CHS = 1;
                break;
            default:
                break;
        }
        
        //Inicio de la conversión
        //ADCON0bits.GO = 1; //Conversión A/D en marcha
        IniciarConversion();

        //Bucle de espera de final de la conversión A/D
        //Comprobación del bit GO/DONE hasta que se ponga a '0'
        while(ADCON0bits.DONE);

        //Lectura resultado de la conversión de los resgistros ADRESH y ADRESL
        ResultadoConversion = (((unsigned int)ADRESH)<<8)|(ADRESL);

        //Visualización de los 10 bits del resultado de la conversión
        PORTD = ResultadoConversion;
        PORTE = ResultadoConversion >> 8;
    }

    return;
}
/*******************************************************************************
 * Sebastián F. Puente Reyes, M.Sc.
 * Programación en C de Microcontroladores PIC usando MPLAB XC8
 ******************************************************************************/