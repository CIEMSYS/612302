/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 7 de octubre de 2013, 09:20 AM
 */

#include <xc.h>
#include "BitsConfigurations.h" //FOSC = 48 Mhz usando un cristal de 4Mhz y la PLL

char Resultado = 0xF0;

void main(void)
{
    ADCON1 = 0x0F; //Puertos asociados al conversor A/D como digitales

    //Configuración Interrupción por cambio del PORTB (RB7 - RB4)
    RCONbits.IPEN = 0;      //Sistema de prioridad de interrupciones deshabilitado
    INTCONbits.GIE = 1;     //Interrupciones habilitadas a nivel global
    INTCONbits.RBIE = 1;    //Interrupcion por cambio en el Puerto B (PORTB<7:4>) habilitada

    //Configuración puertos
    INTCON2bits.RBPU = 0;   //Usamos las resistencias pull-ups del PORTB
    TRISB = 0xFF;           //Puerto B como entrada
    TRISD = 0x00;           //Puerto D como salida

    while(1)
    {
        PORTD = Resultado;
    }
}

//Función para atender las interrupciones. En este caso solo hay una fuente de interrupción posible
void interrupt interrupciones()
{
    if(INTCONbits.RBIF == 1)    //Se establece si la interrupción por el cambio del PORTB (PORTB<7:4>) causó la interrupción
    {
        PORTB = PORTB;          //Ver DataSheet, donde se establece la necesidad de acceder al  PORTB
        INTCONbits.RBIF = 0;    //Se borra el flag de interrupción
        Resultado = Resultado + 1;
    }
    else
        PORTD = 0xFF;
}
