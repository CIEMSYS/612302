/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 19 de noviembre de 2014, 11:52 AM
 */

//Librerias
#include <xc.h>
#include <stdio.h>
#include "ConfigBits.h"
#include "DriveLCD.h"
#include <plib/timers.h>

//Macros
#define _XTAL_FREQ 4000000 //4 Mhz CristaL
#define SEL PORTBbits.RB7

//Prototipos de funciones
void Setup(void);

//Variables globales
bit Ciclos = 0;
unsigned int ValorTimer0 = 0;
float Frecuencia = 0;

char Mensaje1[] = "*Frecuencimetro*";
char Mensaje2[20];

/*******************************************************************************
 * Función principal
 ******************************************************************************/
void main(void)
{
    Setup();

    while(1)
    {
        Frecuencia = 1000000.0/(ValorTimer0);
        
        if(SEL == 0)
            sprintf(Mensaje2, "F(Hz) = %.3f", Frecuencia);

        else
            sprintf(Mensaje2, "T(us) = %.8i", ValorTimer0);

        SetDDRamAddr(0); while(BusyXLCD());
        putsXLCD(Mensaje1); while(BusyXLCD());
        SetDDRamAddr(64); while(BusyXLCD());
        putsXLCD(Mensaje2); while(BusyXLCD());
    }

    return;
}

/*******************************************************************************
 * Función Setup
 ******************************************************************************/
void Setup()
{
    //Configuración oscilador interno
    OSCCONbits.IRCF = 0b110; //Fosc = 4 MHz

    //Configuración puertos
    ADCON1bits.PCFG = 0b1111; //Canales analógicos desactivados
    TRISBbits.TRISB7 = 1; //Línea RB7 como entrada digital

    //Configuración Interrupciones
    RCONbits.IPEN = 0; //Sistema de prioridad de interrupciones deshabilitado
    INTCONbits.INT0IE = 1; //Interrupcion externa por cambio en el Puerto B (PORTB<7:4>) habilitada
    INTCON2bits.INTEDG0 = 1; //La interrupción externa 0 se dispara por flanco de subida
    INTCONbits.GIE = 1; //Interrupciones habilitadas a nivel global

    //Configuración Timer0
    OpenTimer0(TIMER_INT_OFF & T0_16BIT & T0_SOURCE_INT & T0_PS_1_1);

    //Configuración LCD
    OpenXLCD(FOUR_BIT & LINES_5X7);
    WriteCmdXLCD(BLINK_OFF & CURSOR_OFF);
    WriteCmdXLCD(SHIFT_DISP_LEFT);

    return;
}

/******************************************************************************
 * Función para el manejo de las interrupciones habilitadas
 * Las interrupciones habilitadas son las siguientes:
 * - Interrupción externa 0
 ******************************************************************************/
void interrupt AtencionInterrupciones()
{
    //Atención a la interrupción externa 0

    if(Ciclos == 0)
    {
        WriteTimer0(0);
        Ciclos = 1;
    }
    
    else if(Ciclos == 1)
    {
        ValorTimer0 = ReadTimer0();
        Ciclos = 0;
    }

    INTCONbits.INT0IF = 0;
}
//******************************************************************************