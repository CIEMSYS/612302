/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 9 de octubre de 2014, 11:01 AM
 */

#include <xc.h>
#include <plib/adc.h>

/*Variable global donde se alamacenará el resultado de la conversión AD*/
int ConversionADC;

/*******************************************************************************
 * Función Principal
 ******************************************************************************/
void main(void)
{
    /* Configuración puertos digitales */
    TRISD = 0x00;
    TRISEbits.TRISE0 = 0;
    TRISEbits.TRISE1 =0;
    TRISBbits.TRISB0 = 1;

    /* Configuración conversor AD. Ver Descripción.txt */
    OpenADC(ADC_FOSC_8 & ADC_RIGHT_JUST & ADC_4_TAD,
            ADC_CH0 & ADC_INT_OFF & ADC_REF_VREFPLUS_VSS,
            ADC_2ANA);
    
    /* Bucle Infinito */
    while(1)
    {
        switch(PORTBbits.RB0)
        {
            case 0:
                SelChanConvADC(ADC_CH0);
                break;
            case 1:
                SelChanConvADC(ADC_CH1);
                break;
            default:
                break;
        }
        while(BusyADC());
        ConversionADC = ReadADC();

        /* Visualización de los 10 bits de la conversión */
        PORTD = ConversionADC;
        PORTEbits.RE0 = ConversionADC >> 8;
        PORTEbits.RE1 = ConversionADC >> 9;
    }
    return;
}
/*******************************************************************************
 * Sebastián F. Puente Reyes, M.Sc.
 * Programación en C de Microcontroladores PIC usando MPLAB XC8
 ******************************************************************************/