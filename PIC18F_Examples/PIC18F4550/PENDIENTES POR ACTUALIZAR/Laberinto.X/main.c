/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 25 de septiembre de 2014, 12:08 AM
 */

#include <xc.h>
#include "Fuses.h"
#include "DriveLCD.h"

////////////////////////////////////////////////////////////////////////////////
//Macros
#define CLEAR_LCD       WriteCmdXLCD(0b00000001)
#define BUSY_LCD        while(BusyXLCD())
#define LINEA_01_LCD    SetDDRamAddr(0)
#define LINEA_02_LCD    SetDDRamAddr(64)
#define ALARMA          PORTBbits.RB1
#define BOTON           PORTBbits.RB0
#define _XTAL_FREQ 4000000 //4 Mhz FOSC

////////////////////////////////////////////////////////////////////////////////
//Variables globales
const char TextA_P1[] = "Que es";
const char TextB_P1[] = "genetica?";
const char TextA_P2[] = "Que es";
const char TextB_P2[] = "un genotipo?";
const char TextA_P3[] = "Que es";
const char TextB_P3[] = "un fenotipo?";
const char TextA_P4[] = "Que es";
const char TextB_P4[] = "un gen?";
const char TextA_P5[] = "Que raza";
const char TextB_P5[] = "es aa?";
const char TextA_P6[] = "Que raza";
const char TextB_P6[] = "es AA?";
const char TextA_P7[] = "Nombre";
const char TextB_P7[] = "las leyes";
const char TextA_P8[] = "Haga";
const char TextB_P8[] = "un ejemplo";
const char TextTiempo[] = "    Tiempo!    ";

const char Mensaje1[] = "** BIENVENIDO **";
const char Mensaje2[] = "****************";
const char Mensaje3[] = "Responda las";
const char Mensaje4[] = "sgtes preguntas";
const char Mensaje5[] = "Elaborado por:";
const char Mensaje6[] = "   Soltelect   ";
const char Mensaje7[] = "   3108825296   ";
const char Mensaje8[] = "Intellectus sas";
const char Mensaje9[] = "   3106963192   ";
const char Mensaje10[] = "Sebastian Puente";
const char Mensaje11[] = "   3004302995   ";


////////////////////////////////////////////////////////////////////////////////
//Prototipos de funciones
void VerPregunta1();
void VerPregunta2();
void VerPregunta3();
void VerPregunta3();
void VerPregunta4();
void VerPregunta5();
void VerPregunta6();
void VerPregunta7();
void VerPregunta8();
void SelPregunta(char n);
void delayms(int time_ms);
void IniciaLCD();
void MensajeInicio();
void Creditos();

////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void main(void)
{
    OSCCONbits.IRCF = 0b110;    //Oscilador interno a 4 Mhz
    ADCON1bits.PCFG = 0b1111;   //Canales anal�gicos deshabilitados
    IniciaLCD();
    MensajeInicio();
    TRISBbits.TRISB0 = 1;       //L�nea RB0 para conectar el boton
    TRISBbits.TRISB1 = 0;       //L�nra RB1 para conectar el zumbador

    unsigned char p = 0;

    while(1)
    {
        if(BOTON == 1)
        {
            p = p + 1;
            if (p == 9)
            {
                p = 1;
            }
            ALARMA = 1;
            delayms(300);
            ALARMA = 0;
            SelPregunta(p);
            delayms(10000);
            CLEAR_LCD; BUSY_LCD;
            putrsXLCD(TextTiempo); BUSY_LCD;
            delayms(1500);
            if (p == 8)
            {
                Creditos();
                MensajeInicio();
            }
        }
    }
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void SelPregunta(char n)
{
    switch(n)
    {
        case 1:
            VerPregunta1();
            break;
        case 2:
            VerPregunta2();
            break;
        case 3:
            VerPregunta3();
            break;
        case 4:
            VerPregunta4();
            break;
        case 5:
            VerPregunta5();
            break;
        case 6:
            VerPregunta6();
            break;
        case 7:
            VerPregunta7();
            break;
        default:
            VerPregunta8();
            break;
    }
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta1()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P1); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P1); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta2()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P2); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P2); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta3()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P3); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P3); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta4()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P4); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P4); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta5()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P5); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P5); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta6()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P6); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P6); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta7()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P7); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P7); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void VerPregunta8()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(TextA_P8); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(TextB_P8); BUSY_LCD;
    return;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void delayms(int time_ms)
{
    for(int n = 0; n < time_ms; n++)
    {
        __delay_ms(1);
    }
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void IniciaLCD()
{
    OpenXLCD(FOUR_BIT & LINES_5X7); while( BusyXLCD() );
    WriteCmdXLCD(BLINK_OFF & CURSOR_OFF); while( BusyXLCD() );
    WriteCmdXLCD(SHIFT_DISP_LEFT); while( BusyXLCD() );
 }
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void MensajeInicio()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(Mensaje1); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(Mensaje2); BUSY_LCD;
    delayms(2000);
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(Mensaje3); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(Mensaje4); BUSY_LCD;
}
////////////////////////////////////////////////////////////////////////////////
//******************************************************************************
void Creditos()
{
    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(Mensaje5); BUSY_LCD;
    delayms(1500);

    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(Mensaje6); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(Mensaje7); BUSY_LCD;
    delayms(2000);

    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(Mensaje8); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(Mensaje9); BUSY_LCD;
    delayms(2000);

    CLEAR_LCD; BUSY_LCD;
    putrsXLCD(Mensaje10); BUSY_LCD;
    LINEA_02_LCD; BUSY_LCD;
    putrsXLCD(Mensaje11); BUSY_LCD;
    delayms(3000);
}
////////////////////////////////////////////////////////////////////////////////