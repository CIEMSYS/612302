/*
 * File:   Main.c
 * Author: Sebastian Puente R
 *
 * Created on 1 de octubre de 2013, 01:01 PM
 */

#include <xc.h>
#include "BitsConfigurations.h"
#include <plib/timers.h> //Libreria para el manejo de los temporizadores

#define ValorTimer0 49911 //Valor a cargar en el TIMER0 para un tiempo de desbordamiento igual a 1 seg

unsigned char contador = 0;

void main()
{
    //Oscilador 8 Mhz Interno
    OSCCONbits.IRCF = 0b111; //Configuramos oscilador interno a 8 Mhz
    //OSCCONbits.IRCF2 = 1;
    //OSCCONbits.IRCF1 = 1;
    //OSCCONbits.IRCF0 = 1;

    //Configuracion puertos
    ADCON1 = 0x0F;      //Todos los pines asociados al conversor A/D son configurados como entradas o salidas digitales
    TRISD = 0x00;       //PORTD como salida digital
    //TRISBbits.TRISB0 = 1;  //Pin RB0 como entrada

    //Configuracion fuentes de interrupciones
    RCONbits.IPEN = 1;      //Sistema de prioridad de interrupciones habilitado
    INTCONbits.INT0IE = 1;   //Interrupcion externa 0 habilitada (Cambio en RB0)
    INTCON2bits.INTEDG0 =0; //La interrupción externa 0 se dispara por flanco de bajada
    //No se le puede configurar priordad a la interrupción externa 0, ya que ella siempre tiene prioridad alta

    //Configuración Timer0. Se pueden usar las funciones de la libreria timers.h (ver documentos de librerias para los PIC18)
    INTCON2bits.TMR0IP = 0; //Prioridad baja para la interrupción del TIMER0
    OpenTimer0(T0_SOURCE_INT & T0_16BIT & T0_PS_1_128 & TIMER_INT_ON); //Configuramos las caracteristocas para el TIMER 0
    WriteTimer0(ValorTimer0); //Escribimos el valor inicial del TIMER 0 de acuerdo a cálculos previos siempre despues del OpenTimer0

    INTCONbits.GIEH = 1;    //Se habilitan globalmente las interrupciones de prioridad alta
    INTCONbits.GIEL = 1;    //Se habilitan globalmente las interrupciones de prioridad baja

    //Programa
    while(1)
    {
        PORTD = contador;
    }
}

//Atencion a todas las interrupciones de prioridad alta habilitadas
void high_priority interrupt InterrupcionesAltas()
{
    if (INTCONbits.INT0F == 1)  //A través de las diferentes banderas de las fuentes de interrupción se puede averiguar la causante de la interrupcion
    {
        contador = 0;
        INTCONbits.INT0F = 0;   //Despues de identificada la interrupción de prioridad alta se debe borrar su bandera respectiva
    }
    else
        Nop();

}

//Atencion a todas las interrupciones de prioridad baja habilitadas
void low_priority interrupt InterrupcionesBajas()
{
    if(INTCONbits.TMR0IF == 1)
    {
        contador++;
        WriteTimer0(ValorTimer0);
        INTCONbits.TMR0IF = 0;
    }
    else
        Nop();
}