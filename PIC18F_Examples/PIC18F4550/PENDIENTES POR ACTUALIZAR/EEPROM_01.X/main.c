/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 15 de septiembre de 2014, 02:07 PM
 */

//******************************************************************************
//DESCRIPCI�N
//Para este ejemplo se trabaja con el oscilador interno a 8 MHz.
//Se conecta un display 7 segmentos de c�todo com�n
//Ver la libreria BitsConfiguracion.h
//******************************************************************************

#include <xc.h>
#include "BitsConfiguracion.h"

//Prototipos de funciones
void SetUp();
//void WriteByteEEPROM(char Address, char Byte);
//char ReadByteEEPROM(char Address);
unsigned char f = 0;

void main()
{
    SetUp();

    for(char n = 0; n < 3; n++)
    {
        f = f + 1;
        PORTB = n + f;
    }

}

//Funci�n SetUp
void SetUp()
{
    //Frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111;    //Oscilador interno a 8 MHz
    //Configuracion puertos digitales
    TRISB = 0x00;   //Puerto B como entrada
//    TRISC = 0xFF;   //Puerto C com entrada
//    TRISD = 0x00;   //Puerto D como salida
}
