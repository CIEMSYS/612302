/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 5 de noviembre de 2013, 07:35 PM
 */

#include <xc.h>
#include "lib/BitsConfiguracion.h"
#include "lib/DriveLCD.h"
#include "lib/keypad.h"

#define _XTAL_FREQ 4000000 //4 Mhz CristaL

////////////////////////////////////////////////////////////////////////////////
void IniciaLCD(void);

////////////////////////////////////////////////////////////////////////////////
// Principal
void main(void)
 {
    ADCON1 = 0x0F;
    
    char Mensaje1[] = "DIGITE LA CLAVE:";
    char Clave[4] = {'0','0','0','0'};
    char i;

    IniciaLCD();
    putsXLCD(Mensaje1); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD());

    for(i = 0; i <= 3; i++)
    {
        Clave[i] = GetkeyKEYPAD();
        WriteDataXLCD('*'); while(BusyXLCD());
        //putcXLCD(Clave[i]); while(BusyXLCD());
    }

    SetDDRamAddr(64); while(BusyXLCD());
    putsXLCD("Clave: "); while(BusyXLCD());
    putsXLCD(Clave); while(BusyXLCD());

    while(1);
}

////////////////////////////////////////////////////////////////////////////////
void IniciaLCD()
 {
	OpenXLCD(EIGHT_BIT & LINES_5X7);
        while( BusyXLCD() );
        WriteCmdXLCD(BLINK_OFF & CURSOR_OFF);
        while( BusyXLCD() );
        WriteCmdXLCD(SHIFT_DISP_LEFT);
        while( BusyXLCD() );
 }
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//RETARDOS PARA LA LIBRERIA MYLCD
////////////////////////////////////////////////////////////////////////////////
void DelayFor18TCY(void)
{
    _delay(18);
    return;
}
////////////////////////////////////////////////////////////////////////////////
void DelayPORXLCD(void)
{
    __delay_ms(15);
    return;
}
////////////////////////////////////////////////////////////////////////////////
void DelayXLCD(void)
{
    __delay_ms(5);
    return;
}
////////////////////////////////////////////////////////////////////////////////