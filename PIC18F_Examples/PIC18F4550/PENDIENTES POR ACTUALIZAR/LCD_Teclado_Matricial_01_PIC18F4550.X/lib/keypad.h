/***********************************************************************************
PIC 18F452 Keypad Scanner */
// Rows are connected to Port B 
// Columns are connected to Port D with external pull-up resistors.

///////////////////////////////////////
// Keypad Config.
#define KEYP_NUM_ROWS 4
#define KEYP_NUM_COLS 4

char KeyCodes[16] = {'1','2','3','A',
		     '4','5','6','B',
		     '7','8','9','C',
		     '*','0','#','D'
		    };

////////////////////////////////////////
//Prototipos de funciones
char KEYPADRead(void);
char ScanKEYPAD(void);
char GetkeyKEYPAD(void);
////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////
//// KEYPADRead()
char KEYPADRead(void)
// Find a key, wait for it to be released and return.
{ char key = ScanKEYPAD();
  if (key)
      while (ScanKEYPAD() != 0)
         /* Nothing */  ;  
   return key;
 } 

///////////////////////////////////////////////////////////////////////////
//// ScanKEYPAD
char ScanKEYPAD(void)
// Scan the keypad for a keypress.
// Return 0 for no press or the char pressed.
 {	signed char row,col,tmp;
   	char key=0;
	int wait;

   	// Disable ADC functionality on Port A
   	//ADCON1 = 6; //PIC18F452
        ADCON1 = 0x0F; //PIC18F4550/20

   	// Initialise Port for input, and PORTC for output
   	TRISD = 0xFF; //Columnas
        PORTB = 0xFF;
   	TRISB = 0;  //Filas

   	for (row=0; row < KEYP_NUM_ROWS; row++)
    { // Drive appropriate row low and read columns:
      PORTB = ~(1 << row);
      for (wait=0; wait<100; ++wait)
         ;
      tmp = PORTD;
    
      // See if any column is active (low):
      for (col=0; col<KEYP_NUM_COLS; ++col)
         if ((tmp & (1<<col)) == 0)
          { signed char idx = (row*KEYP_NUM_COLS) + col;
            key = KeyCodes[idx]; 
            goto DONE;
          }
    }
   	DONE:

   	// Disable Port Drive and return.
   	TRISB = 0xFF;
   	return key;
 }

///////////////////////////////////////////////////////////////////////////
char GetkeyKEYPAD (void)
// Use the input routine from the *Keypad_Read* assembly file to 
// Scan for a key and return ASCII value of the Key pressed.
 { 
	char mykey;
  	while ((mykey = KEYPADRead()) == 0x00)
    /* Poll again */;
  	return mykey;
 }


