/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 17 de septiembre de 2014, 11:41 PM
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BitsConfiguracion.h"
#include "DriveLCD.h"

#define _XTAL_FREQ 4000000 //4 Mhz FOSC
#define LED_PREGUNTA PORTCbits.RC0
#define LED_CORRECTO PORTCbits.RC1
#define LED_INCORRECTO PORTCbits.RC2

#define P01 PORTBbits.RB2
#define P02 PORTBbits.RB3
#define P03 PORTBbits.RB4
#define P04 PORTBbits.RB5
#define P05 PORTBbits.RB6
#define P06 PORTBbits.RB7
#define P07 PORTAbits.RA0
#define P08 PORTAbits.RA1
#define P09 PORTAbits.RA2
#define P10 PORTAbits.RA3
#define P11 PORTAbits.RA4
#define P12 PORTAbits.RA5

struct Pregunta
{
    char fila1[16];
    char fila2[16];
};

struct Pregunta Pregunta1;
struct Pregunta Pregunta2;
struct Pregunta Pregunta3;
struct Pregunta Pregunta4;
struct Pregunta Pregunta5;
struct Pregunta Pregunta6;
struct Pregunta Pregunta7;
struct Pregunta Pregunta8;
struct Pregunta Pregunta9;
struct Pregunta Pregunta10;
struct Pregunta Pregunta11;
struct Pregunta Pregunta12;

char Mensaje1[] = "Bienvenido";
char Mensaje2[] = "Lea y escoja";
char Mensaje3[] = "en la celula...";

char Mensaje01[] = "CELULA VEGETAL";
char Mensaje02[] = "Elaborada por:";
char Mensaje03[] = "   Soltelect   ";
char Mensaje04[] = "   3108825296   ";
char Mensaje05[] = "Intellectus sas";
char Mensaje06[] = "   3106963192   ";
char Mensaje07[] = "Sebastian Puente";
char Mensaje08[] = "   3004302995   ";

const char Respuesta01[] = "Nucleo";
const char Respuesta02[] = "Nucleolo";
const char Respuesta03[] = "Aparato de golgi";
const char Respuesta04[] = "Pared celular";
const char Respuesta05[] = "Lisosomas";
const char Respuesta06[] = "Ribosomas";
const char Respuesta07[] = "Citoesqueleto";
const char Respuesta08[] = "Centriolo";
const char Respuesta09[] = "R. endplas. liso";
const char Respuesta10[] = "Vacuolas";
const char Respuesta11[] = "R. endpla rugoso";
const char Respuesta12[] = "Mitocondrias";

char aux,n;
char Respuestas[]= {20,1,2,3,4,5,6,7,8,9,10,11,12};

////////////////////////////////////////////////////////////////////////////////
// Prototipos de funciones
void SetUp();
void IniciaLCD();
void delayms(int ms);
void TextoPreguntas();
void MostrarPregunta(struct Pregunta n);
void EscogerPregunta(unsigned char n);
char GetKey();
char VerificarRespuesta(char NumPregunta, char RespuestaActual);
void Creditos();

////////////////////////////////////////////////////////////////////////////////
// Principal
void main(void)
{
    char Aciertos = 0;
    char AciertosString[4];
    char Verif;
    char Respuesta;

    SetUp();

    for(n = 1; n < 13; n++)
    {
        aux = 1;
        if(aux == 0) continue;

        EscogerPregunta(n);
        LED_PREGUNTA = 1;
        LED_CORRECTO = 0;
        LED_INCORRECTO = 0;
        Respuesta = GetKey();
        if(Respuesta == 0) continue;
        Verif = VerificarRespuesta(n,Respuesta);
        delayms(1500);

        if(Verif == 0)
        {
            n = n - 1; continue;
        }
        if(Verif == 1)
            Aciertos = Aciertos + 1;
    }

    WriteCmdXLCD(0b00000001); while(BusyXLCD());
    itoa(AciertosString,Aciertos,10);
    putsXLCD("Total Puntos"); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD());
    putsXLCD(AciertosString); while(BusyXLCD());
    putsXLCD("/12"); while(BusyXLCD());

    while(1);
}

////////////////////////////////////////////////////////////////////////////////
void SetUp()
{
    OSCCONbits.IRCF = 0b110;     //Oscilador interno 4 MHz
    ADCON1bits.PCFG = 0b1111;   //Canales analógicos desabilitados

    IniciaLCD();

    //Configuración puertos
    TRISCbits.TRISC0 = 0;
    TRISCbits.TRISC1 = 0;
    TRISCbits.TRISC2 = 0;
    TRISBbits.TRISB0 = 1;
    TRISBbits.TRISB1 = 1;
    TRISBbits.TRISB2 = 1;
    TRISBbits.TRISB3 = 1;
    TRISBbits.TRISB4 = 1;
    TRISBbits.TRISB5 = 1;
    TRISBbits.TRISB6 = 1;
    TRISBbits.TRISB7 = 1;
    TRISAbits.TRISA0 = 1;
    TRISAbits.TRISA1 = 1;
    TRISAbits.TRISA2 = 1;
    TRISAbits.TRISA3 = 1;
    TRISAbits.TRISA4 = 1;
    TRISAbits.TRISA5 = 1;

    LED_PREGUNTA = 0;
    LED_CORRECTO = 0;
    LED_INCORRECTO = 0;
    
    TextoPreguntas();
    Creditos();

    WriteCmdXLCD(0b00000001); while( BusyXLCD() );
    SetDDRamAddr(0); while(BusyXLCD());
    putsXLCD(Mensaje1);
    delayms(2000);
    SetDDRamAddr(0); while(BusyXLCD());
    putsXLCD(Mensaje2); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD());
    putsXLCD(Mensaje3);
    delayms(2000);
    WriteCmdXLCD(0b00000001); while( BusyXLCD() );
    SetDDRamAddr(0); while(BusyXLCD());

    RCONbits.IPEN = 0;
    INTCONbits.INT0IE = 1;
    INTCON2bits.INTEDG0 = 1;
    INTCON3bits.INT1IE = 1;
    INTCON2bits.INTEDG1 = 1;
    INTCONbits.GIE = 1;
}
////////////////////////////////////////////////////////////////////////////////
void IniciaLCD()
{
    OpenXLCD(FOUR_BIT & LINES_5X7); while( BusyXLCD() );
    WriteCmdXLCD(BLINK_OFF & CURSOR_OFF); while( BusyXLCD() );
    WriteCmdXLCD(SHIFT_DISP_LEFT); while( BusyXLCD() );
 }
////////////////////////////////////////////////////////////////////////////////
void MostrarPregunta(struct Pregunta n)
{
    WriteCmdXLCD(0b00000001); while(BusyXLCD());
    putsXLCD(n.fila1); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD());
    putsXLCD(n.fila2); while(BusyXLCD());
}
////////////////////////////////////////////////////////////////////////////////
void EscogerPregunta(unsigned char n)
{
    switch(n)
    {
        case 1:
            MostrarPregunta(Pregunta1); break;
        case 2:
            MostrarPregunta(Pregunta2); break;
        case 3:
            MostrarPregunta(Pregunta3); break;
        case 4:
            MostrarPregunta(Pregunta4); break;
        case 5:
            MostrarPregunta(Pregunta5); break;
        case 6:
            MostrarPregunta(Pregunta6); break;
        case 7:
            MostrarPregunta(Pregunta7); break;
        case 8:
            MostrarPregunta(Pregunta8); break;
        case 9:
            MostrarPregunta(Pregunta9); break;
        case 10:
            MostrarPregunta(Pregunta10); break;
        case 11:
            MostrarPregunta(Pregunta11); break;
        case 12:
            MostrarPregunta(Pregunta12); break;
        default:
            break;
    }
}
////////////////////////////////////////////////////////////////////////////////
void TextoPreguntas()
{
    strcpy(Pregunta1.fila1,"Ubique");
    strcpy(Pregunta1.fila2,"el nucleo");

    strcpy(Pregunta2.fila1,"Ubicado dentro");
    strcpy(Pregunta2.fila2,"del nucleo");

    strcpy(Pregunta3.fila1,"El aparato");
    strcpy(Pregunta3.fila2,"de golgi");

    strcpy(Pregunta4.fila1,"Rodea la");
    strcpy(Pregunta4.fila2,"celula");

    strcpy(Pregunta5.fila1,"Donde esta");
    strcpy(Pregunta5.fila2,"el lisosoma?");

    strcpy(Pregunta7.fila1,"Senale el");
    strcpy(Pregunta7.fila2,"citoesqueleto");

    strcpy(Pregunta8.fila1,"Permiten la");
    strcpy(Pregunta8.fila2,"polimerizacion");

    strcpy(Pregunta9.fila1,"El reticulo endo");
    strcpy(Pregunta9.fila2,"plasmatico liso");

    strcpy(Pregunta10.fila1,"Ubique");
    strcpy(Pregunta10.fila2,"las vacuolas");

    strcpy(Pregunta11.fila1,"Reticulo endopla");
    strcpy(Pregunta11.fila2,"smatico granular");

    strcpy(Pregunta6.fila1,"Sintetizan");
    strcpy(Pregunta6.fila2,"proteinas");

    strcpy(Pregunta12.fila1,"Ubique las");
    strcpy(Pregunta12.fila2,"Mitocondrias");
}
////////////////////////////////////////////////////////////////////////////////
void delayms(int ms)
{
    for(int n = 0; n < ms; n++)
    {
        __delay_ms(1);
    }
}
////////////////////////////////////////////////////////////////////////////////
char GetKey()
{
    char auxi = 1;
    do
    {
        if (aux == 0) return 0;
        if(P01 == 1) return 1;
        if(P02 == 1) return 2;
        if(P03 == 1) return 3;
        if(P04 == 1) return 4;
        if(P05 == 1) return 5;
        if(P06 == 1) return 6;
        if(P07 == 1) return 7;
        if(P08 == 1) return 8;
        if(P09 == 1) return 9;
        if(P10 == 1) return 10;
        if(P11 == 1) return 11;
        if(P12 == 1) return 12;
        else auxi = 0;
    }while(auxi == 0);

}
////////////////////////////////////////////////////////////////////////////////
void interrupt Interrupciones()
{
    if(INTCONbits.INT0IF == 1)
    {
        aux = 0;
        INTCONbits.INT0IF = 0;
    }
    if(INTCON3bits.INT1IF == 1)
    {
        aux = 0;
        if(n == 1)
            n = n - 1;
        else
            n = n - 2;
        INTCON3bits.INT1IF = 0;
    }
}
////////////////////////////////////////////////////////////////////////////////
char VerificarRespuesta(char NumPregunta, char RespuestaActual)
{
    WriteCmdXLCD(0b00000001); while(BusyXLCD());
    char texto[16];

    switch (RespuestaActual)
    {
        case 1:
            strcpy(texto,Respuesta01);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 2:
            strcpy(texto,Respuesta02);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 3:
            strcpy(texto,Respuesta03);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 4:
            strcpy(texto,Respuesta04);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 5:
            strcpy(texto,Respuesta05);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 6:
            strcpy(texto,Respuesta06);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 7:
            strcpy(texto,Respuesta07);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 8:
            strcpy(texto,Respuesta08);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 9:
            strcpy(texto,Respuesta09);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 10:
            strcpy(texto,Respuesta10);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 11:
            strcpy(texto,Respuesta11);
            putsXLCD(texto); while(BusyXLCD());
            break;
        case 12:
            strcpy(texto,Respuesta12);
            putsXLCD(texto); while(BusyXLCD());
        default:
            break;
    }
    delayms(1000);

    if(RespuestaActual == Respuestas[NumPregunta])
    {
        WriteCmdXLCD(0b00000001); while(BusyXLCD());
        LED_PREGUNTA = 0;
        LED_CORRECTO = 1;
        LED_INCORRECTO = 0;
        putsXLCD("CORRECTO!");
        SetDDRamAddr(64); while(BusyXLCD());
        putsXLCD("Has acertado!"); while(BusyXLCD());
        return 1;
    }
    if(RespuestaActual == 0)
    {
        return 2;
    }
    else
    {
        WriteCmdXLCD(0b00000001); while(BusyXLCD());
        LED_PREGUNTA = 0;
        LED_CORRECTO = 0;
        LED_INCORRECTO = 1;
        putsXLCD("INCORRECTO!");
        SetDDRamAddr(64); while(BusyXLCD());
        putsXLCD("Intenta de nuevo!"); while(BusyXLCD());
        return 0;
    }
}
////////////////////////////////////////////////////////////////////////////////
void Creditos()
{
    SetDDRamAddr(0); while(BusyXLCD()); putsXLCD(Mensaje01); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD()); putsXLCD(Mensaje02); while(BusyXLCD());
    delayms(1500);

    WriteCmdXLCD(0b00000001); while(BusyXLCD());
    putsXLCD(Mensaje03); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD()); putsXLCD(Mensaje04); while(BusyXLCD());
    delayms(1500);

    WriteCmdXLCD(0b00000001); while(BusyXLCD());
    putsXLCD(Mensaje05); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD()); putsXLCD(Mensaje06); while(BusyXLCD());
    delayms(1500);

    WriteCmdXLCD(0b00000001); while(BusyXLCD());
    putsXLCD(Mensaje07); while(BusyXLCD());
    SetDDRamAddr(64); while(BusyXLCD()); putsXLCD(Mensaje08); while(BusyXLCD());
    delayms(2000);
}

//Sebastián Fernando Puente Reyes
//ing.sebastianpuente@gmail.com
//labdifuso@gmail.com