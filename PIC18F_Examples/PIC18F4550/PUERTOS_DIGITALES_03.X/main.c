/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 10 de mayo de 2017, 03:04 PM
 */

//Directivas de preprocesamiento
#include <xc.h>             //Libreria necesaria para el PIC escogido
#include "ConfigBits.h"     //Libreria con los Bits de Configuración

//Macros            8 MHz
#define _XTAL_FREQ 8000000 //Este macro es necesario para utilizar __delay_us(x), __delay_ms(x)

//Prototipos de funciones
void SetUp();
void delayms(int ms);

//Variables Globales
bit LED = 0;    //Variable tipo bit

//Función principal en C
void main()
{
    SetUp();    //Llamado a la función SetUp

    while(1)    //Bucle infinito
    {
        LED = !LED;             //Se complementa la variable LED
        PORTEbits.RE0 = LED;    //El valor de la variable LED se muestra por la línea RE0
        delayms(1000);          //Función creada para temporizaciones en ms, se pide un retar de 1000ms = 1seg
    }
}

//Función SetUp
void SetUp()
{
    //COnfiguración Frecuencia de trabajo del MCU a través del oscilador interno
    OSCCONbits.IRCF = 0b111;    //Se escogen los 8 Mhz del oscilador interno como frecuencia de trabajo del MCU
    
    //Configuración Puertos
    ADCON1bits.PCFG = 0b1111;   //Todos los pines asociados como canales analógicos son configurados como digitales (Pag 266 DataSheet)
    TRISEbits.TRISE0 = 0;       //Línea RE0 como salida digital
}

//Función delayms encargada de realizar un retardo en milisegundos dado por el argumento int ms
void delayms(int ms)
{
    for(int n = 0; n < ms; n++)
    {
        __delay_ms(1);
    }
}
////////////////////////////////////////////////////////////////////////////////