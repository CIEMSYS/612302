/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 10 de mayo de 2017, 02:51 PM
 */

//******************************************************************************
//NOTA
//Para este ejemplo se trabaja con el oscilador interno a 8 MHz
//Ver el archivo de cabecera ConfigBits.h
//******************************************************************************

//Directivas de preprocesamiento
#include <xc.h> //Libreria necesaria para el PIC escogido
#include "ConfigBits.h"  //Libreria con los Bits de Configuración

//Prototipos de funciones
void SetUp();

//Variables Globales
char Suma = 0;

//Función principal en C
void main()
{
    SetUp();            //Llamado a la función SetUp
    char Numero1 = 0;   //Variable local a la función main

    while(1) //Bucle infinito
    {
        Numero1 = PORTB;    //El valor de las 8 líneas del PORTB es asignado a la variable tipo char (8 bits) Numero1
        Suma = Numero1 + 2;
        PORTD = Suma;       //El contenido de suma (8 bits) es sacado al PORTD
    }
}

//Función SetUp
void SetUp()
{
    //COnfiguración Frecuencia de trabajo del MCU a través del oscilador interno
    OSCCONbits.IRCF = 0b111;    //Se escogen los 8 Mhz del oscilador interno como frecuencia de trabajo del MCU
    //Configuración Puertos
    ADCON1bits.PCFG = 0b1111;   //Todos los pines asociados como canales analógicos son configurados como digitales (Pag 266 DataSheet)
    TRISB = 0xFF;               //Todas las líneas del PORTB como entradas digitales
    TRISD = 0x00;               //Todas las líneas del PORTD como salidas digitales
}