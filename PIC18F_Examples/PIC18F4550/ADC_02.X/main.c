/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     ADC_02 
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F4550
 * Compiler:        XC8
 * Version:         1.42
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Junio de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 8 MHz.
 * Se realiza la conversion a digital de los canales AN0 y AN1 dependiendo de la
 * l�nea RB0; donde se conectar� un interruptor (SW1). Posteriormente se
 * visualiza el resultado de la conversi�n del canal seleccionado a trav�s de
 * las l�neas RE1-RE0-RD7-RD6-RD5-RD4-RD3-RD2-RD1-RD0.
 * Los voltajes de referencia para el conversor son:
 * Vref+ = 3V (RA3), Vref- = 0V (Vss). Vref = 3V (seg�n datasheet min 3V)
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "BitsConfiguracion.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define _XTAL_FREQ 8000000
#define SW1 PORTBbits.RB0
#define IniciarConversion()   ADCON0bits.GO = 1
//SW1 hace referencia al switch conectado en la l�nea RB0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void SetUpADC(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
unsigned int ResultadoConversion = 0;
//Variable entera para guardar el resultado de la conversi�n A/D

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();    //Configuraci�n Inicial
    SetUpADC(); //Configuraci�n del Conversor AD

    while(1) //Ciclo infinito
    {
        //Selecci�n del canal a convertir
        switch(SW1)
        {
            case 0:
                //Se selecciona el canal AN0 para la conversi�n A/D
                ADCON0bits.CHS = 0;
                break;
            case 1:
                //Se selecciona el canal AN0 para la conversi�n A/D
                ADCON0bits.CHS = 1;
                break;
            default:
                break;
        }
        //ADCON0bits.GO = 1; //Iniciar conversi�n
        IniciarConversion();

        //Bucle de espera de final de la conversi�n A/D
        //Comprobaci�n del bit GO/DONE hasta que se ponga a '0'
        while(ADCON0bits.DONE);

        //Lectura resultado de la conversi�n de los resgistros ADRESH y ADRESL
        ResultadoConversion = (((unsigned int)ADRESH)<<8)|(ADRESL);

        //Visualizaci�n de los 10 bits del resultado de la conversi�n
        PORTD = ResultadoConversion;
        PORTE = ResultadoConversion >> 8;
    }
    return;
}

/*******************************************************************************
 * FUNCTION:	SetUpADC()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n m�dulo conversor A/D
 ******************************************************************************/
void SetUpADC(void)
{
    /* Configuraci�n canales anal�gicos a utilizar
     * Se trabajar� con los canales AN0(pin 2) y AN1(pin 3) los demas canales
     * quedan deshabilitados y se podran trabajar como digitales */
    ADCON1bits.PCFG = 0b1101;

    /* Configuraci�n  tensi�n de referencia
     * Vref- = Vss, Vref+ = se conecta en la l�nea f�sica RA3 (pin 5)
     * para este ejemplo conectamos 3V */
    ADCON1bits.VCFG = 0b01; 

    /* Configuraci�n reloj de conversi�n TAD
     * TOSC = 1/8000000 = 0.125 useg (TAD debe ser m�nimo 0.8us)
     * TAD = 8*TOSC = 1.0 useg*/
    ADCON2bits.ADCS = 0b001;

    /* Configuraci�n del tiempo de adquisici�n
     * Seg�n Datasheet, TACQ min debe ser 1,4us.
     * De acuerdo a lo anterior, escogemos TACQ = 2*TAD = 2.0 useg */
    ADCON2bits.ACQT = 0b001;

    /* Configuraci�n del modo de almacenamiento de la conversi�n.
     * Justificado a derechas */
    ADCON2bits.ADFM = 1;

    //Activaci�n del conversor
    ADCON0bits.ADON = 1;

    return;
}

/*******************************************************************************
 * FUNCTION:	SetUp()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    //En las siguientes l�neas se visualizaran los 10 bits de la conversion
    TRISD = 0;              //L�neas puerto D como salidas digitales
    TRISEbits.TRISE0 = 0;   //L�nea RE0 como salida digital
    TRISEbits.TRISE1 = 0;   //L�nea RE1 como salida digital
    /* L�nea RB0 como entrada. En esa l�nea se conectar� un interruptor para la
     * selecci�n del canal a convertir.
     * Si RB0 = 0, se debe hacer la conversi�n del canal AN0
     * Si RB0 = 1, se debe hacer la conversi�n del canal AN1
     */
    TRISBbits.RB0 = 1;
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/