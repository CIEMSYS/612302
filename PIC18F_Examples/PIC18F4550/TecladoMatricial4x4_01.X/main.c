/*******************************************************************************
* FileName:         State_Clock.c
* Dependencies:     See INCLUDES section below
* Processor:        PIC18F4550
* Compiler:         XC8
* Version:          1.42
* Author:           Sebasti�n Fernando Puente Reyes
* e-mail:           sebastian.puente@unillanos.edu.co
* Date:             Mayo de 2017
*******************************************************************************/

#include <xc.h>
#include "BitsConfiguracion.h"
#include "KeyPad.h"

//******************************************************************************
//Prototipos de funciones
void SetUp(void);

//******************************************************************************
//Variables globales
char TeclaOprimida = 0; //Variable que tendr� el c�digo ascci de la tecla oprimida

/*******************************************************************************
 * Funci�n principal
 ******************************************************************************/
void main(void)
{
    SetUp();    //Configuraci�n Inicial

    while(1)
    {
        TeclaOprimida = GetKey();   //Se captura la tecla oprimida y se terna el c�digo ascci
        PORTD = TeclaOprimida;      //Se saca por el PORTD el c�dido ASCCI de la tecla oprimida
    }

    return;
}

/*******************************************************************************
 * FUNCTION:	SetUp()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (Fosc y Puertos)
 ******************************************************************************/
void SetUp()
{
    OSCCONbits.IRCF = 0b111;    //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111;   //Canales anal�gicos desactivados
    TRISD = 0x00;   //PORTD como salida
    PORTD = 0;      //L�neas del PORTD a cero
    return;
}

/*******************************************************************************
 * Dise�o Digital con Microcontroladores PIC de 8 bits
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/