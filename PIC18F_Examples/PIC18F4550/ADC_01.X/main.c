/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     ADC_01 
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F4550
 * Compiler:        XC8
 * Version:         1.42
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Junio de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 4 MHz.
 * Se realiza la conversion a digital (10 bits) del canal AN0 y posteriormente
 * se visualiza en las l�neas RE1-RE0-RD7-RD6-RD5-RD4-RD3-RD2-RD1-RD0.
 * Los voltajes de referencia para el conversor son:
 * Vref+ = 5V (VDD), Vref- = 0V (Vss). ?Vref = 5V (seg�n datasheet min 3V)
 ******************************************************************************/
/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "BitsConfiguracion.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void SetUp_ADC(void);
int SetChGoADC(unsigned char Channel);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    //Variable entera para guardar el resultado de la conversi�n A/D
    unsigned int ResultadoConversion = 0;
    
    SetUp();        //Configuraci�n inicial
    SetUp_ADC();    //Configuraci�n ADC
    
    while(1)        //Bucle infinito
    {
        ResultadoConversion = SetChGoADC(0); //Conversi�n del canal AN0
        //Visualizaci�n de los 10 bits del resultado de la conversi�n
        PORTD = ResultadoConversion;
        PORTE = ResultadoConversion >> 8;
    }

    return;
}

/*******************************************************************************
 * FUNCTION:	SetChGoADC(unsigned char Channel)
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: 
 ******************************************************************************/
int SetChGoADC(unsigned char Channel)
{
    //Selecci�n del canal a convertir
    ADCON0bits.CHS = Channel;
    ADCON0bits.GO = 1;  //Se lanza la conversi�n
    //Se espera a que termine la conversi�n comprobando que el bit GO/DONE se ponga a '0'
    while(ADCON0bits.DONE); 
    return ((ADRESH << 8) + ADRESL);   
}

/*******************************************************************************
 * FUNCTION:	SetUp_ADC()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n del Conversor Anal�gico-Digital
 ******************************************************************************/
void SetUp_ADC()
{
    //Configuraci�n canales anal�gicos a utilizar
    ADCON1bits.PCFG = 0b1110; //Se trabajar� con el canal AN0(pin 2) los demas canales quedan deshabilitados y se podran trabajar como digitales
    //Configuraci�n  tensi�n de referencia
    ADCON1bits.VCFG = 0b00; //Vref- = Vss, Vref+ = Vdd (0v - 5v)
    //Configuraci�n reloj de conversi�n TAD
    ADCON2bits.ADCS = 0b001; //8*Tosc (Fosc/8) = 1us (TAD debe ser m�nimo 0,8us)
    //Configuraci�n del tiempo de adquisici�n automatico
    ADCON2bits.ACQT = 0b001; //TACQ = 2TAD = 2us (seg�n Datasheet TACQ min debe ser 1,4us)
    //Configuraci�n del modo de almacenamiento de la conversi�n
    ADCON2bits.ADFM = 1; //Justificado a derechas
    //Activaci�n del conversor
    ADCON0bits.ADON = 1; //Conversor A/D activado

    return;
}

/*******************************************************************************
 * FUNCTION:	SetUp()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp()
{
    OSCCONbits.IRCF = 0b111;    //Oscilador interno a 8 MHz
    TRISD = 0;                  //L�neas puerto D como salidas digitales
    TRISEbits.TRISE0 = 0;       //L�nea RE0 como salida digital
    TRISEbits.TRISE1 = 0;       //L�nea RE1 como salida digital
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/