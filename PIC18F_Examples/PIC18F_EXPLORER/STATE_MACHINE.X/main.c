/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 17 de abril de 2017, 05:20 PM
 */
//Frecuencia de Trabajo Fosc = 40 MHz

#include "SystemInitialize.h"

// Enumerations
enum {StIntro, StVoltmeter, StClock} StateMachine;
unsigned char State = StIntro;

/*
                         Main application
 */
void main(void)
{
    SysInitialize();
    
    while(1)
    {
        switch(State)
        {
            case StIntro: Intro();
            break;
            case StVoltmeter: Voltmeter();
            break;
            case StClock: Clock();
            break;
        }
    }
}
//------------------------------------------------------------------------------

