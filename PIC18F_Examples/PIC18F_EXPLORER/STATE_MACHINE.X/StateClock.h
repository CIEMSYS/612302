/* 
 * File:   StateClock.h
 * Author: Lenovo
 *
 * Created on 17 de abril de 2017, 05:11 PM
 */

#ifndef STATECLOCK_H
#define	STATECLOCK_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "SystemInitialize.h"

volatile unsigned char hr = 0, min = 0, sec = 0, timeset = 0;

// Enumerations
enum {hour, minute} Time;

// Function prototypes
void Clock(void);
void DispClock(void);
void CountClock(void);
void SetClock(void);

#ifdef	__cplusplus
}
#endif

#endif	/* STATECLOCK_H */

