
#include "StateIntro.h"

extern unsigned char State;

/*******************************************************************
* FUNCTION:     Intro ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Displays the Introductory Text on LCD and host PC
********************************************************************/
void Intro(void)
{
    // Initialize LCD
    mOPEN_LCD;
    
    // Display Intro text on LCD
    mCURSOR_LINE1;
    LCDPutStr("  MPUs y MCUs  ");
    mCURSOR_LINE2;
    LCDPutStr(" PIC18 Explorer ");

    // Delay before switching to the next display
    IntroDelay();

    // Switch to Voltmeter application
    State++;
}

/*******************************************************************
* FUNCTION:     Intro_delay ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Delays the display of Introductory Text on LCD
********************************************************************/
void IntroDelay(void)
{
    unsigned char i;
    for (i=0;i<100;i++)
    {__delay_ms(30);}
}