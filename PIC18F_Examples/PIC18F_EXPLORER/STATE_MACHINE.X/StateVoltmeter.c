
#include "StateVoltmeter.h"

extern unsigned char State;

/*******************************************************************
* FUNCTION:     Voltmeter ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Displays the Voltmeter text and Now/Next options
*               on LCD and host PC
********************************************************************/
void Voltmeter (void)
{
    // Display Voltmeter text and Now/Next options on LCD
    mCURSOR_LINE1;
    LCDPutStr("   Voltmeter    ");
    mCURSOR_LINE2;
    LCDPutStr("RB0=Now RA5=Next");

    // Poll the button to be pressed
    while (State == 1)
    {
        if (select == pressed)          // Condition if RB0 is pressed
        {
            SWdelay();
            DispVolts();              // Display Volts
        }
        if (scroll == pressed)          // Condition if RA5 is pressed
        {
            SWdelay();
            State = 0;                    // Switch to Temperature application
        }
    }
}

/*******************************************************************
* FUNCTION:     disp_Volts ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Displays the voltage value and Exit option
*               on LCD and host PC
********************************************************************/
void DispVolts(void)
{
    char TextoLCD[16];
    float a;

    while (State == 1)
    {
        a = VoltResult();
        
        // Display Voltage value and Exit option on LCD
        mCURSOR_LINE1;
        LCDPutStr("Volts =         ");
        mCURSOR_LINE2;
        sprintf(TextoLCD, "%.2f", a);
        LCDPutStr(TextoLCD);
        LCDPutStr("V   RB0=Exit");

        // Poll the RB0 button
        if (select == pressed)              // Condition if RB0 is pressed
        {
            SWdelay();
            State = 0;                        // Switch to Temperature application
        }
    }
}

/*******************************************************************
* FUNCTION:     Volt_Result()
* INPUTS:       None
* OUTPUTS:      b = Voltage value
* DESCRIPTION:  Converts the ADC result into voltage decimal value
********************************************************************/
float VoltResult(void)
{
    float b;
    
    b = (float)SetChGoADC(0);
    b = (b*5)/1023;    // Voltage value for 5V devices
    
    return b;
}