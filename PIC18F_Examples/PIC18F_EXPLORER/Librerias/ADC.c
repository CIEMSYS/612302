
#include "ADC.h"

//Configuraci�n ADC
void SetUpADC(unsigned char Channels)
{
    ADCON1bits.PCFG = Channels; //N�mero de canales a utilizar
    ADCON1bits.VCFG = 0b00; //Config Voltaje Referencia (0V - 5v)
    ADCON2bits.ADFM = 1; //Resultado Justificado a la derecha
    ADCON2bits.ADCS = 0b010; //TAD = 32/FOSC = 0.8 us (m�n 0.7us seg�n DataSheet)
    ADCON2bits.ACQT = 0b001; //Tiempo Adquisici�n TACQ = 2TAD = 1,6us (m�n 1,4us)
    ADCON0bits.ADON = 1; //Se habilita el ADC
    
    ADRESL = 0x00;
    ADRESH = 0X00;

    return;
}  

//Seleccionar canal para la conversi�n
void SetChADC(unsigned char Channel)
{
    ADCON0bits.CHS = Channel;
    return;  
}
//Seleccionar canal, lanzar conversi�n y se espera a que termine
int SetChGoADC(unsigned char Channel)
{
    ADCON0bits.CHS = Channel;
    ADCON0bits.GO = 1; //Se lanzar la conversi�n
    while(ADCON0bits.DONE); //Se espera a que termine la conversi�n
    return ((ADRESH << 8) + ADRESL);   
}

//Lanzar Conversi�n y esperar a que finalice
int GoADC(void)
{
    ADCON0bits.GO = 1; //Se lanzar la conversi�n
    while(ADCON0bits.DONE); //Se espera a que termine la conversi�n
    return ((int)ADRESH << 8) | ADRESL;
}

//Habilitar ADC
void OnADC(void)
{
    ADCON0bits.ADON = 1; //Habilitar el ADC
    return;
}

//Deshabilitar ADC
void OffADC(void)
{
    ADCON0bits.ADON = 0; //Deshabilitar el ADC
    return;
}