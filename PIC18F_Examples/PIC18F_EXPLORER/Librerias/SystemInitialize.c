
#include "SystemInitialize.h"

void SysInitialize(void)
{
    PORTSetUp();
    SetUpADC(CHS_1);
    TMR2Initialize(T2CKPS_1, T2OUTPS_1);
    CCP4Initialize(PWM);
    return;
}

/*******************************************************************
* FUNCTION:     SWdelay ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Delay after pressing the RB0 and RB5 switch buttons
********************************************************************/
void SWdelay (void)
{
    while(scroll == pressed)
        __delay_ms(1);
    while (select == pressed)
        __delay_ms(1);

    return;
}

/*******************************************************************
* FUNCTION:     
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  
********************************************************************/
void PORTSetUp(void)
{
    ADCON1bits.PCFG = 0b1110; //S�lo el canal A0 activado los demas como digitales
    LATA = 0x00;
    LATB = 0x00;
    scroll_dir = 1;
    select_dir = 1;
    return;
}