/*
 * File:   main.c
 * Author: Sebastián
 *
 * Created on 6 de marzo de 2016, 07:42 PM
 */

#include <xc.h>
#include <stdint.h>
#include "BitsConfig.h"
#include "InterruptManager.h"
#include "tmr0.h"
#include "tmr1.h"

#define HIGH 1
#define LOW 0
#define LED0 PORTDbits.RD0
#define LED1 PORTDbits.RD1

//Variables globales


//Prototipos de funciones

void Initialize(void);

void main(void)
{
    Initialize();
    TMR0Initialize();
    TMR1Initialize();
    InterruptInitialize();  

    LED0 = LOW;
    LED1 = LOW;    
    
    while(1)
    {
        if(Timer1Flag == 1)
        {
            LED1 = !LED1;
            Timer1Flag = 0;
        }
        if(Timer0Flag == 1)
        {
            LED0 = !LED0;
            Timer0Flag = 0;
        }
    }  
}

void Initialize()
{
    LATD = 0x00;
    TRISDbits.RD0 = 0; //RD0 como salida
    TRISDbits.RD1 = 0; //RD1 como salida
}