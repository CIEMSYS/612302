/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   tmr0.h
 * Author: Sebastián Puente Reyes
 * Comments:
 * Revision history: 06/MAR/2016
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef TMR0_H
#define	TMR0_H

#include <stdint.h>

#define T0PS_2 0b000
#define T0PS_4 0b001
#define T0PS_8 0b010
#define T0PS_16 0b011
#define T0PS_32 0b100
#define T0PS_64 0b101
#define T0PS_128 0b110
#define T0PS_256 0b111

uint8_t Timer0Flag; 

//Prototipos de función para el Timer0

void TMR0Initialize(void); //Configuración inicial Timer0
void TMR0Start(void); //Start Timer0
void TMR0Stop(void); //Stop Timer0
uint16_t TMR0Read(void); //Lectura del valor del Timer0
void TMR0Write(uint16_t ValTMR0); //Escribir un valor al registro del Timer0
void TMR0InterruptRoutine(void); //Rutina para la interrupción del Timer0

#endif	/* TMR0_H */