
#include <xc.h>
#include "InterruptManager.h"
#include "tmr0.h"
#include "tmr1.h"

void InterruptInitialize(void)
{
    /* IPEN (RCON<7>): Interrupt Priority Enable bit
     * 1 = Enable priority levels on interrupts
     * 0 = Disable priority levels on interrupts (default)
     */
    RCONbits.IPEN = 0; //Sistema de prioridad de interrupciones deshabilitado
    
    PeripheralInterruptEnable();
    GlobalInterruptEnable();
}

void interrupt InterruptsManager()
{
    if(INTCONbits.TMR0IF == 1)
    {
        TMR0InterruptRoutine();
    }
    if(PIR1bits.TMR1IF == 1)
    {
        TMR1InterruptRoutine();
    }
}