
#include <xc.h>
#include "tmr0.h"

void TMR0Initialize(void)
{
    //Tiempo Incremento (Ti) = (4/Fosc)(Prescaler)
    //Time (8 bits) = (Ti)(256 - ValorInicial)
    //Time (16 bits) = (Ti)(65536 - ValorInicial)
    T0CONbits.TMR0ON = 0; //Stop timer0
    T0CONbits.T0CS = 0; //Fuente de reloj: ciclo de instrucción (Fosc/4 = 10 MHz, T = 0.1 us)
    T0CONbits.PSA = 0; //Pre-escalar activado
    T0CONbits.T08BIT = 0; //Modo 16 bits
    T0CONbits.T0PS = T0PS_256; //Pre-esacalar a 256. Ti = 25,6 us
    
    TMR0H = 0xB3;
    TMR0L = 0xB5;
    
    INTCONbits.TMR0IF = 0; //Borrado bandera desbordamiento
    
    /* TMR0IE (INTCON<5>): TMR0 Overflow Interrupt Enable bit
     * 1 = Enable the TMR0 overflow interrupt
     * 0 = Disable the TMR0 overflow interrupt (default)
     */
    INTCONbits.TMR0IE = 1;
    
    TMR0Start();
}

void TMR0Start(void)
{
    T0CONbits.TMR0ON = 1; //Habilitación del Timer0
}

void TMR0Stop(void)
{
    T0CONbits.TMR0ON = 0; //Stop Timer0
}

uint16_t TMR0Read(void)
{
    uint16_t ReadVal;
    uint8_t ReadValHigh;
    uint8_t ReadValLow;
    
    ReadValLow = TMR0L;
    ReadValHigh = TMR0H;
    
    ReadVal = ((uint16_t)ReadValHigh << 8) | ReadValLow;

    return ReadVal;
}

void TMR0Write(uint16_t ValTMR0)
{
    TMR0H = (ValTMR0 >> 8);
    TMR0L = (uint8_t) ValTMR0;
}

void TMR0InterruptRoutine(void)
{
    // Clear the TMR0 interrupt flag
    INTCONbits.TMR0IF = 0;
    
    // Write to the Timer0 register (46005 -> 499.99 mseg)
    TMR0H = 0xB3;
    TMR0L = 0xB5;

    // Add your TMR0 interrupt custom code
    Timer0Flag = 1;
}