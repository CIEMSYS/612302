/* 
 * File:   USART.h
 * Author: Lenovo
 *
 * Created on 8 de febrero de 2018, 01:18 PM
 */

#ifndef USART_H
#define	USART_H

#include <xc.h>
#include <stdio.h>
#include <stdint.h>

void EUSART1_Initialize(void);
uint8_t EUSART1_Read(void);
void EUSART1_Write(uint8_t txData);

#endif	/* USART_H */

