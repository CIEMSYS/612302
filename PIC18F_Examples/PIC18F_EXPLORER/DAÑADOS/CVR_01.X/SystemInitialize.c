
#include "SystemInitialize.h"

void SysInitialize(void)
{
    scroll_dir = 1;
    select_dir = 1;
    ADCON1bits.PCFG = 0b1111; //Canales analogicos desactivados
    CVRInitialize();
}

/*******************************************************************
* FUNCTION:     SWdelay ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Delay after pressing the RB0 and RB5 switch buttons
********************************************************************/
void SWdelay (void)
{
    while(scroll == pressed)
        __delay_ms(1);
    while (select == pressed)
        __delay_ms(1);
}