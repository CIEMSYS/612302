/* 
 * File:   StateIntro.h
 * Author: Lenovo
 *
 * Created on 4 de abril de 2017, 11:28 PM
 */

#ifndef STATEINTRO_H
#define	STATEINTRO_H
 
#include "SystemInitialize.h"

// Function prototypes
void Intro(void);
void IntroDelay(void);

#endif	/* STATEINTRO_H */

