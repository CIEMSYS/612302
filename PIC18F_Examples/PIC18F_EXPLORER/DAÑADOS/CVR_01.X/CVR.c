
#include "CVR.h"

void CVRInitialize(void)
{
    CVRCONbits.CVREN = 0;   //CVREF circuit powered down
    CVRCONbits.CVROE = 1;   //CVREF voltage level is also output on the RF5/AN10/CVREF pin
    CVRCONbits.CVRR = 1;    //0 to 0.667 CVRSRC, with CVRSRC/24 step size (low range)
    CVRCONbits.CVRSS = 0;   //Comparator reference source, CVRSRC = AVDD ? AVSS
    CVRCONbits.CVR = 0b0000;
    CVRCONbits.CVREN = 1;   //CVREF circuit powered on
}

void CVROn(void)
{
    CVRCONbits.CVREN = 1;
}

void CVROff(void)
{
    CVRCONbits.CVREN = 0;
}

void CVRRange(uint8_t Range)
{
    CVRCONbits.CVRR = Range;
}

void CVRValue(uint8_t Value)
{
    CVRCONbits.CVR = Value;
}