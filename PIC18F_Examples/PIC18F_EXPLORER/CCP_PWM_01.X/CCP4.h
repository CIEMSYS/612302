/* 
 * File:   CCP4.h
 * Author: Electronica
 *
 * Created on 12 de septiembre de 2016, 11:20 PM
 */

#ifndef CCP4_H
#define	CCP4_H

#include <xc.h>
#include <stdint.h>

uint8_t CCP4Flag;

//CCP4 Mode

#define Capture_01_FE   0b0100
#define Capture_01_RE   0b0101
#define Capture_04_RE   0b0110
#define Capture_16_RE   0b0111
#define PWM             0b1111

//Prototipos de función para el CCP4

void CCP4Initialize(unsigned char ModeCCP4); //Configuración inicial CCP4
void CCP4InterruptRoutine(void); //Rutina para la interrupción del CCP4

#endif	/* CCP4_H */