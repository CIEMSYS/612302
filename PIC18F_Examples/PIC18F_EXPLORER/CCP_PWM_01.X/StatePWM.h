/* 
 * File:   StatePWM.h
 * Author: Sebastián Fernando Puente Reyes
 *
 * Created on 17 de mayo de 2017, 01:07 AM
 */

#ifndef STATEPWM_H
#define	STATEPWM_H

#include "SystemInitialize.h"

// Function prototypes
void PWMGenerator(void);
void DispDutyCycle(void);
uint16_t Map(uint16_t x, uint16_t MinRang1, uint16_t MaxRang1, uint16_t MinRang2, uint16_t MaxRang2);

#endif	/* STATEPWM_H */

