/* 
 * File:   SystemInitialize.h
 * Author: Lenovo
 *
 * Created on 17 de mayo de 2017, 11:53 AM
 */

#ifndef SYSTEMINITIALIZE_H
#define	SYSTEMINITIALIZE_H

#include <xc.h>
#include "ConfigurationBits.h"
#include <stdint.h>
#include <stdio.h>
#include "BBSPI_LCD.h"
#include "ADC.h"
#include "TIMER2.h"
#include "CCP4.h"

#define _XTAL_FREQ 40000000

// Definitions for select and scroll buttons
#define scroll_dir  TRISAbits.TRISA5
#define scroll      PORTAbits.RA5
#define select_dir  TRISBbits.TRISB0
#define select      PORTBbits.RB0
#define pressed     0

// Function prototypes
void SysInitialize(void);
void PORTSetUp(void);
void SWdelay(void);

#endif	/* SYSTEMINITIALIZE_H */

