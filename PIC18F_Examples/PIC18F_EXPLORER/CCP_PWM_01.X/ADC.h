/* 
 * File:   ADC.h
 * Author: SEBASTIAN PUENTE REYES
 *
 * Created on 17 de mayo de 2017, 12:07 PM
 */

#ifndef ADC_H
#define	ADC_H

#include <xc.h>

//Macros selecci�n de canal a convertir
#define CH_0   0b0000
#define CH_1   0b0001
#define CH_2   0b0010
#define CH_3   0b0011
#define CH_4   0b0100
#define CH_5   0b0101
#define CH_6   0b0110
#define CH_7   0b0111
#define CH_8   0b1000
#define CH_9   0b1001
#define CH_10  0b1010
#define CH_11  0b1011
#define CH_12  0b1100
#define CH_13  0b1101
#define CH_14  0b1110
#define CH_15  0b1111

//N�mero canales a utlizar (ver bits PCFG)
#define CHS_0   0b1111
#define CHS_1   0b1110
#define CHS_2   0b1101
#define CHS_3   0b0010
#define CHS_4   0b0011
#define CHS_5   0b0100
#define CHS_6   0b0101
#define CHS_7   0b0110
#define CHS_8   0b0111
#define CHS_9   0b1000
#define CHS_10  0b1001
#define CHS_11  0b1010
#define CHS_12  0b1011
#define CHS_13  0b1100
#define CHS_14  0b1101
#define CHS_15  0b1110

//Prototipos funciones
void SetUpADC(unsigned char Channels);
void SetChADC(unsigned char Channel);
int SetChGoADC(unsigned char Channel);
int GoADC(void);
void OnADC(void);
void OffADC(void);

#endif	/* ADC_H */

