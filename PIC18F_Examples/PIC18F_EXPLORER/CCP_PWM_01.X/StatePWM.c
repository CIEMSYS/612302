#include "StatePWM.h"

extern unsigned char State;

/*******************************************************************
* FUNCTION:     PWMGenerator ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  
********************************************************************/
void PWMGenerator(void)
{
    // Display PWM Generator text and Now/Next options on LCD
    mCURSOR_LINE1;
    LCDPutStr(" PWM  Generator ");
    mCURSOR_LINE2;
    LCDPutStr("RB0=Now RA5=Next");

    // Poll the button to be pressed
    while (State == 1)
    {
        if (select == pressed)          // Condition if RB0 is pressed
        {
            SWdelay();
            DispDutyCycle();              // Display PWM
        }
        if (scroll == pressed)          // Condition if RA5 is pressed
        {
            SWdelay();
            State = 0;                    // Switch to Intro State
        }
    }
}

/*******************************************************************
* FUNCTION:     DispDutyCycle ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  
********************************************************************/
void DispDutyCycle(void)
{
    char TextoLCD[16];
    uint16_t ValorPot;
    uint16_t ValorDutyCycle;
    
    PR2Write(249);

    while (State == 1)
    {
        ValorPot = SetChGoADC(CH_0);
        //                             --ADC--  --PWM--
        ValorDutyCycle = Map(ValorPot, 0, 1023, 0, 1023);
        CCPR4L = ValorDutyCycle >> 2;
        CCP4CONbits.DC4B = ValorDutyCycle;
        
        // Display Voltage value and Exit option on LCD
        mCURSOR_LINE1;
        LCDPutStr("Duty Cycle =         ");
        mCURSOR_LINE2;
        sprintf(TextoLCD, "%d", ValorDutyCycle);
        LCDPutStr(TextoLCD);
        LCDPutStr("   RB0=Exit");

        // Poll the RB0 button
        if (select == pressed)              // Condition if RB0 is pressed
        {
            SWdelay();
            State = 0;                        // Switch to Intro State
        }
    }
}

/*******************************************************************
* FUNCTION:     Map()
* INPUTS:       None
* OUTPUTS:      b = Voltage value
* DESCRIPTION:  Converts the ADC result into voltage decimal value
********************************************************************/
uint16_t Map(uint16_t x, uint16_t MinRang1, uint16_t MaxRang1, uint16_t MinRang2, uint16_t MaxRang2)
{
    uint16_t ValorMapeado;
    uint32_t aux;
    
    aux = (uint32_t)(x - MinRang1)*(uint32_t)((MaxRang2 - MinRang2));
    ValorMapeado = (aux/(MaxRang1 - MinRang1)) + MinRang2;
    
    return ValorMapeado;
}