/*
 * File:   main.c
 * Author: SEBASTIÁN FERNANDO PUENTE REYES
 *
 * Created on 17 de mayo de 2017, 11:52 AM
 */

#include "SystemInitialize.h"
#include "StateIntro.h"
#include "StatePWM.h"

// Enumerations
enum {StIntro, StPWM} StateMachine;
unsigned char State = StIntro;

/*
                         Main application
 */
void main(void)
{
    SysInitialize();
    
    while(1)
    {
        switch(State)
        {
            case StIntro: Intro();
            break;
            case StPWM: PWMGenerator();
            break;
        }
    }
}
//------------------------------------------------------------------------------