/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 31 de marzo de 2016, 02:05 PM
 */

#include <xc.h>
#include <stdint.h>
#include "InterruptManager.h"
#include "tmr1.h"
#include "BBSPI_LCD.h"
#include "Clock.h"

#define _XTAL_FREQ 40000000

//Variables globales

//Prototipos de funci�n
void Intro(void);

void main(void)
{
    Intro();
    TMR1Initialize();
    InterruptInitialize();
    
    while(1)
    {
        CountClock();
    }
}

void Intro(void)
{
    // Initialize LCD
    mOPEN_LCD;
    
    // Display Intro text on LCD
    mCURSOR_LINE1;
    LCDPutStr("Reloj en Tiempo Real");
}