/* 
 * File:   Clock.h
 * Author: Sebastián
 *
 * Created on 20 de junio de 2016, 12:46 AM
 */

#ifndef CLOCK_H
#define	CLOCK_H

#include <xc.h>
#include "BBSPI_LCD.h"
#include "tmr1.h"

volatile unsigned char Hr = 0, Min = 0, Sec = 0;

//Prototipos de funciones
void CountClock(void);
void SetClock(void);
void DispClock(void);


#endif	/* CLOCK_H */

