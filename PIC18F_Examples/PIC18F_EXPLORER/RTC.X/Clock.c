
#include "Clock.h"


void CountClock(void)
{
    if(Timer1Flag == 1)              // Condition if Timer1 flags
    {
        Sec++;                      // Increment seconds
        if (Sec > 59)               // Condition if seconds is greater than 59
        {
            Sec = 0;                // Set seconds to 0
            Min++;                  // Increment minutes
        }
        if (Min > 59)               // Condtion if minutes is greater than 59
        {
            Min = 0;                // Set minutes to 0
            Hr++;                   // Increment hours
        }
        if (Hr > 12 && Min == 0 && Sec == 0)    //Condition if clock reaches (13:00:00)
        {
            Hr = 1;                     // Set hour to 1
        }
        DispClock();                   // Display clock
        Timer1Flag = 0;
    }
}

void DispClock(void)
{
    // Display clock on LCD Line 1
    mCURSOR_LINE2;
    LCDPutChar(Hr/10+'0');
    LCDPutChar(Hr%10+'0');
    LCDPutChar(':');
    LCDPutChar(Min/10+'0');
    LCDPutChar(Min%10+'0');
    LCDPutChar(':');
    LCDPutChar(Sec/10+'0');
    LCDPutChar(Sec%10+'0');
    LCDPutStr("        ");   
}