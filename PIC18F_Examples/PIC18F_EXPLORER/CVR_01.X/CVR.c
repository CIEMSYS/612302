
#include "CVR.h"

/*******************************************************************
* FUNCTION:     CVRInitialize()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Config. Inicial del Comparator Voltage Reference Module
********************************************************************/
void CVRInitialize(void)
{
    CVRCONbits.CVREN = 0;   //CVREF circuit powered down
    CVRCONbits.CVROE = 1;   //CVREF voltage level is also output on the RF5/AN10/CVREF pin
    CVRCONbits.CVRR = 1;    //0 to 0.667 CVRSRC, with CVRSRC/24 step size (low range)
    CVRCONbits.CVRSS = 0;   //Comparator reference source, CVRSRC = AVDD ? AVSS
    CVRCONbits.CVR = 0b0000;
    CVRCONbits.CVREN = 1;   //CVREF circuit powered on
    return;
}

/*******************************************************************
* FUNCTION:     CVROn()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Habilitación Comparator Voltage Reference Module
********************************************************************/
void CVROn(void)
{
    CVRCONbits.CVREN = 1;
    return;
}

/*******************************************************************
* FUNCTION:     CVROff()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Deshabilitación Comparator Voltage Reference Module
********************************************************************/
void CVROff(void)
{
    CVRCONbits.CVREN = 0;
    return;
}

/*******************************************************************
* FUNCTION:     CVRRange(uint8_t Range)
* INPUTS:       Range = 1(Low Range) or 0(High Range)
* OUTPUTS:      None
* DESCRIPTION:  Selección del rango del CVR
* 				if CVRR = 1: CVREF = ((CVR3:CVR0)/24) x (CVRSRC)
*				if CVRR = 0: CVREF = (CVRSRC/4) + ((CVR3:CVR0)/32) x (CVRSRC)
********************************************************************/
void CVRRange(uint8_t Range)
{
	//Usar los macros LOW_RANGE o HIGH_RANGE
    CVRCONbits.CVRR = Range;
    return;
}

/*******************************************************************
* FUNCTION:     CVRValue(uint8_t Value)
* INPUTS:       Value = (0 ≤ (CVR<3:0>) ≤ 15)
* OUTPUTS:      None
* DESCRIPTION:  Valor CVR que establece un Voltaje de acuerdo al rango
********************************************************************/
void CVRValue(uint8_t Value)
{
    CVRCONbits.CVR = Value;
    return;
}