/* 
 * File:   StateCVR.h
 * Author: Lenovo
 *
 * Created on 22 de mayo de 2017, 05:14 PM
 */

#ifndef STATECVR_H
#define	STATECVR_H

#include "SystemInitialize.h"

void CVRIntro(void);
void CVRRangeSel(void);
void CVRGenerate(void);

#endif	/* STATECVR_H */

