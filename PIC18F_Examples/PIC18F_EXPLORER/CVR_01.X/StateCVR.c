
#include "StateCVR.h"
#include <stdint.h>

extern unsigned char State;

/*******************************************************************
* FUNCTION:     CVRIntro()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  
********************************************************************/
void CVRIntro(void)
{
    // Display Comparator Voltaje Reference text and Now/Next options on LCD
    mCURSOR_LINE1;
    LCDPutStr("Comp Voltaje Ref");
    mCURSOR_LINE2;
    LCDPutStr("RB0=Now RA5=Next");

    // Poll the button to be pressed
    while (State == 1)
    {
        if (select == pressed)  //Condition if RB0 is pressed
        {
            SWdelay();
            CVRRangeSel();      //Selecci�n del rango del CVR
            CVRGenerate();      //Generar voltajes con el CVR
        }
        if (scroll == pressed)  // Condition if RA5 is pressed
        {
            SWdelay();
            State = 0;          // Switch to Intro application
        }
    }
}

/*******************************************************************
* FUNCTION:     CVRRangeSel()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  
********************************************************************/
void CVRRangeSel(void)
{
    //Se pregunta si se desea el Low Range o High Range del CVR
    mLCD_CLEAR;
    mCURSOR_LINE1;
    LCDPutStr("Range CVR Select");
    mCURSOR_LINE2;
    LCDPutStr("RB0=Low RA5=High");
    
    while(1)
    {
        if (select == pressed)  //Condition if RB0 is pressed
        {
            SWdelay();
            CVRRange(LOW_RANGE);
            break;
        }
        if (scroll == pressed)
        {
            SWdelay();
            CVRRange(HIGH_RANGE);
            break;
        }
    }
}

/*******************************************************************
* FUNCTION:     CVRGenerate()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  
********************************************************************/
void CVRGenerate(void)
{
    char TextoLCD[16];
    uint8_t CVRVal = 0;
    
    mLCD_CLEAR; //Borrar LCD

    while (State == 1)
    {
        CVRValue(CVRVal);
        
        // Display CVR value and Exit option on LCD
        mCURSOR_LINE1;
        LCDPutStr("CVR Value = ");
        sprintf(TextoLCD, "%2d", CVRVal);
        LCDPutStr(TextoLCD);
        mCURSOR_LINE2;
        LCDPutStr("RA5--> RB0=Exit");

        //Poll the button to be pressed
        if (select == pressed)  // Condition if RB0 is pressed
        {
            SWdelay();
            State = 0;          // Switch to Intro application
        }
        if (scroll == pressed)  // Condition if RA5 is pressed
        {
            SWdelay();
            CVRVal++;
            if(CVRVal == 16) CVRVal = 0;
        }
    }
}