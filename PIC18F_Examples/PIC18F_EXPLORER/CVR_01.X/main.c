/*
 * File:   main.c
 * Author: Sebastián F. Puente Reyes
 *
 * Created on 22 de mayo de 2017, 04:31 PM
 */

#include "SystemInitialize.h"
#include "StateIntro.h"
#include "StateCVR.h"

// Enumerations
enum {StIntro, StCVR} StateMachine;
unsigned char State = StIntro;

/*
                         Main application
 */
void main(void)
{
    SysInitialize();
    
    while(1)
    {
        switch(State)
        {
            case StIntro: Intro();
            break;
            case StCVR: CVRIntro();
            break;
        }
    }
}