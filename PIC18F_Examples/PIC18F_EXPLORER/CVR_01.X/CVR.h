/* 
 * File:   CVR.h
 * Author: Lenovo
 *
 * Created on 22 de mayo de 2017, 05:02 PM
 */

#ifndef CVR_H
#define	CVR_H

#include <xc.h>
#include <stdint.h>

#define LOW_RANGE 1
#define HIGH_RANGE 0

//Functions Prototypes
void CVRInitialize(void);
void CVROn(void);
void CVROff(void);
void CVRRange(uint8_t Range);
void CVRValue(uint8_t Value);

#endif	/* CVR_H */

