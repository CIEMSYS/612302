/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 8 de febrero de 2018, 01:04 PM
 */

#include <xc.h>
#include "ConfigurationBits.h"
#include <stdio.h>
#include "USART.h"

//Variables globales

//Prototipos de funci�n
void SystemInitialize(void);

void main(void)
{
    SystemInitialize();
    
    while(1)
    {
        printf("HOLA MUNDO");
    }
    
    return;
}

void SystemInitialize(void)
{
    EUSART1_Initialize();
    return;
}