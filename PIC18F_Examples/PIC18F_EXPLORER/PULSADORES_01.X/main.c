/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 18 de marzo de 2016, 05:26 PM
 */

#include <xc.h>
#include "BitsConfig.h"
#include <stdint.h>

#define _XTAL_FREQ 40000000

#define BotonIzq PORTBbits.RB0
#define BotonIzq_LAT LATBbits.LATB0
#define BotonIzq_TRIS TRISBbits.TRISB0

#define BotonDer PORTAbits.RA5
#define BotonDer_LAT LATAbits.LATA5
#define BotonDer_TRIS TRISAbits.TRISA5

#define Oprimido 0
#define HIGH 1
#define LOW 0
#define LED0 PORTDbits.RD0
#define LED0_LAT LATDbits.LATD0
#define LED0_TRIS TRISDbits.TRISD0


void Initialize(void);

void main(void)
{
    Initialize();
    
    while(1)
    {
        if(BotonDer == Oprimido)
        {
            while(BotonDer == Oprimido) //Anti rebotes
            {
                __delay_ms(1);
            }
            
            LED0 = !LED0;
        }
    }
}

void Initialize(void)
{
    LED0_LAT = 0;
    LED0_TRIS = 0;
    LED0 = 0;
    
    BotonDer_LAT = 0;
    BotonDer_TRIS = 1;
    BotonDer = 0;
    
    BotonIzq_LAT = 0;
    BotonIzq_TRIS = 1;
    BotonIzq = 0;
    
    ADCON1bits.PCFG = 0b1110; //S�lo el canal A0 activado los demas como digitales
}
