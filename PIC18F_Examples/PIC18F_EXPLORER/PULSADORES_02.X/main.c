/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 18 de marzo de 2016, 05:26 PM
 */

#include <xc.h>
#include "BitsConfig.h"
#include <stdint.h>

#define _XTAL_FREQ 10000000

#define BotonIzq PORTBbits.RB0
#define BotonIzq_LAT LATBbits.LATB0
#define BotonIzq_TRIS TRISBbits.TRISB0

#define BotonDer PORTAbits.RA5
#define BotonDer_LAT LATAbits.LATA5
#define BotonDer_TRIS TRISAbits.TRISA5

#define Oprimido 0
#define HIGH 1
#define LOW 0

void Initialize(void);
void SwitchDelay(void);

uint8_t aux = 0x01;

void main(void)
{
    Initialize();
    
    while(1)
    {
        PORTD = aux;
        if(BotonDer == Oprimido)
        {
            SwitchDelay();
            aux = aux>>1;
            if(aux == 0)
                aux = 0b10000000;
        }
        if(BotonIzq == Oprimido)
        {
            SwitchDelay();
            aux = aux<<1;
            if(aux == 0)
                aux = 0b00000001;
        }
    }
}

void Initialize(void)
{
    LATD = 0;
    TRISD = 0;
    
    BotonDer_LAT = 0;
    BotonDer_TRIS = 1;
    BotonDer = 0;
    
    BotonIzq_LAT = 0;
    BotonIzq_TRIS = 1;
    BotonIzq = 0;
    
    ADCON1bits.PCFG = 0b1110; //S�lo el canal A0 activado los demas como digitales
}

void SwitchDelay(void)
{
    while(BotonDer == Oprimido) //Anti rebotes
        __delay_ms(1);
    
    while(BotonIzq == Oprimido)
        __delay_ms(1);
}