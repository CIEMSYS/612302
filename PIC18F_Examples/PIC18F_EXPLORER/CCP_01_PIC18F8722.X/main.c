/*
 * File:   main.c
 * Author: Electronica
 *
 * Created on 12 de septiembre de 2016, 09:58 PM
 */

#include "SystemInitialize.h"
#include "InterruptManager.h"

//Prototipos de funciones
void Intro(void);
void IntroDelay(void);
void Imprimir(void);

//Variables globales
char TextoLCD[16];

uint16_t t1 = 0;
uint16_t t2 = 0;
uint16_t T = 0;
uint8_t aux = 0;

/*
                         Main application
 */
void main(void)
{
    SysInitialize();
    InterruptInitialize();
    Intro();
    mLCD_CLEAR;
    
    while(1)
    {
        if(CCP4Flag)
        {
            PORTDbits.RD0 = !PORTDbits.RD0;
            
            T = CCPR4H;
            T<<=8;
            T+=CCPR4L;
            
            if(aux == 0)
            {
                t1 = T;
                aux = 1;
            }
            else
            {
                t2 = T;
                T = t2 - t1;
                Imprimir();
                aux = 0;
            }
            CCP4Flag = 0;
        }
    }  
}

void Intro(void)
{
    // Initialize LCD
    mOPEN_LCD;   
    // Display Intro text on LCD
    mCURSOR_LINE1;
    LCDPutStr("Modulo CCP");
    mCURSOR_LINE2;
    LCDPutStr("Modo Captura");
    
    IntroDelay(); //Retardo 2 seg
    
    return;
}

void Imprimir(void)
{
    mCURSOR_LINE1;
    sprintf(TextoLCD, "CCP4 = %d", t);
    LCDPutStr(TextoLCD);
    return;
}

//Retardo de 2 seg
void IntroDelay(void)
{
    unsigned char i;
    for (i=0;i<100;i++)
    {__delay_ms(20);}
    
    return;
}