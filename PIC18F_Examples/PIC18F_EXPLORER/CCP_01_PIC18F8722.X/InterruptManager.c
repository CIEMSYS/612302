
#include "InterruptManager.h"

void InterruptInitialize(void)
{
    /* IPEN (RCON<7>): Interrupt Priority Enable bit
     * 1 = Enable priority levels on interrupts
     * 0 = Disable priority levels on interrupts (default)
     */
    RCONbits.IPEN = 0; //Sistema de prioridad de interrupciones deshabilitado
    
    GlobalInterruptEnable();
    PeripheralInterruptEnable();
}

void interrupt InterruptsManager()
{
    if(PIR3bits.CCP4IF == 1)
    {
        CCP4InterruptRoutine();
    }
}