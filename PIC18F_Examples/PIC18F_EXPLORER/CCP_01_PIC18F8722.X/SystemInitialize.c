
#include "SystemInitialize.h"

void SysInitialize(void)
{
    //S�lo el canal AN0 activo, los demas digitales
    ADCON1bits.PCFG = 0b1110;
    scroll_dir = 1;
    select_dir = 1;
    
    LATD = 0;
    TRISDbits.RD0 = 0;
    PORTDbits.RD0 = 1;

    TMR1Initialize();
    CCP4Initialize();
    return;
}

/*******************************************************************
* FUNCTION:     SWdelay ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Delay after pressing the RB0 and RB5 switch buttons
********************************************************************/
void SWdelay (void)
{
    while(scroll == pressed)
        __delay_ms(1);
    while (select == pressed)
        __delay_ms(1);
    
    return;
}