
#include <xc.h>
#include "CCP4.h"

void CCP4Initialize(void)
{
    /* T3CCP<2:1> (T3CON<6,3>): Timer3 and Timer1 to CCPx Enable bits
     * 11 = Timer3 and Timer4 are the clock sources for ECCP1, ECCP2, ECCP3, CCP4 and CCP5
     * 10 = Timer3 and Timer4 are the clock sources for ECCP3, CCP4 and CCP5; Timer1 and Timer2 are the clock sources for ECCP1 and ECCP2
     * 01 = Timer3 and Timer4 are the clock sources for ECCP2, ECCP3, CCP4 and CCP5; Timer1 and Timer2 are the clock sources for ECCP1
     * 00 = Timer1 and Timer2 are the clock sources for ECCP1, ECCP2, ECCP3, CCP4 and CCP5
     */
    T3CONbits.T3CCP1 = 0;
    T3CONbits.T3CCP2 = 0;
    
    //Reset CCP4 module
    CCP4CONbits.CCP4M = 0b0000;
    
    /* CCPxM<3:0> (CCPxCON): CCP Module x Mode Select bits
     * 0000 = Capture/Compare/PWM disabled; resets CCPx module
     * 0001 = Reserved
     * 0010 = Compare mode, toggle output on match; CCPxIF bit is set
     * 0011 = Reserved
     * 0100 = Capture mode, every falling edge
     * 0101 = Capture mode, every rising edge
     * 0110 = Capture mode, every 4th rising edge
     * 0111 = Capture mode, every 16th rising edge
     * 1000 = Compare mode, initialize CCPx pin low; on compare match, force CCPx pin high; CCPxIF bit is set
     * 1001 = Compare mode, initialize CCPx pin high; on compare match, force CCPx pin low; CCPxIF bit is set
     * 1010 = Compare mode, generate software interrupt on compare match; CCPxIF bit is set; CCPx pin reflects I/O state
     * 1011 = Compare mode, trigger special event; CCPxIF bit is set, CCPx pin is unaffected (For the effects of the trigger, see Section 17.3.4 ?Special Event Trigger?.)
     * 11xx = PWM mode
     */
    CCP4CONbits.CCP4M = Capture_01_RE;
    
    // Clear the CCP4 interrupt flag
    PIR3bits.CCP4IF = 0;
    
     /* CCP4IE (PIE3<1>): CCP4 Interrupt Enable bit
     * 1 = Enable
     * 0 = Disable
     */
    PIE3bits.CCP4IE = 1; //Desahibilitada Interrupcion CCP4
    
    //Pin CCP4(RG3) como entrada (Modo Captura)
    LATGbits.LATG3 = 0;
    TRISGbits.RG3 = 1;
    
    return;
}

void CCP4InterruptRoutine(void)
{
    // Clear the CCP4 interrupt flag
    PIR3bits.CCP4IF = 0;

    // Add your TMR0 interrupt custom code
    CCP4Flag = 1;
    
    return;
}
