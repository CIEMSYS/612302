/*
 * File:   main.c
 * Author: Sebastián
 *
 * Created on 18 de junio de 2016, 12:05 AM
 */


#include <xc.h>
#include "BitsConfig.h"
#include "BBSPI_LCD.h"

void main(void)
{
    // Initialize LCD
    mOPEN_LCD;
    
    // Display Intro text on LCD
    mCURSOR_LINE1;
    LCDPutStr("Maria Isabella");
    mCURSOR_LINE2;
    LCDPutStr("Puente Novoa");
    
    while(1);
}
