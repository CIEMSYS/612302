/* 
 * File:   tmr2.h
 * Author: Lenovo
 *
 * Created on 17 de septiembre de 2017, 08:14 PM
 */

#ifndef TMR2_H
#define	TMR2_H

#include <xc.h>
#include <stdint.h>

uint8_t Timer2Flag;

//Macros para los valores del Prescaler del Timer2
#define T2CKPS_1 0b00
#define T2CKPS_4 0b01
#define T2CKPS_16 0b10

//Macros para los valores del Postscaler del Timer2
#define T2OUTPS_1   0b0000
#define T2OUTPS_2   0b0001
#define T2OUTPS_3   0b0010
#define T2OUTPS_4   0b0011
#define T2OUTPS_5   0b0100
#define T2OUTPS_6   0b0101
#define T2OUTPS_7   0b0110
#define T2OUTPS_8   0b0111
#define T2OUTPS_9   0b1000
#define T2OUTPS_10  0b1001
#define T2OUTPS_11  0b1010
#define T2OUTPS_12  0b1011
#define T2OUTPS_13  0b1100
#define T2OUTPS_14  0b1101
#define T2OUTPS_15  0b1110
#define T2OUTPS_16  0b1111

//Prototipos de funciones

void TMR2Initialize(uint8_t Prescale, uint8_t Postscale); //Configuración inicial del Timer2
void TMR2Start(void); //Start Timer2
void TMR2Stop(void); //Stop Timer2
uint8_t TMR2Read(void); //Lectura del valor del Timer2
uint8_t PR2Read(void); //Lectura del valor del PR2
void TMR2Write(uint8_t ValTMR2); //Escribir un valor al registro del Timer2
void PR2Write(uint8_t ValPR2); //Escribir un valor al registro PR2 del Timer2
void TMR2InterruptRoutine(void); //Rutina para la interrupción del Timer2

#endif	/* TMR2_H */