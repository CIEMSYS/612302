#include "tmr2.h"

// *****************************************************************************
// void TMR2Initialize(uint8_t Prescale, uint8_t Postscale)
// Configuración inicial para el Timer2
// -----------------------------------------------------------------------------
void TMR2Initialize(uint8_t Prescale, uint8_t Postscale) //Configuración inicial del Timer2
{
    TMR2Stop();             //Timer2 off
    PIR1bits.TMR2IF = 0;    //Borrado bandera de interrupción
    
    //Timer2 Clock Prescale Select bits
    T2CONbits.T2CKPS = Prescale;
    //Timer2 Output Postscale Select bits
    T2CONbits.TOUTPS = Postscale;

    TMR2Start();
}


void TMR2Start(void) //Start Timer2
{
    T2CONbits.TMR2ON = 1;
}


void TMR2Stop(void) //Stop Timer2
{
    T2CONbits.TMR2ON = 0;
}


uint8_t TMR2Read(void) //Lectura del valor del Timer2
{
    return TMR2;
}


uint8_t PR2Read(void) //Lectura del valor del PR2
{
    return PR2;
}


void TMR2Write(uint8_t ValTMR2) //Escribir un valor al registro del Timer2
{
    TMR2 = ValTMR2;
}


void PR2Write(uint8_t ValPR2) //Escribir un valor al registro PR2 del Timer2
{
    PR2 = ValPR2;
}


void TMR2InterruptRoutine(void) //Rutina para la interrupción del Timer2
{
    // Clear the TMR2 interrupt flag
    PIR1bits.TMR2IF = 0;

    // Add your TMR2 interrupt custom code
    Timer2Flag = 1;
}
