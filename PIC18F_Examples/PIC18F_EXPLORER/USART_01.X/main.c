/*
 * File:   main.c
 * Author: Sammy Guergachi <sguergachi at gmail.com>
 *
 * Created on 28 de noviembre de 2017, 01:27 AM
 */


#include <xc.h>
#include <stdint.h>
#include <stdio.h>
#include "ConfigurationBits.h"


void EUSART1_Initialize(void);
uint8_t EUSART1_Read(void);
void EUSART1_Write(uint8_t txData);


void main(void)
{
    EUSART1_Initialize();
    
    printf("ISABELLA PUENTE NOVOA\n\r");
    printf("MPUS Y MCUS");     
    
    while(1);
    
}


void EUSART1_Initialize(void)
{
    //RECEIVE STATUS AND CONTROL REGISTER
    //Serial Port Enable bit
    RCSTA1bits.SPEN = 1;    //Serial port enabled (configures RXx/DTx and TXx/CKx pins as serial port pins)
    //9-bit Receive Enable bit
    RCSTA1bits.RX9 = 0;     //Selects 8-bit reception
    //Continuous Receive Enable bit
    RCSTA1bits.CREN = 0;    //Disables receiver
    //Address Detect Enable bit
    RCSTA1bits.ADDEN = 0;   //Asynchronous mode 9-bit (RX9 = 0):Don?t care.
    
 
    //TRANSMIT STATUS AND CONTROL REGISTER
    //TX9: 9-bit Transmit Enable bit
    TXSTA1bits.TX9 = 0;     //Selects 8-bit transmission
    //TXEN: Transmit Enable bit
    TXSTA1bits.TXEN = 1;    //Transmit enabled
    //SYNC: EUSART Mode Select bit
    TXSTA1bits.SYNC = 0;    //Asynchronous mode
    //SENDB: Send Break Character bit
    TXSTA1bits.SENDB = 0;   //Asynchronous mode: Sync Break transmission completed
    //BRGH: High Baud Rate Select bit
    TXSTA1bits.BRGH = 1;    //Asynchronous mode: High speed
    
        
    //BRG16: 16-bit Baud Rate Register Enable bit
    BAUDCON1bits.BRG16 = 0; //8-bit Baud Rate Generator ? SPBRGx only (Compatible mode), SPBRGHx value ignored

    // Baud Rate = 9600; SPBRG1 255; 
    SPBRG1 = 0xFF;

    // TXREG 0; 
    TXREG1 = 0x00;

    // RCREG 0; 
    RCREG1 = 0x00;
    
    return;
}
////////////////////////////////////////////////////////////////////////////////
uint8_t EUSART1_Read(void)
{
    while(!PIR1bits.RC1IF)
    {
    }

    if(1 == RCSTA1bits.OERR)
    {
        // EUSART1 error - restart

        RCSTA1bits.CREN = 0;
        RCSTA1bits.CREN = 1;
    }

    return RCREG1;
}
////////////////////////////////////////////////////////////////////////////////
void EUSART1_Write(uint8_t txData)
{
    while(0 == PIR1bits.TX1IF)
    {
    }

    TXREG1 = txData;    // Write the data byte to the USART.
}



char getch(void)
{
    return EUSART1_Read();
}

void putch(char txData)
{
    EUSART1_Write(txData);
}