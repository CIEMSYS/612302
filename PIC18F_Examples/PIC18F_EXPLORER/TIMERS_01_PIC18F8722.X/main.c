/*
 * File:   main.c
 * Author: Electronica
 *
 * Created on 2 de marzo de 2016, 02:43 PM
 */

/* Resumen Proyecto
 * Tipo Oscilador:
 * Oscilador primario
 * Cristal 10MHz y uso de PLL (HSPLL)
 * Fosc = 10MHz * 4 = 40MHz
 * Descripci�n;
 * Se hace uso del Timer0 (16 bit) usando como reloj el ciclo de instrucci�n: Fosc/4 (10MHz)
 * para lograr una temporizaci�n de 500 mseg. Luego, el tiempo de desbordamiento
 * debe ser igual a 500 mseg. Para lograr dicho tiempo el valor inicial a cargar
 * en el Timer0 debe ser de 46005 usandose un pre-escalar de 256.
 * La temporizaci�n es utilizada para manejar el tiempo de incremento de un contador
 * de 8 bits que se podr� visualizar en el PORTD.
 * */

#include <xc.h>
#include "BitsConfig.h"

//Prototipos de funciones
void SetUp(void);       //Configuraci�n inicial
void T0_SetUp(void);    //Funci�n que configura el Timer0
void delay(void);       //Funci�n que entrega un retardo de medio segundo

//Variables globales
char Contador = 0;

//Funci�n Principal
void main(void)
{
    SetUp();
    T0_SetUp();
       
    while(1)
    {
        PORTD = Contador;
        Contador++;
        delay();
    }
    
    return;
}

//Funci�n SetUp
void SetUp(void)
{
   //Configuraci�n Puertos
    TRISD = 0x00; //Todas la l�neas del puerto D como salidas
}

//Funci�n para la configuraci�n del Timer0
void T0_SetUp(void)
{
    T0CONbits.TMR0ON = 0; //Stop timer0
    T0CONbits.T0CS = 0; //Fuente de reloj: ciclo de instrucci�n (Fosc/4 = 10 MHz)
    T0CONbits.PSA = 0; //Pre-escalar activado
    T0CONbits.T08BIT = 0; //Modo 16 bits
    T0CONbits.T0PS = 0b111; //Pre-esacalar a 256. Ti = 25,6 us
    T0CONbits.TMR0ON = 1; //Run timer0
}

//Funci�n para un retardo de 1 seg con el Timer0
/* Para este ejemplo no se utilizar� la interrupci�n. Es decir que para saber si
 * ya se ha desbordado el Timer0 se revisar� cuando TMR0IF (flag bit) se ponga en 1
 * ValorInicialTMR0 = 46005 = 0xB3B5 = 0b1011001110110101
 * TiempoDesbordamientoReal = () * (25,6 E-6 seg) * (65536 - 46005) = 499.99 mseg*/
void delay(void)
{ 
    INTCONbits.TMR0IF = 0; //Borramos el flag bit para que nos pueda indicar el desbordamiento
    TMR0L = 0xB5; //Cargamos la parte baja del valor a cargar en el Timer0
    TMR0H = 0xB3; //Cargamos la parte alta
    while(INTCONbits.TMR0IF == 0); //Esperamos a que el flag bit indique el desbordamiento
    return;
}