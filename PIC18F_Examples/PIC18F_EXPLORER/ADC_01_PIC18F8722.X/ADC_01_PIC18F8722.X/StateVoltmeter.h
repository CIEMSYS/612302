/* 
 * File:   StateVoltmeter.h
 * Author: Lenovo
 *
 * Created on 4 de abril de 2017, 11:42 PM
 */

#ifndef STATEVOLTMETER_H
#define	STATEVOLTMETER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "SystemInitialize.h"

// Function prototypes
void Voltmeter(void);
void DispVolts(void);
float VoltResult(void);


#ifdef	__cplusplus
}
#endif

#endif	/* STATEVOLTMETER_H */

