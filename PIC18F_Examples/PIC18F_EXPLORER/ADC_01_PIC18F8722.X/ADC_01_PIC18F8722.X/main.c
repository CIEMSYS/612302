/*
 * File:   main.c
 * Author: Lenovo
 *
 * Created on 27 de marzo de 2017, 12:46 PM
 */
//Frecuencia de Trabajo Fosc = 40 MHz

#include "SystemInitialize.h"

// Enumerations
enum {StIntro, StVoltmeter} StateMachine;
unsigned char State = StIntro;

/*
                         Main application
 */
void main(void)
{
    SysInitialize();
    
    while(1)
    {
        switch(State)
        {
            case StIntro: Intro();
            break;
            case StVoltmeter: Voltmeter();
            break;
        }
    }
}
//------------------------------------------------------------------------------

