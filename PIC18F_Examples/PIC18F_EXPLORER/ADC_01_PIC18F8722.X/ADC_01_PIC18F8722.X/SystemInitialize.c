
#include "SystemInitialize.h"

void SysInitialize(void)
{
    LATA = 0x00;
    TRISA = 0xF3;

    LATB = 0x00;
    TRISB = 0xFF;
    
    //INTCON2bits.nRBPU = 0x01;
    
    SetUpADC(CHS_1);
}


/*******************************************************************
* FUNCTION:     SWdelay ()
* INPUTS:       None
* OUTPUTS:      None
* DESCRIPTION:  Delay after pressing the RB0 and RB5 switch buttons
********************************************************************/
void SWdelay (void)
{
    while(scroll == pressed)
        __delay_ms(1);
    while (select == pressed)
        __delay_ms(1);
}