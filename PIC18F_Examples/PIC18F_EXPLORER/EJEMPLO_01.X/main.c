/*
 * File:   main.c
 * Author: Sebastián
 *
 * Created on 28 de marzo de 2016, 10:59 AM
 */

#include <xc.h>
#include "BitsConfig.h"

void main(void)

{
    OSCCONbits.IRCF =0b111; //oscilador interno a 8 Mhz
    OSCTUNEbits.PLLEN = 1; //PLL habilitad Fosc = 32 Mhz
    return;
}
