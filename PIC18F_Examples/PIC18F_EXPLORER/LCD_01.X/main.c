/*
 * File:   main.c
 * Author: Sebas Puente Reyes
 *
 * Created on 12 de septiembre de 2017, 09:02 AM
 */

#include <xc.h>
#include "BitsConfig.h"
#include "BBSPI_LCD.h"
#include <stdio.h>  //Libreria para sprintf

void DelayMseg(unsigned int n);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
char TextoLCD[16];
char Mensaje[] = "Imprimir LCD:";
char Caracter = 'A';
int a = 31;
float b = 3.1416;

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    // Initialize LCD
    mOPEN_LCD;
   
    // Display Intro text on LCD
    mCURSOR_LINE1;
    LCDPutStr("  MPUs y MCUs  ");
    mCURSOR_LINE2;
    LCDPutStr(" PIC18 Explorer ");
    
    DelayMseg(3000); //Retardo de 3 segundos
    
    mLCD_CLEAR;
    mCURSOR_OFF;
    mCURSOR_LINE1;
    LCDPutStr(Mensaje);     //Imprimir un string en la l�nea 1 desde la posici�n 0
    mCURSOR_FWD;            //Se desplaza a la derecha el cursor
    LCDPutChar(Caracter);   //Imprimir caracter en la posici�n actual del cursor
    
    //Uso de sprintf para imprimir variables n�mericas
    sprintf(TextoLCD, "a=%d,b=%.3f",a,b);
    mCURSOR_LINE2;
    LCDPutStr(TextoLCD);    //Imprimir el resultado de sprintf en la seguna l�nea
    
    while(1);	//Bucle Infinito
}

/*******************************************************************************
 * Retardo n Milisegundos
 ******************************************************************************/
void DelayMseg(unsigned int n) //n -> milisegundos
{
    unsigned int i;
    for (i=0; i<n; i++)
    {
        __delay_ms(1);
    }
}