/*******************************************************************************
 * Nombre Archivo:		main.c
 * Nombre Proyecto:  	
 * Tema:                
 * Curso:               Dise�o Digital con Microcontroladores PIC de 8 bits 
 * Dependencias:    	Ver INCLUDES section below
 * Procesador:			PIC18F
 * Compilador:			XC8, Ver. 
 * Autor:          		Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Fecha:            	25 de noviembre de 2017, 09:26 AM
 *******************************************************************************
 * DESCRIPCI�N
 * Reloj Principal:
 * 
 * 
 * 
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <stdio.h>
#include "ConfigurationBits.h"
#include "lcd.h"

//#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define HIGH    1
#define LOW     0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
char TextoLCD[16];
char Mensaje[] = "Digitales II";
char Caracter = 'A';
int a = 31;
float b = 3.1416;

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp(); 					//Configuraci�n inicial
    Lcd_Init(); 				//Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF);	//Se apaga el cursor

    Lcd_Out(1,0,Mensaje);			//Imprimir un string en la l�nea 1 desde la posici�n 0
    Lcd_Cmd(LCD_MOVE_CURSOR_RIGHT); //Se desplaza a la derecha el cursor
    Lcd_Chr_CP(Caracter);			//Imprimir caracter en la posici�n actual del cursor 
    //Uso de sprintf para imprimir variables n�mericas
    sprintf(TextoLCD, "a=%d,b=%.3f",a,b);
    Lcd_Out(2,0,TextoLCD);			//Imprimir el resultado de sprintf en la seguna l�nea
    
    while(1);	//Bucle Infinito
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp()
{
    OSCCONbits.IRCF = 0b111;	//Oscilador interno a 16 MHz, Fosc = 16 MHz
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/
