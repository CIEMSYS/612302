/*******************************************************************************
 * Nombre Archivo:		main.c
 * Nombre Proyecto:  	TIMER2_01
 * Tema:                Temporizadores
 * Curso:               Dise�o Digital con Microcontroladores PIC de 8 bits 
 * Dependencias:    	Ver INCLUDES section below
 * Procesador:			PIC18F
 * Compilador:			XC8, Ver. 1.44
 * Autor:          		Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Fecha:            	3 de noviembre de 2017, 10:26 AM
 *******************************************************************************
 * DESCRIPCI�N
 * Reloj Principal: Oscilador Interno a 4 MHz.
 * 
 * 
 * 
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define HIGH    1
#define LOW     0

#define ONDA PORTBbits.RB0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void TMR0Initialize(void);
void TMR2Initialize(void);
void Delay2ms(void);
void Delay8ms(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();
    TMR0Initialize();
    TMR2Initialize();

    while (1) //Ciclo infinito
    {
        ONDA = HIGH;
        Delay2ms();
        ONDA = LOW;
        Delay8ms();
    }
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno si es el caso
    OSCCONbits.IRCF = 0b101;    //Oscilador interno a 4 MHz
    
    //Activaci�n/desactivaci�n canales anal�gicos

    //Puertos digitales
    //Para entradas digitales revisar los registros ANSEL y/o ANSELH
    LATB = 0;
    PORTB = 0;
    TRISBbits.TRISB0 = 0;   //L�nea RB0 como salida digital

    //Otras

    return;
}

/*******************************************************************************
 * FUNCI�N:     TMR0Initialize(void)
 * ENTRADAS:    Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N: Configuraci�n m�dulo Timer0
 ******************************************************************************/
void TMR0Initialize(void)
{
    //Tiempo Incremento (Ti) = (4/Fosc)(Prescaler)
    //Time (8 bits) = (Ti)(256 - ValorInicial)
    //Time (16 bits) = (Ti)(65536 - ValorInicial)
    T0CONbits.TMR0ON = 0;   //Timer0 deshailitado
    T0CONbits.T0CS = 0;     //Reloj: Ciclo Instrucci�n (Fosc/4 = 1 MHz, T = 1 useg)
    T0CONbits.T08BIT = 1;   //Timer0 a 8 bits
    T0CONbits.PSA = 0;      //Pre-escalar activado
    T0CONbits.T0PS = 0b011; //Pre-esacalar 1:16, Ti = 16 useg
    T0CONbits.TMR0ON = 1;   //Timer0 habilitado

    return;
}

/*******************************************************************************
 * FUNCI�N:     TMR2Initialize(void)
 * ENTRADAS:    None
 * SALIDAS:     None
 * DESCRIPCI�N: Configuraci�n m�dulo Timer2
 ******************************************************************************/
void TMR2Initialize(void)
{
	//Time = (PR2+1)(4)(Tosc)(Prescaler)(Postcaler)
	//Se desea una temporizaci�n de 8 mseg, Prescaler = 16, Postcaler = 2
	//Se despeja PR2, PR2 = 249
    T2CONbits.T2CKPS = 0b11;    //Prescaler = 16
    T2CONbits.T2OUTPS = 0b0001;  //Postcaler = 2
    T2CONbits.TMR2ON = 1;       //Timer2 On
    PR2 = 249;
    
    return;
}

/*******************************************************************************
 * FUNCI�N:     Delay2ms()
 * ENTRADAS:    None
 * SALIDAS:     None
 * DESCRIPCI�N: Retardo de 2 mseg a trav�s del Timer0
 ******************************************************************************/
void Delay2ms(void)
{
    //Time = (Ti)(256 - ValorInicial)
    //Time = (16 useg)(256 - 131) = 2000 useg = 2 mseg
    TMR0 = 131; //Valor inicial del registro TMR0
    INTCONbits.TMR0IF = 0; //Borramos el bit que nos indica el desbordamiento
    //Mientras el bit TMR0IF sea 0 significa que a�n no se desborda el TMR0.
    //esperamos a que se ponga en 1 (es decir que pasen los 2 mseg)
    while(INTCONbits.TMR0IF == 0);
    
    return;
}

/*******************************************************************************
 * FUNCI�N:     Delay8ms()
 * ENTRADAS:    None
 * SALIDAS:     None
 * DESCRIPCI�N: Retardo de 8 mseg a trav�s del Timer2
 ******************************************************************************/
void Delay8ms(void)
{
    TMR2 = 0;  //Nos aseguramos que el Timer2 inicie en 0
    PIR1bits.TMR2IF = 0;  //Borramos la bandera que nos indica que TMR2 = PR2
    /*Esperamos que la bandera sea 1 indicando que TMR2 = PR2, lo cual ocurrira
     * en 8 mseg*/
    while(PIR1bits.TMR2IF == 0);
    
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/
