/*******************************************************************************
 * Nombre Archivo:		main.c
 * Nombre Proyecto:  	TIMER0_02
 * Tema:                Temporizadores
 * Curso:               Dise�o Digital con Microcontroladores PIC de 8 bits 
 * Dependencias:    	Ver INCLUDES section below
 * Procesador:			PIC18F45K20
 * Compilador:			XC8, Ver. 1.44
 * Autor:          		Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Fecha:            	Octubre de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Reloj Principal: Interno a 4 MHz
 * Se genera una se�al cuadrada por la l�nea RB0 con un periodo (T) de 800 mseg
 * con un ciclo de trabajo del 75%. Es decir, la se�al se mantiene en estado
 * ALTO por 600 mseg(0.75*800) y luego pasa a estado BAJO por 200 mseg(0.25*800)
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define ONDA PORTBbits.RB0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void Timer0Initialize(void);
void Delay(unsigned int ValorTimer0);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();
    Timer0Initialize();

    while(1) //Ciclo infinito
    {
        //Creamos la se�al digital con T = 800 mseg con ciclo de trabajo de 75%
        ONDA = 1; //Ponemos en alto la l�nea RB0
        Delay(28036); //Esperamos 600 mseg
        ONDA = 0; //Ponemos en bajo la l�nea RB0
        Delay(53036); //Esperamos 200 mseg
    }
    return;
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{  
    //Configuraci�n frecuencia oscilador interno si es el caso
    OSCCONbits.IRCF = 0b101;    //Oscilador interno a 4 MHz
    
    //Activaci�n/desactivaci�n canales anal�gicos

    //Puertos digitales
    LATB = 0;
    PORTB = 0;
    TRISBbits.TRISB0 = 0;   //L�nea RB0 como salida digital

    //Otras
    return;
}

/*******************************************************************************
 * FUNCI�N:     Timer0Initialize()
 * ENTRADAS:    None
 * SALIDAS:     None
 * DESCRIPCI�N: Configuraci�n inicial del Timer0
 ******************************************************************************/
void Timer0Initialize(void)
{
    //Ciclo de instrucci�n = Tcy = 4/4000000 = 1 useg
    T0CONbits.T0CS = 0;     //Modo temporizador
    T0CONbits.T08BIT = 0;   //Timer0 a 16 bits
    T0CONbits.PSA = 0;      //Habilitaci�n pre-escalar
    T0CONbits.T0PS = 0b011; //Pre-esacalar 1:16
    T0CONbits.TMR0ON = 1;   //Timer0 habilitado
    return;
}

/*******************************************************************************
 * FUNCI�N:     Delay()
 * ENTRADAS:    Valor a cargar en el Timer0 (int)
 * SALIDAS:     None
 * DESCRIPCI�N: Carga el Timer0 con el valor adecuado (0 - 65535) para el tiempo
 *              de desbordamiento deseado.
 ******************************************************************************/
void Delay(unsigned int ValorTimer0)
{
    INTCONbits.TMR0IF = 0; //Borramos la bandera de desbordamiento del Timer0
    //Escribir los 16 bits del Timer0. Siempre se debe escribir primero TMR0H
    TMR0H = ValorTimer0>>8;
    TMR0L = ValorTimer0;
    //Esperar a que TMR0IF sea 1, es decir a que se desborde el Timer0 
    while(INTCONbits.TMR0IF == 0);
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/