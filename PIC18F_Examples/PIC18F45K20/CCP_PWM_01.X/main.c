/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     CCP_PWM_01 
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F4550
 * Compiler:        XC8
 * Version:         1.42
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Junio de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Oscilador: Interno a 8 MHz.
 * 
 * 
 ******************************************************************************/
/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <stdio.h>
#include "ConfigurationBits.h"
#include "lcd.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define TRIS_BOTON_A    TRISDbits.TRISD0
#define TRIS_BOTON_B    TRISDbits.TRISD1    
#define BOTON_A         PORTDbits.RD0
#define BOTON_B         PORTDbits.RD1
#define OPRIMIDO        0

#define _XTAL_FREQ 16000000

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SystemInitialize(void);
void SetUpTimer2(void);
void SetUpCCP2(void);
void SWdelay (void);
void DutyCycle(unsigned int Value);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
//               Porcentaje PWM    0% 10% 20%  30%  40%  50%  60%  70%  80%  90%  100%
unsigned int ValoresDutyCycle[] = {0, 80, 160, 240, 320, 400, 480, 560, 640, 720, 800};
unsigned char TextoLCD[16];
signed char aux = 0;  //Variable para direccionar el arreglo ValoresDutyCycle[]

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SystemInitialize();
    SetUpTimer2();
    SetUpCCP2();
    
    Lcd_Init();                     //Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF);        //Se apaga el cursor
    Lcd_Out(1,0,"CCP Modo PWM");    //Imprimir texto en la primer l�nea de la LCD
    
    while(1) //Ciclo infinito
    { 
        if(BOTON_A == OPRIMIDO) //Validaci�n boton A y se incrementa en 10 el % del ciclo util
        {
            SWdelay();          //Antirebotes
            aux++;
            if(aux == 11) aux = 10;
        }
        if(BOTON_B == OPRIMIDO) //Validaci�n boton B, se decrementa en 10 el % del ciclo util
        {
            SWdelay();          //Antirebotes
            aux--;
            if(aux < 0) aux = 0;
        }
        
        DutyCycle(ValoresDutyCycle[aux]);   //Ajuste del Duty Cycle del PWM
        
        //Se imprime en la LCD el porcentaje del ciclo util que se tiene configurado
        sprintf(TextoLCD, "CicloUtil = %3d",aux*10);
        Lcd_Out(2,0,TextoLCD);
        Lcd_Chr_CP('%');
    }
}
/*******************************************************************************
 * FUNCTION:    SystemInitialize()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SystemInitialize(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 16 MHz
    
    TRIS_BOTON_A = 1;   //L�nea de entrada para el boton A
    TRIS_BOTON_B = 1;   //L�nea de entrada para el boton B
    return;
}

/*******************************************************************************
 * FUNCTION:    SetUpTimer2()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n Timer2 para el m�dulo PWM
 ******************************************************************************/
void SetUpTimer2(void)
{
    //Tiempo Incremento (Ti) = (Prescaler)(4/Fosc) = (1)(0.25useg) = 0.25 useg
    T2CONbits.TMR2ON = 0;       //Timer2 off
    T2CONbits.T2CKPS = 0b00;    //Prescaler Timer2 is 1
    T2CONbits.TMR2ON = 1;       //Timer2 on
    return;
}

/*******************************************************************************
 * FUNCTION:    SetUpCCP2()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Configuraci�n del m�dulo CCP2 como PWM
 ******************************************************************************/
void SetUpCCP2(void)
{
    CCP2CONbits.CCP2M = 0b0000;     //CCP2 Reset
    CCP2CONbits.CCP2M = 0b1111;     //CCP2 PWM Mode
    
    //Pin CCP2(RC1) debe ser salida para el modo PWM
    //Teniendo en cuenta que CCP2MX = 1 en los bits de configuraci�n
    LATCbits.LATC1 = 0;
    TRISCbits.TRISC1 = 0;
    
    //**Parametros PWM**
    //Se desea una frecuencia de PWM de 20KHz, Tpwm = 50useg
    //Tpwm = (PR2+1)(Ti), PR2 = (Tpwm/Ti)-1, PR2 = (50/0.25)-1, PR2 = 199
    PR2 = 199;
    //Resoluci�n = log[4(PR2+1)]/log(2) = 9.64386 (2^9.64386 = 800)
    //Rango de 0-800 para el ciclo util entre el 0% y 100% respectivamente.
    //El valor de 0 a 800 se carga en los 10 bits (CCPR2L:CCP2CON<5:4>)
    CCPR2L = 0;
    CCP2CONbits.DC2B = 0;
    return;
}

/*******************************************************************************
 * FUNCTION:    DutyCycle(unsigned int Value);
 * INPUTS:      Valor entre 0 y 800 para el rango de 0% a 100% del ciclo util del PWM
 * OUTPUTS:     None
 * DESCRIPTION: Carga los 10 bits CCPR2L:CCP2CON<5:4> para ajustar el ciclo util del PWM
 ******************************************************************************/
void DutyCycle(unsigned int Value)
{
    CCPR2L = Value >> 2;
    CCP2CONbits.DC2B = Value;
    return;
}

/*******************************************************************************
 * FUNCTION:    SWdelay()
 * INPUTS:      None
 * OUTPUTS:     None
 * DESCRIPTION: Antirebotes para los botones A y B
 ******************************************************************************/
void SWdelay(void)
{
    while(BOTON_A == OPRIMIDO)
        __delay_ms(1);
    while (BOTON_B == OPRIMIDO)
        __delay_ms(1);
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/