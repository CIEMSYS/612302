/*******************************************************************************
 * Nombre Archivo:		main.c
 * Nombre Proyecto:  	ADC_01
 * Tema:                Conversor Anal�gico-Digital
 * Curso:               Dise�o Digital con Microcontroladores PIC de 8 bits 
 * Dependencias:    	Ver INCLUDES section below
 * Procesador:			PIC18F
 * Compilador:			XC8, Ver. 1.44
 * Autor:          		Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Fecha:            	24 de noviembre de 2017, 09:12 AM
 *******************************************************************************
 * DESCRIPCI�N
 * Reloj Principal:
 * 
 * 
 * 
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <stdio.h>  //Libreria para sprintf
#include "ConfigurationBits.h"
#include "lcd.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define HIGH    1
#define LOW     0

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void SetUpADC(void);
unsigned int SetChGoADC(unsigned char Channel);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
char TextoLCD[16];
unsigned int ConvADC0 = 0;
unsigned int ConvADC1 = 0;

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();    //Configuraci�n inicial

    while (HIGH)    //Ciclo infinito
    {
        ConvADC0 = SetChGoADC(0);   //Convertir canal AN0
        __delay_us(2);
        ConvADC1 = SetChGoADC(1);   //Convertir canal AN1
        
        //Conversi�n de un entero de 4 digitos a un string de caracteres
        sprintf(TextoLCD, "Canal AN0 = %.4d", ConvADC0);
        Lcd_Out(1,0,TextoLCD);  //Se imprime en la LCD un string de caracteres
        sprintf(TextoLCD, "Canal AN1 = %.4d", ConvADC1);
        Lcd_Out(2,0,TextoLCD);
    }

    return;
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno si es el caso
    OSCCONbits.IRCF = 0b111; //Oscillador interno, Fosc = 16 MHz
    
    SetUpADC(); //Confguraci�n ADC  
    Lcd_Init(); //Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF);    //Se apaga el cursor

    return;
}

/*******************************************************************************
 * FUNCI�N:		SetUpADC()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n del ADC
 ******************************************************************************/
void SetUpADC(void)
{
    TRISAbits.TRISA0 = 1;   //RA0 como entrada
    TRISAbits.TRISA1 = 1;   //RA1 como entrada
    ANSELbits.ANS0 = 1;     //RA0 como canal anal�gico
    ANSELbits.ANS1 = 1;     //RA1 como canal anal�gico
    
    ADCON2bits.ADCS = 0b101;    //TAD = 1.0 useg (m�n 0.7 useg)
    ADCON2bits.ACQT = 0b001;    //TACQT = 2TAD = 2.0 useg (m�n 1.4 useg)
    ADCON2bits.ADFM = 1;    //Resultado justificado a la derecha
    ADCON1bits.VCFG0 = 0;   //Vref+ = VDD
    ADCON1bits.VCFG1 = 0;   //Vref- = VSS
    
    ADCON0bits.ADON = 1;    //ADC ON
    
    return;
}

unsigned int SetChGoADC(unsigned char Channel)
{
    ADCON0bits.CHS = Channel;   //Selecci�n canal
    ADCON0bits.GO = 1;  //Se lanza la conversi�n
    //Se espera a que se termine la conversi�n (GO/DONE se pone a 0)
    while(ADCON0bits.DONE);
    //Se junta el ADRESH y el ADRESL en un valor de 16 bits y se retorna
    return ((ADRESH << 8) + ADRESL);
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/
