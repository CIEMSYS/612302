/*******************************************************************************
 * Nombre Archivo:		main.c
 * Nombre Proyecto:  	LCD_Y_TECLADO_01
 * Tema:                Visualizaci�n y Captura de Informaci�n
 * Curso:               Dise�o Digital con Microcontroladores PIC de 8 bits 
 * Dependencias:    	Ver INCLUDES section below
 * Procesador:			PIC18F45K20
 * Compilador:			XC8, Ver. 1.44
 * Autor:          		Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Fecha:            	24 de octubre de 2017, 04:01 PM
 *******************************************************************************
 * DESCRIPCI�N
 * Reloj Principal: Interno a 16 MHz
 * 
 * 
 * 
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <stdio.h>
#include "ConfigurationBits.h"
#include "lcd.h"
#include "KeyPad.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define HIGH    1
#define LOW     0

#define _XTAL_FREQ 16000000

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void delayms(unsigned int ms);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
char Mensaje1[] = "Digite Opcion: ";
char Mensaje2[] = "Digite su codigo";
char TeclaOprimida;
char Codigo[9] = {'0','0','0','0','0','0','0','0','0'};

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp(); //Configuraci�n inicial
    Lcd_Init(); //Se inicializa la LCD
    
    Lcd_Cmd(LCD_CURSOR_OFF); //Se apaga el cursor
    Lcd_Out(1,0,Mensaje1);
    TeclaOprimida = GetKey();
    delayms(200);
    Lcd_Chr_CP(TeclaOprimida);
    Lcd_Out(2,0,Mensaje2);

    Lcd_Cmd(LCD_BLINK_CURSOR_ON);

    for(unsigned char i = 0; i < 9; i++) //Se capturan 9 digitos
    {
        Codigo[i] = GetKey();

        if(i == 0)
        {
            Lcd_Cmd(LCD_CLEAR); //Borramos LCD
            Lcd_Out(1,0,"Codigo:");
            Lcd_Cmd(LCD_SECOND_ROW); //Pasamos a la segunda l�nea
        }
        
        if(Codigo[i] == '*') //Con * para corregir
        {
            i = i - 2;
            Lcd_Cmd(LCD_MOVE_CURSOR_LEFT); //Desplazar cursor a la izquierda
        }
        else
            Lcd_Chr_CP(Codigo[i]);
        
        delayms(500);
    }
    Lcd_Cmd(LCD_CURSOR_OFF);

    while(1);
    
    return;
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 16 MHZ

    //Activaci�n/desactivaci�n canales anal�gicos

    //Puertos digitales
    //Para entradas digitales revisar los registros ANSEL y/o ANSELH

    //Otras

    return;
}

/*******************************************************************************
 * FUNCI�N:		delayms()
 * ENTRADAS:	unsigned int ms (milisegundos deseados)
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Crear retardos en mseg a trav�s de __delay_ms()
 ******************************************************************************/
void delayms(unsigned int ms)
{
    for(int n = 0; n < ms; n++)
    {
        __delay_ms(1);
    }
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/
