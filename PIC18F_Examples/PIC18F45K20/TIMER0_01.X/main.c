/*******************************************************************************
 * Nombre Archivo:		main.c
 * Nombre Proyecto:  	TIMER0_01
 * Tema:                Temporizadores
 * Curso:               Dise�o Digital con Microcontroladores PIC de 8 bits 
 * Dependencias:    	Ver INCLUDES section below
 * Procesador:			PIC18F45K20
 * Compilador:			XC8, Ver. 1.44
 * Autor:          		Sebasti�n Fernando Puente Reyes
 * e-mail:          	sebastian.puente@unillanos.edu.co
 * Fecha:            	Octubre de 2017
 *******************************************************************************
 * DESCRIPCI�N
 * Reloj Principal: Oscilador interno a 4 MHz (Fosc = 4 MHz)
 * Se genera una se�al cuadrada por la l�nea RB0 con un periodo (T) de 800 mseg
 * con un ciclo de trabajo del 75%. Es decir, la se�al se mantiene en estado
 * ALTO por 600 mseg(0.75*800) y luego pasa a estado BAJO por 200 mseg(0.25*800)
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);
void SetupTimer0(void);
void delay_5ms(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    SetUp();        //Configuraci�n inicial
    SetupTimer0();  //Se configura el modulo Timer0

    //Generaci�n de la se�al cuadrada con un periodo de 10 mseg por la l�nea RB0
    while(1)
    {
        //Se pone en alto la l�nea RB0 durante 5 mseg
        PORTBbits.RB0 = 1;
        delay_5ms();
        //Se pone en bajo la l�nea RB0 durante 5 mseg
        PORTBbits.RB0 = 0;
        delay_5ms();
    }
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno si es el caso
    OSCCONbits.IRCF = 0b101; //Oscilador interno a 4 MHz

    //Activaci�n/desactivaci�n canales anal�gicos

    //Puertos digitales
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital

    //Otras

    return;
}

/*******************************************************************************
 * FUNCI�N:		SetupTimer0()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n Timer0
 ******************************************************************************/
void SetupTimer0(void)
{
    T0CONbits.T0PS = 0b100; //Pre-escalar = 32
    T0CONbits.PSA = 0; //Pre-escalar activado
    T0CONbits.T0CS = 0; //Modo temporizador seleccionado
    T0CONbits.T08BIT = 1; //Modo 8 bits
    return;
}

/*******************************************************************************
 * FUNCI�N:		delay_5ms()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Retardo de 5 mseg usando el Timer0
 ******************************************************************************/
void delay_5ms(void)
{
    TMR0 = 100; //Valor inicial del registro TMR0
    INTCONbits.TMR0IF = 0; //Borramos el bit que nos indica el desbordamiento
    T0CONbits.TMR0ON = 1; //Iniciamos el modulo Timer0
    /* Mientras el bit TMR0IF sea 0 significa que a�n no se desborda el TMR0.
     * Luego, debemos esperar a que se ponga en 1 (es decir que pasen los 5ms */
    while(INTCONbits.TMR0IF == 0);
    T0CONbits.TMR0ON = 0; //Desactivamos el modulo Timer0
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/