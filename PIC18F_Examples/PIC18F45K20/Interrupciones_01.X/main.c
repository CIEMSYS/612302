/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 27 de octubre de 2015, 02:33 PM
 */

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <stdint.h>
#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define _XTAL_FREQ 16000000 //Macro para __delay_ms(), __delay_us(), etc.

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
uint8_t u8_Contador = 0;
uint8_t u8_Salto = 1;

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    //--Configutaciones Iniciales
    SetUp();

    //Configuraci�n Interrupciones
    RCONbits.IPEN = 0; //No prioridad en las interrupciones
    
    //Habilitaci�n a nivel individual de cada una de las fuentes de interrupci�n
    
    //Se habilita la interrupci�n del puerto B (Cambio en las l�neas RB4, RB5, RB6 y RB7)
    INTCONbits.RBIE = 1;
    //se puede habilatar individualmente cada una de las l�neas anteriores para la interrupcci�n del PORTB
    //a trav�s del los 4 bits m�s siginificativos (IOCB7,IOCB6,IOCB5,IOCB4) del registro IOCB
    IOCB = 0xF0; //Todas las l�neas RB<7:4> habilitadas para la interrupci�n del PORTB
    
    //Se habilita la interrupcion externa 0 (cambio por flanco en RB0)
    INTCONbits.INT0IE = 1; 
    INTCON2bits.INTEDG0 = 1; //interrupci�n externa 0 ser� por flanco de subida (cambio de 0 a 1)
    
    INTCONbits.GIE = 1; //Interrupciones habilitadas a nivel global
    
    while(1) //Ciclo Infinito
    {
        PORTD = u8_Contador; //La variable contador es sacada por las 8 l�neas del puerto B
        __delay_ms(100);
        __delay_ms(100);
        __delay_ms(100);
        u8_Contador = u8_Contador + u8_Salto;
    }
}

/*******************************************************************************
 * FUNCI�N:		SetUp()
 * ENTRADAS:	Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N:	Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno (si es el caso)
    OSCCONbits.IRCF = 0b111; //HFINTOSC = 16 MHz, Fosc = 16 MHz
    
    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports
    LATB = 0; //Inicializar PORTA
    LATD = 0; //Inicializar PORTB

    //--B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
    //(Seccion 10.7 Datasheet: Port Analog Control)
    //ANSEL  --> ANS7(RE2),ANS6(RE1),ANS5(RE0),ANS4(RA5),ANS3(RA3),ANS2(RA2),ANS1(RA1),ANS0(RA0)
    //ANSELH --> ANS12(RB0),ANS11(RB4),ANS10(RB1),ANS9(RB3),ANS8(RB2)
    //S�lo es necesaria cuando se desee configurar dichos pines como entradas digitales
    //ANSELbits.ANS0 = 0; //Habilitaci�n buffer entrada digital de RA0 (Ejemplo)
    ANSELH = 0x00; //Habilitaci�n buffers de entrada lineas RB0,RB1,RB2,RB3 y RB4

    //--B.3--Sentido I/O Ports
    TRISB = 0xFF; //L�neas del PORTB como entradas
    TRISD = 0x00; //L�neas del PORTD como salidas

    //---C---Otras
    //Conexi�n resistencias de pull-up en cada una de las l�neas del PORTB
    WPUB = 0xFF;
    //Habilitaci�n de las resistencias de pull-up conectadas
    INTCON2bits.NOT_RBPU = 0;
    
    return;
}

//Funci�n para el manejo de las interrupciones
void interrupt ISR()
{
    if(INTCONbits.RBIF == 1)
    {
        PORTB = PORTB;
        INTCONbits.RBIF = 0; //Borramos la bandera de interrupcion del PORTB
        u8_Contador = 0;
        PORTD = u8_Contador;
        u8_Salto = 1;
    }
    if(INTCONbits.INT0IF == 1)
    {
        INTCONbits.INT0IF = 0;
        u8_Contador = 0;
        PORTD = u8_Contador;
        u8_Salto = 2;
    }
    else
        Nop();
}
    
 /*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/