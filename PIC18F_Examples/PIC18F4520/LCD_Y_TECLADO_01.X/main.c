/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 15 de mayo de 2015, 02:00 PM
 */

#include <xc.h>
#include <stdio.h>
#include "BitsConfiguracion.h"
#include "lcd.h"
#include "KeyPad.h"

#define _XTAL_FREQ 8000000

//******************************************************************************
//Prototipos de funciones
void SetUp(void);
void delayms(unsigned int ms);

//******************************************************************************
//Variables globales
char Mensaje1[] = "Digite Opcion: ";
char Mensaje2[] = "Digite su codigo";
char TeclaOprimida;
char Codigo[9] = {'0','0','0','0','0','0','0','0','0'};

//******************************************************************************
//Funci�n principal (main)
void main(void)
{
    SetUp(); //Configuraci�n inicial
    Lcd_Init(); //Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF); //Se apaga el cursor

    Lcd_Out(1,0,Mensaje1);
    TeclaOprimida = GetKey();
    delayms(200);
    Lcd_Chr_CP(TeclaOprimida);
    Lcd_Out(2,0,Mensaje2);

    Lcd_Cmd(LCD_BLINK_CURSOR_ON);

    for(unsigned char i = 0; i < 9; i++) //Se capturan 9 digitos
    {
        Codigo[i] = GetKey();

        if(i == 0)
        {
            Lcd_Cmd(LCD_CLEAR); //Borramos LCD
            Lcd_Out(1,0,"Codigo:");
            Lcd_Cmd(LCD_SECOND_ROW); //Pasamos a la segunda l�nea
        }
        
        if(Codigo[i] == '*') //Con * para corregir
        {
            i = i - 2;
            Lcd_Cmd(LCD_MOVE_CURSOR_LEFT); //Desplazar cursor a la izquierda
        }
        else
            Lcd_Chr_CP(Codigo[i]);
        
        delayms(500);
    }
    Lcd_Cmd(LCD_CURSOR_OFF);

    while(1);
    
    return;
}
//******************************************************************************
//Desarrollo funci�n SetUp(). Configura Fosc y puertos
void SetUp()
{
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
}
//******************************************************************************
//Funci�m delayms
void delayms(unsigned int ms)
{
    for(int n = 0; n < ms; n++)
    {
        __delay_ms(1);
    }
    return;
}
//******************************************************************************
//Programaci�n en C de Microcontroladores PIC de 8 bits con MPLAB XC8
//Sebasti�n Puente Reyes, M.Sc.
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************