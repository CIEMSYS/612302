/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 5 de marzo de 2015, 02:37 PM
 */

//Directivas de preprocesamiento
#include <xc.h> //Libreria siempre necesaria que contiene toda la informacion del microcontrolador utilizado
#include <stdint.h>
#include "BitsConfiguracion.h"

//Macros
#define _XTAL_FREQ 4000000 //Macro para el correcto funcionamiento del retardo __delay_ms(). Se pone la frecuencia de trabajo

//Prototipos de funciones
void SetUp(void);

//Variables globales
unsigned char Contador = 0;

//uint8_t Contador = 0;

//Funci�n principal
void main(void)
{
    //Variables locales a la funci�n main()
    uint16_t i;

    SetUp(); //Llamado funci�n SetUp()

    while(1)
    {
        for( i = 0; i < 256; i++)
        {
            Contador = i;
            PORTA = Contador;
            __delay_ms(100);
            __delay_ms(100);
        }
    }

    return;
}
