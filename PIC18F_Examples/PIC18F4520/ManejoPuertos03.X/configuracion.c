#include <xc.h>

//Desarrollo función SetUp
void SetUp(void)
{
    //Configuración frecuencia oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz

    //Configuración puertos digitales
    ADCON1bits.PCFG = 0b1111; //Todos los canales analógicos desactivados
    TRISA = 0x00; //Todas las líneas del puerto A como salidas digitales

    return;
}