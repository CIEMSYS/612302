/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 14 de mayo de 2015, 01:51 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"
#include <stdio.h> //Libreria para sprintf
#include "lcd.h"

//******************************************************************************
//Prototipos de funciones
void SetUp(void);

//******************************************************************************
//Funci�n principal (main)
void main(void)
{
    char TextoLCD[16];
    char Mensaje[] = "Digitales II";
    char Caracter = 'A';
    int a = 31;
    float b = 3.1416;
   
    SetUp(); //Configuraci�n inicial
    Lcd_Init(); //Se inicializa la LCD
    Lcd_Cmd(LCD_CURSOR_OFF); //Se apaga el cursor

    Lcd_Out(1,0,Mensaje); //Imprimir un string en la l�nea 1 desde la posici�n 0
    Lcd_Cmd(LCD_MOVE_CURSOR_RIGHT); //Se desplaza a la derecha el cursor
    Lcd_Chr_CP(Caracter); //Imprimir caracter en la posici�n actual del cursor
    
    //Uso de sprintf para imprimir variables n�mericas
    sprintf(TextoLCD, "a=%d,b=%.3f",a,b);
    Lcd_Out(2,0,TextoLCD); //Imprimir el resultado de sprintf en la seguna l�nea
    
    while(1);

    return;
}
//******************************************************************************
//Desarrollo funci�n SetUp(). Configura Fosc y puertos
void SetUp()
{
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
}
//******************************************************************************
//Programaci�n en C de Microcontroladores PIC de 8 bits con MPLAB XC8
//Sebasti�n Puente Reyes, M.Sc.
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************