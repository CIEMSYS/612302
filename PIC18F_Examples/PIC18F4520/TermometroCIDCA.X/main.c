/*******************************************************************************
 * File:   main.c
 * Author: Sebastian Puente Reyes
 * e-mail: sebastian.puente@unillanos.edu.co
 * Universidad de los Llanos
 * Villavicencio-Meta, Colombia
 * Created on 25 de abril de 2015, 10:29 PM
 ******************************************************************************
 * DESCRIPCI�N: TermometroCIDCA
 * Oscilador: Interno a 8 MHz.
 * Se visualiza la temperatura ambiente sensada por el sensor de temperatura
 * LM35. El LM35 entrega un voltaje el cual es proporcional a la temperatura
 * ambiente (10mV/�C). Se utiliza el canal 0 del ADC para capturar dicho voltaje
 * y realizar la conversi�n a digital. Luego, se obtiene el valor de temperatura
 * (�C) para posteriormente visualizarse en dos Displays 7 segmentos, los cuales
 * se conectan y multiplexan por el PORTD. Adicionalmente, se puede establecer
 * una temperatura de referencia a trav�s de dos pulsadores conectados en RB4 y
 * RB5 (se trabaja la interrupci�n del PORTB), para luego compararla con la
 * temperatura medida y entregar un nivel ALTO por RC3 cuando �sta sea  >= a la
 * temperatura de referencia. De lo contrario, se entrega un nivel BAJO.
 ******************************************************************************/

#include <xc.h>
#include "BitsConfiguracion.h"
#include <math.h>

//******************************************************************************
//Macros
#define _XTAL_FREQ 8000000 //Macro para el retardo __delay_ms(). Se pone la frecuencia de trabajo

//TRIS y PORT para el habilitador del display unidades
#define TRIS_EN_DISP_UNIDADES TRISBbits.TRISB0
#define EN_DISP_UNIDADES PORTBbits.RB0

//TRIS y PORT para el habilitador del display de decenas
#define TRIS_EN_DISP_DECENAS TRISBbits.TRISB1
#define EN_DISP_DECENAS PORTBbits.RB1

//TRIS y PORT para el indicador cuando TempMedida >= TempRef
#define TRIS_INDICADOR_TEMP TRISCbits.TRISC3
#define INDICADOR_TEMP PORTCbits.RC3

//TRIS y PORT para los interruptores TEMP_MAS y TEMP_MENOS
#define TRIS_TEMP_MAS TRISBbits.TRISB4
#define TEMP_MAS PORTBbits.RB4
#define TRIS_TEMP_MENOS TRISBbits.TRISB5
#define TEMP_MENOS PORTBbits.RB5

//TRIS y PORT donde se conectan los dos displays 7 segmentos
#define TRIS_DISPLAYS TRISD
#define DISPLAYS PORTD
//Macro para el n�mero (0-9) en formato 7 segmentos que se envia a los displays
#define SEND_TO_DISPLAYS(x) DISPLAYS = Disp7SegCC[x]

#define LM35 0

//******************************************************************************
//Prototipos de funciones
void SetUp(void);
void PrintTwoDisp(unsigned char Numero);
void SetUp_ADC(void);
unsigned int GetADC(unsigned char CanalADC);
void MedirTemperatura(void);

//******************************************************************************
//Variables globales
//Arreglo para el manejo de un display 7 segmentos de c�todo com�n
//                    0    1    2    3    4    5    6    7    8    9
char Disp7SegCC[] = {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};
unsigned int ConversionADC = 0; //Almacenar 10 bits de la conversi�n A/D
float TempMedida = 0; //Temperatura medida
unsigned char TempRef = 35; //Temperatura para monitorear

//******************************************************************************
//Funci�n main()
void main(void)
{
    SetUp(); //Configuraci�n inicial
    SetUp_ADC(); //Configuraci�n ADC
   
    while(1)
    {
        MedirTemperatura(); //Se obtiene la temperatura en �C del sensor
        PrintTwoDisp((unsigned char)TempMedida); //Se muestra en los displays
        if(TempMedida >= TempRef)
            INDICADOR_TEMP = 1;
        else
            INDICADOR_TEMP = 0;
    }
    return;
}
//******************************************************************************
//Desarrollo funci�n SetUp(). Configura Fosc y puertos
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    
    //Configuraci�n puertos E/S
    TRIS_EN_DISP_UNIDADES = 0; //Salida para activar el Display Unidades
    TRIS_EN_DISP_DECENAS = 0; //Salida para activar el Display Decenas
    TRIS_TEMP_MAS = 1; //Entrada para el interruptor TempMas
    TRIS_TEMP_MENOS = 1; //Entrada para el interruptor TempMenos
    
    //Conexi�n de los displays. Se conectan en el PORT definido en los macros
    //TRIS_DISPLAYS y DISPLAYS. Si por ejemplo se elige el PORTD entonces:
    //RD0->a, RD1->b, RD2->c, RD3->d, RD4->e, RD5->f, RD6->g, RD0-> NC
    TRIS_DISPLAYS = 0x00;

    //Salida que se pondr� en alto cuando TempMedida es >= a TempRef
    TRIS_INDICADOR_TEMP = 0;
    
    //Configuraci�n Interrupciones
    RCONbits.IPEN = 0; //Sistema prioridad de interrupciones deshabilitado
    INTCONbits.GIE = 1; //Interrupciones habilitadas a nivel global
    INTCONbits.RBIE = 1; //Interrupci�n por cambio en el PORTB<7:4> habilitada

    return;
}
//******************************************************************************
//Desarrollo funci�n MedirTemperatura.
void MedirTemperatura()
{
    ConversionADC = GetADC(LM35); //Lectura del sensor LM35
    TempMedida = ((float)ConversionADC) * 0.48828;
    TempMedida = round(TempMedida);
    return;
}
//******************************************************************************
//Desarrollo funci�n SetUp_ADC(). Configura el conversor A/D
void SetUp_ADC()
{
    //Configuraci�n canales anal�gicos a utilizar
    ADCON1bits.PCFG = 0b1110; //Se trabajar� con el canal AN0(pin 2) los demas canales quedan deshabilitados y se podran trabajar como digitales
    //Configuraci�n  tensi�n de referencia
    ADCON1bits.VCFG = 0b00; //Vref- = Vss, Vref+ = Vdd (0v - 5v)
    //Configuraci�n reloj de conversi�n TAD
    ADCON2bits.ADCS = 0b001; //8*Tosc = 1us (TAD debe ser m�nimo 0,7us)
    //Configuraci�n del tiempo de adquisici�n
    ADCON2bits.ACQT = 0b001; //TACQ = 2TAD = 2us (seg�n Datasheet TACQ min debe ser 1,4us)
    //Configuraci�n del modo de almacenamiento de la conversi�n
    ADCON2bits.ADFM = 1; //Justificado a derechas
    //Activaci�n del conversor
    ADCON0bits.ADON = 1; //Conversor A/D activado

    return;
}
//******************************************************************************
//Funci�n GetADC. Se hace la conversi�n del canal 'CanalADC'
unsigned int GetADC(unsigned char CanalADC)
{
    ADCON0 = (CanalADC << 2) | (ADCON0 & 0b11000011); //Fijar canal a convertir
    ADCON0bits.GO = 1; //Inicio conversi�n
    //Bucle de espera de final de la conversi�n A/D
    while(ADCON0bits.DONE); //Comprobar q' el bit GO/DONE se ponga a '0'
    return (((unsigned int)ADRESH)<<8)|(ADRESL);
}
//******************************************************************************
//Desarrollo funci�n PrintTwoDisp
//Esta funci�n se encarga de visualizar en dos display 7 segmentos (multiplexados)
//un valor n�merico de 0 a 99 el cual se encuentra en el argumento "Numero"
void PrintTwoDisp(unsigned char Numero)
{
    unsigned char Unidades, Decenas = 0;

    if(Numero > 9)
    {
        Unidades = Numero%10;
        Decenas = Numero/10;
    }
    else
    {
        Decenas = 0;
        Unidades = Numero;
    }
    
    //Se muestra "Numero" en los dos displays por un lapso de tiempo
    for(char n = 0; n < 31; n++) 
    {
        EN_DISP_UNIDADES = 1;
        EN_DISP_DECENAS = 0;
        SEND_TO_DISPLAYS(Unidades);
        __delay_ms(8);
        EN_DISP_UNIDADES = 0;
        EN_DISP_DECENAS = 1;
        SEND_TO_DISPLAYS(Decenas);
        __delay_ms(8);
    }
    return;
}
//******************************************************************************
//Funci�n para el manejo de interrupciones
void interrupt PortBInt()
{
    //La interrupci�n (PORTB<7:4>) caus� la interrupci�n?
    if(INTCONbits.RBIF == 1)
    {
        PORTB = PORTB; //Ver DataSheet, donde se establece la necesidad de acceder al  PORTB
        INTCONbits.RBIF = 0; //Se borra el flag de interrupci�n
        //__delay_ms(10);

        PrintTwoDisp(TempRef); //Se muestra la temperatura de referencia actual

        if(TEMP_MAS == 1) TempRef = TempRef + 1;
        else
        {
            if(TEMP_MENOS == 1) TempRef = TempRef - 1;
        }
        PrintTwoDisp(0);
    }
    else
        Nop();
}
//******************************************************************************
//Sebasti�n Puente Reyes, M.Sc.
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************