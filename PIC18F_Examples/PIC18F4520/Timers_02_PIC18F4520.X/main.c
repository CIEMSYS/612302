/*******************************************************************************
 * File:   main.c
 * Author: Sebastian Puente Reyes
 * e-mail: sebastian.puente@unillanos.edu.co
 * Universidad de los Llanos
 * Villavicencio-Meta, Colombia
 * Created on 21 de abril de 2015, 03:16 PM
 ******************************************************************************
 * DESCRIPCI�N: Timer0_03
 * Oscilador: Interno a 4 MHz.
 * Se genera una se�al cuadrada por la l�nea RB0 con un periodo (T) de 800 mseg
 * con un ciclo de trabajo del 75%. Es decir, la se�al se mantiene en estado
 * ALTO por 600 mseg(0.75*800) y luego pasa a estado BAJO por 200 mseg(0.25*800)
 ******************************************************************************/

//******************************************************************************
//Librerias
#include <xc.h>
#include "BitsConfiguracion.h"

//******************************************************************************
//Macros
#define ONDA_CUADRADA PORTBbits.RB0

//******************************************************************************
//Prototipos de funciones
void SetUp(void);
void Delay(unsigned int ValorTimer0);

//******************************************************************************
//Funci�n Principal
void main(void)
{
    SetUp();

    while(1) //Ciclo infinito
    {
        //Creamos la se�al digital con T = 800 mseg con ciclo de trabajo de 75%
        ONDA_CUADRADA = 1; //Ponemos en alto la l�nea RB0
        Delay(28036); //Esperamos 600 mseg
        ONDA_CUADRADA = 0; //Ponemos en bajo la l�nea RB0
        Delay(53036); //Esperamos 200 mseg
    }
    return;
}
//******************************************************************************
//Funci�n delay
void Delay(unsigned int ValorTimer0)
{
    //Esta funci�n carga el Timer0 con el valor adecuado (0 - 65535) para el
    //tiempo de desbordamiento deseado
    INTCONbits.TMR0IF = 0; //Borramos la bandera de desbordamiento del Timer0
    //Escribimos los 16 bits del Timer0. Siempre se debe escribir primero TMR0H
    TMR0H = ValorTimer0>>8;
    TMR0L = ValorTimer0;
    while(INTCONbits.TMR0IF == 0); //Esperamos a que la bandera TMR0IF sea 1
    return;
}
//******************************************************************************
//Funci�n setup
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital

    //Configuraci�n Timer0
    //Ciclo de instrucci�n = Tcy = 4/4000000 = 1 useg
    T0CONbits.T0CS = 0; //Timer0 en modo temporizador
    T0CONbits.T08BIT = 0; //Timer0 a 16 bits
    T0CONbits.PSA = 0; //Habilitamos el  pre-escalar
    T0CONbits.T0PS = 0b011; //Pre-esacalar 1:16
    T0CONbits.TMR0ON = 1; //Timer0 habilitado

    return;
}
//******************************************************************************
//Sebasti�n Puente Reyes, M.Sc.
//Programa de Igenier�a Electr�nica
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************