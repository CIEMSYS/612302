/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 27 de abril de 2016, 10:42 PM
 */
//******************************************************************************
//DESCRIPCI�N
//Para este ejemplo se utilizar� el m�dulo Timer0 en modo temporizador a 8 bits.
//El objetivo es generar una se�al digital (onda cuadrada) con una frecuencia de
//5 KHz (5000 Hz) con un ciclo de trabajo del 50% por la l�nea RB0. Es decir que
//la se�al dura en estado bajo el mismo tiempo que dura en estado alto.
//El periodo ser� de 1/5000 seg (200 useg). Luego, como el ciclo de trabajo es
//del 50% entonces esta se�al debe permanecer en estado alto 100 useg y en
//estado bajo el mismo tiempo.
//******************************************************************************

#include <xc.h>
#include "BitsConfig.h"

//******************************************************************************
#define ONDA_CUADRADA PORTBbits.RB0

//******************************************************************************
void SetUp(void);
void Delay(void);

//******************************************************************************
void main(void)
{
    SetUp();
    
    while(1) //Ciclo infinito
    {
        //Creamos la se�al digital de 5000 Hz por la l�nea RB0
        ONDA_CUADRADA = 0; //Ponemos en bajo la l�nea RB0
        Delay(); //Esperamos 100 useg
        ONDA_CUADRADA = 1; //Ponemos en alto la l�nea RB0
        Delay(); //Esperamos 100 useg       
    }
}

//******************************************************************************
void Delay(void)
{
    //Para un tiempo de desbordamiento de 100 useg se calcula que el valor a
    //cargar en el registro TMR0L debe ser de 156 sin usar pre-escalar
    INTCONbits.TMR0IF = 0; //Borramos la bandera de desbordamiento del Timer0
    TMR0L = 156; //Valor inicial calculado para el Timer0
    while(INTCONbits.TMR0IF == 0); //Esperamos a que la bandera TMR0IF sea 1
    return;
}
//******************************************************************************
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital

    //Configuraci�n Timer0
    //Ciclo de instrucci�n = Tcy = 4/4000000 = 1 useg
    T0CONbits.T0CS = 0; //Timer0 en modo temporizador
    T0CONbits.T08BIT = 1; //Timer0 a 8 bits
    T0CONbits.PSA = 1; //Deshabilitamos el  pre-escalar
    T0CONbits.TMR0ON = 1; //Timer0 habilitado

    return;
}
//******************************************************************************
//Sebasti�n Puente Reyes, M.Sc.
//Programa de Igenier�a Electr�nica
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************
