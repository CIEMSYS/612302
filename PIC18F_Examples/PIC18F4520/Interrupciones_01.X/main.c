/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 27 de octubre de 2015, 02:33 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"
#define _XTAL_FREQ 4000000

char Contador = 0;
char Salto = 1;

void main(void)
{
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    TRISD = 0; //todas las l�neas del puerto D como salidas digitales
    INTCON2bits.RBPU = 0; //resistencias de pull-up de todas las l�neas del puerto B
    
    //Configuraci�n interrupciones
    RCONbits.IPEN = 0; //No prioridad en las interrupciones
    
    //Habilitaci�n a nivel individual de cada una de las fuentes de interrupci�n
    INTCONbits.RBIE = 1; //Se habilita la interrupci�n del puerto B (Cambio en las l�neas RB4, RB5, RB6 y RB7)
    INTCONbits.INT0IE = 1; //Se habilita la interrupcion externa 0 (cambio por flanco en RB0)
    INTCON2bits.INTEDG0 = 1; //interrupci�n externa 0 ser� por flanco de subida (cambio de 0 a 1)
    
    INTCONbits.GIE = 1; //Interrupciones habilitadas a nivel global
    
    while(1) //Ciclo Infinito
    {
        PORTD = Contador; //La variable contador es sacada por las 8 l�neas del puerto B
        __delay_ms(100);
        __delay_ms(100);
        __delay_ms(100);
        Contador = Contador + Salto;
    }
}

//Funci�n para el manejo de las interrupciones
    void interrupt interrupciones()
    {
        if(INTCONbits.RBIF == 1)
        {
            PORTB = PORTB;
            INTCONbits.RBIF = 0; //Borramos la bandera de interrupcion del PORTB
            Contador = 0;
            PORTD = Contador;
            Salto = 1;
        }
        if(INTCONbits.INT0IF == 1)
        {
            INTCONbits.INT0IF = 0;
            Contador = 0;
            PORTD = Contador;
            Salto = 2;
        }
        else
            Nop();
    }
