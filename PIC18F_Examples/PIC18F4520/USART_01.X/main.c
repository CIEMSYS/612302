/*
 * File:   main.c
 * Author: Sebasti�n
 *
 * Created on 7 de junio de 2016, 09:02 PM
 */

#include <xc.h>
#include <stdio.h>
#include "BitsConfig.h"
#include "KeyPad.h"
#include "lcd.h"

//Macros
#define _XTAL_FREQ 8000000

//Prototipos funciones
void SetUp(void);
void EUSARTInitialize(void);

//Variables globales
char Mensaje1[] = "Prueba USART";

//Funci�n principal
void main(void)
{
    SetUp();
    Lcd_Init();
    Lcd_Out(1,0,Mensaje1);
    while(1);
}
//******************************************************************************
void SetUp()
{
    OSCCONbits.IRCF = 0b111; //Oscilador interno 8 MHz, FOSC = 8 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos deshabilitados
    
    //Interrupciones
    RCONbits.IPEN = 0; //No prioridad en las interrupciones
    PIE1bits.TXIE = 1; //Habilitada interrupci�n para tx
    INTCON
}

//******************************************************************************
void EUSARTInitialize()
{
    RCSTAbits.SPEN = 1; //Bit de activaci�n del puerto serie
    
    TRISCbits.TRISC7 = 1; //Pin RC7 como entrada (pin RX)
    TRISCbits.TRISC6 = 0; //Pin RC6 como salida (pin TX)
    
    TXSTAbits.SYNC = 0; //Modo as�ncrono
    
    TXSTAbits.BRGH = 0; //Tasa baudios en baja velocidad
    BAUDCONbits.BRG16 = 0; //Generador tasa de baudios a 8 bits
    SPBRG = 12; //Valor a cargar para obtener 9600 baudios (reales 9615)
}