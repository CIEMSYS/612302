/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 4 de marzo de 2015, 01:46 PM
 */

#include <xc.h>

//Prototipos de funciones
void setup(void); //Funci�n llamada setup sin valor de retorno y sin parametros

//Funci�n principal
void main(void)
{
    setup(); //Llamado a la funci�n setup()

    while(1) //Bucle infinito
    {
        //Se le asigna a la salida RB1 lo que hay presente en la entrada RB0
        PORTBbits.RB1 = PORTBbits.RB0;
    }
    return;
}

//Desarrollo funci�n setup()
void setup(void)
{
    //Seleccionamos la frecuencia del oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4MHz

    //Configuraci�n puertos
    ADCON1bits.PCFG = 0b1111; //Todos los canales analogicos deshabilitados
    TRISBbits.TRISB0 = 1; //L�nea RB0 como entrada digital
    TRISBbits.TRISB1 = 0; //L�nea RB1 como salida digital

    return;
}