/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 21 de marzo de 2015, 08:25 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"

//------------------------------------------------------------------------------
//Macros
#define _XTAL_FREQ 8000000 //Macro para el correcto funcionamiento del retardo __delay_ms(). Se pone la frecuencia de trabajo
#define InA PORTBbits.RB4 //Macro para el interuptor A
#define InB PORTBbits.RB5 //Macro para el interuptor B
#define DISP_UNIDADES PORTBbits.RB0 //Macro para el display de unidades
#define DISP_DECENAS PORTBbits.RB1 //Macro para el display de decenas
#define DISP(x) PORTD = Disp7SegCC[x]

//------------------------------------------------------------------------------
//Prototipos de funciones
void SetUp();
void PrintTwoDisp(unsigned char Valor);

//------------------------------------------------------------------------------
//Variables globales
//Arreglo para el manejo de un display 7 segmentos de c�todo com�n
//                    0    1    2    3    4    5    6    7    8    9
char Disp7SegCC[] = {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};
char Contador = 0;
char Opcion, Mod = 0;

//------------------------------------------------------------------------------
//Funci�n main()
void main(void)
{
    //Configuraci�n inicial
    SetUp();

    PrintTwoDisp(Contador);
    while(1)
    {
        //Se lee la opcion marcada por los interruptores A y B
        Opcion = (Opcion | InB) << 1;
        Opcion = Opcion | InA;

        switch(Opcion)
        {
            case 0:
                if(Contador == 99)
                    Contador = 0;
                else
                    Contador = Contador + 1;
                break;
            case 1:
                if(Contador == 0)
                    Contador = 99;
                else
                    Contador = Contador - 1;
                break;
            case 2:
                if(Contador >= 98)
                    Contador = 0;
                else
                {
                    Mod = Contador%2;
                    if(Mod == 0)
                        Contador = Contador + 2;
                    else
                        Contador = Contador + 1;
                }
                break;
            case 3:
                if(Contador <= 1)
                    Contador = 99;
                else
                {
                    Mod = Contador%2;
                    if(Mod == 0)
                        Contador = Contador - 1;
                    else
                        Contador = Contador - 2;
                }
                break;
            default:
                break;
        }
        Opcion = 0;
        PrintTwoDisp(Contador);
    }
    return;
}

//------------------------------------------------------------------------------
//Desarrollo funci�n SetUp()
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados

    //Configuraci�n puertos E/S
    TRISBbits.TRISB0 = 0; //RB2 como salida para activar el Display Unidades
    TRISBbits.TRISB1 = 0; //RB3 como salida para activar el Display Decenas
    TRISBbits.TRISB4 = 1; //RB0 como entrada para el interruptor A
    TRISBbits.TRISB5 = 1; //RB1 como entrada para el interruptor B
    TRISD = 0x00; //Todas las l�neas del puerto D como salidas

    return;
}

//------------------------------------------------------------------------------
//Desarrollo funci�n PrintTwoDisp
//Esta funci�n se encarga de visualizar en dos display 7 segmentos (multiplexados)
//un valor n�merico de 0 a 99 el cual se encuentra en la variable Valor
void PrintTwoDisp(unsigned char Valor)
{
    unsigned char Unidades = 0;
    unsigned char Decenas = 0;

    if(Valor > 9)
    {
        Unidades = Valor%10;
        Decenas = Valor/10;
    }
    else
    {
        Decenas = 0;
        Unidades = Valor;
    }

    for(char n = 0; n < 31; n++) //Aprox. se muestra un valor en los dos display por medio segundo
    {
        DISP_UNIDADES = 1;
        DISP_DECENAS = 0;
        DISP(Unidades);
        __delay_ms(8);

        DISP_UNIDADES = 0;
        DISP_DECENAS = 1;
        DISP(Decenas);
        __delay_ms(8);
    }
    return;
}
//------------------------------------------------------------------------------