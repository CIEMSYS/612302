/*
 * File:   main.c
 * Author: Sebastián
 *
 * Created on 29 de octubre de 2015, 03:28 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"

void SetupTimer0(void);

unsigned char Contador = 0;

void main(void)
{
    OSCCONbits.IRCF = 0b111;    //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111;   //Canales analógicos desactivados
    TRISD = 0x00;   //todas las líneas del puerto D como salidas digitales
    
    //Configuración módulos que van a manerjar interrupción
    //en este caso el Timer0
    SetupTimer0();
    
    //Configuración fuentes de interrupción
    RCONbits.IPEN = 1;  //Sistema de prioridad de interrupciones habilitado
    INTCONbits.GIEH = 1;    //Se habilitan globalmente las interrupciones de prioridad alta
    INTCONbits.GIEL = 1;    //Se habilitan globalmente las interrupciones de prioridad baja
      
    //Habilitación a nivel individual de cada una de las fuentes de interrupción
    INTCONbits.INT0IE = 1;  //Se habilita la interrupcion externa 0 (cambio por flanco en RB0)
    INTCON2bits.INTEDG0 = 0; //La interrupción externa 0 se dispara por flanco de bajada
    INTCONbits.TMR0IE = 1;  //Se habilita la interrupción por desbordamiento del Timer0
    
    //Configuración de la prioridad de las fuentes de interrupciones
    INTCON2bits.TMR0IP = 0; //Prioridad baja para la interrupción por desbordamiento del Timer0
    //La interrupción externa 0 (cuando se maneja prioridad) siempre tendrá prioridad alta 
    
    //Programa
    while(1)
    {
        PORTD = Contador;
    }
    
    return;
}
//------------------------------------------------------------------------------
void SetupTimer0(void)
{
    //Configuración Timer0
    //Ciclo de instrucción = Tcy = 4/8000000 = 0.5 useg
    T0CONbits.T0CS = 0; //Timer0 en modo temporizador
    T0CONbits.T08BIT = 0; //Timer0 a 16 bits
    T0CONbits.PSA = 0; //Habilitamos el  pre-escalar
    T0CONbits.T0PS = 0b110; //Pre-esacalar 1:128
    //Valor Inicial Timer0
    TMR0L = 0xF7; //Valor inicial calculado para el Timer0 (49911)
    TMR0H = 0xC2;
    T0CONbits.TMR0ON = 1; //Timer0 habilitado
    
    return;
}
//------------------------------------------------------------------------------
//Atencion a todas las interrupciones de prioridad alta habilitadas
void high_priority interrupt InterrupcionesAltas()
{
    if (INTCONbits.INT0IF == 1) //A través de las diferentes banderas de las fuentes de interrupción se puede averiguar la causante de la interrupcion
    {
        INTCONbits.INT0IF = 0; //Despues de identificada la interrupción de prioridad alta se debe borrar su bandera respectiva
        Contador = 0;
    }
    else
        Nop();
}
//------------------------------------------------------------------------------
//Atencion a todas las interrupciones de prioridad baja habilitadas
void low_priority interrupt InterrupcionesBajas()
{
    if(INTCONbits.TMR0IF == 1)
    {
        INTCONbits.TMR0IF = 0; //Borramos la bandera
        
        Contador++;
        //Valor Inicial Timer0
        TMR0L = 0xF7; //Valor inicial calculado para el Timer0 (49911)
        TMR0H = 0xC2;
    }
    else
        Nop();
}
//------------------------------------------------------------------------------
//Sebastián Fernando Puente Reyes, M.Sc.
//Universidad de Los Llanos
//------------------------------------------------------------------------------