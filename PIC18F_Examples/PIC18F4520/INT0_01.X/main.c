/*
 * File:   main.c
 * Author: Sebastián
 *
 * Created on 10 de mayo de 2016, 03:19 PM
 */


#include <xc.h>
#include "BitsConfiguracion.h"

#define LED PORTDbits.RD0

void main(void)
{
    OSCCONbits.IRCF = 0b110; //oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //deshabilitados canales analógicos
    
    //Configuración interrupciones
    RCONbits.IPEN = 0; //sistema de prioridad de interrupciones deshabilitado
    INTCONbits.INT0IE = 1; //habilitacion interrupción externa 0
    INTCON2bits.INTEDG0 = 1; //interrupción externa 0 por flanco de subida
    
    INTCONbits.GIE = 1; //Interrupciones habilitadas globalmente
    
    //Congiguraciónes puertos digitales
    TRISDbits.RD0 =  0;
    
    
    //Programa principal
    LED = 0;
    while(1)
    {
        //AQUI VA SU PROGRAMA
        
    }
}

//Subrutina para atender las interrupciones habilitadas
void interrupt AtencionInterrupciones()
{
    if(INTCONbits.INT0IF == 1) //inter. externa 0 causó la interrupción?
    {
        INTCONbits.INT0IF = 0;
        LED = !LED;
    }
}
