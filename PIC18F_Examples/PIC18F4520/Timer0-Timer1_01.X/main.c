/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 22 de abril de 2015, 10:03 PM
 */

//******************************************************************************
//Librerias
#include <xc.h>
#include "BitsConfiguracion.h"

//******************************************************************************
//Macros
#define ONDA_CUADRADA PORTBbits.RB0

//******************************************************************************
//Prototipos de funciones
void setup(void);
void DelayTimer0(unsigned int ValorTimer0);
void DelayTimer1(unsigned int ValorTimer1);

//******************************************************************************
//Funci�n Principal
void main(void)
{
    setup();

    while(1) //Ciclo infinito
    {
        //Creamos la se�al digital con T = 800 mseg con ciclo de trabajo de 75%
        ONDA_CUADRADA = 1; //Ponemos en alto la l�nea RB0
        DelayTimer0(28036); //Esperamos 600 mseg
        ONDA_CUADRADA = 0; //Ponemos en bajo la l�nea RB0
        DelayTimer1(0); //Esperamos 200 mseg
    }
    return;
}
//******************************************************************************
//Funci�n setup
void setup(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados
    TRISBbits.TRISB0 = 0; //L�nea RB0 como salida digital

    //Configuraci�n Timer0
    //Ciclo de instrucci�n = Tcy = 4/4000000 = 1 useg
    T0CONbits.T0CS = 0; //Timer0 en modo temporizador (reloj interno Fosc/4)
    T0CONbits.T08BIT = 0; //Timer0 a 16 bits
    T0CONbits.PSA = 0; //Habilitamos el  pre-escalar
    T0CONbits.T0PS = 0b011; //Pre-esacalar 1:16
    T0CONbits.TMR0ON = 1; //Timer0 habilitado

    //Configuraci�n Timer1
    T1CONbits.TMR1CS = 0; //Timer1 en modo temporizador (reloj interno Fosc/4)
    T1CONbits.RD16 = 1; //Timer1 a 16 bits
    T1CONbits.T1CKPS = 0b10; //Pre-escalar 1:4
    T1CONbits.TMR1ON = 1; //Timer1 habilitado
 
    return;
}

//******************************************************************************
//Funci�n DelayTimer0
void DelayTimer0(unsigned int ValorTimer0)
{
    //Esta funci�n carga el Timer0 con el valor adecuado (0 - 65535) para el
    //tiempo de desbordamiento deseado
    INTCONbits.TMR0IF = 0; //Borramos la bandera de desbordamiento del Timer0
    //Escribimos los 16 bits del Timer0. Siempre se debe escribir primero TMR0H
    TMR0H = ValorTimer0>>8;
    TMR0L = ValorTimer0;
    while(INTCONbits.TMR0IF == 0); //Esperamos a que la bandera TMR0IF sea 1
    return;
}
//******************************************************************************
//Funci�n delay
void DelayTimer1(unsigned int ValorTimer1)
{
    //Esta funci�n carga el Timer0 con el valor adecuado (0 - 65535) para el
    //tiempo de desbordamiento deseado
    PIR1bits.TMR1IF = 0; //Borramos la bandera de desbordamiento del Timer1
    //Escribimos los 16 bits del Timer0. Siempre se debe escribir primero TMR0H
    TMR1H = ValorTimer1>>8;
    TMR1L = ValorTimer1;
    while(PIR1bits.TMR1IF == 0); //Esperamos a que la bandera TMR0IF sea 1
    return;
}
//******************************************************************************
//Sebasti�n Puente Reyes, M.Sc.
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************