/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 15 de mayo de 2015, 01:30 PM
 */

#include <xc.h>
#include "BitsConfiguracion.h"
#include "KeyPad.h"

//******************************************************************************
//Prototipos de funciones
void SetUp(void);

//******************************************************************************
//Variables globales
char TeclaOprimida = 0;

//******************************************************************************
//Funci�n principal
void main(void)
{
    SetUp();

    while(1)
    {
        TeclaOprimida = GetKey();
        PORTD = TeclaOprimida;
    }

    return;
}

//******************************************************************************
//Funci�n SetUp
void SetUp()
{
    OSCCONbits.IRCF = 0b111;
    ADCON1bits.PCFG = 0b1111;
    TRISD = 0x00;
    PORTD = 0;
}
//******************************************************************************
//Programaci�n en C de Microcontroladores PIC de 8 bits con MPLAB XC8
//Sebasti�n Puente Reyes, M.Sc.
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************