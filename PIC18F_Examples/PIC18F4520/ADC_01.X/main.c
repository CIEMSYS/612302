/*******************************************************************************
 * File:   main.c
 * Author: Sebastian Puente R
 * e-mail: sebastian.puente@unillanos.edu.co
 * Universidad de los Llanos
 * Villavicencio-Meta, Colombia
 * Created on 11 de mayo de 2015, 05:41 PM
 ******************************************************************************
 * DESCRIPCI�N: ConversorAD_01
 * Oscilador: Interno a 8 MHz.
 * Se realiza la conversion a digital (10 bits) del canal AN0 y posteriormente
 * se visualiza en las l�neas RE1-RE0-RD7-RD6-RD5-RD4-RD3-RD2-RD1-RD0.
 * Los voltajes de referencia para el conversor son:
 * Vref+ = 5V (VDD), Vref- = 0V (Vss). ?Vref = 5V (seg�n datasheet min 3V)
 ******************************************************************************/

#include <xc.h>
#include "BitsConfiguracion.h"

//******************************************************************************
//Prototipos de funciones
void SetUp(void);
void SetUp_ADC(void);

//******************************************************************************
//Funci�n principal (main)
void main(void)
{
    //Variable entera para guardar el resultado de la conversi�n A/D
    unsigned int ResultadoConversion = 0;

    SetUp(); //Configuraci�n inicial
    SetUp_ADC(); //Configuraci�n ADC

    while(1) //Bucle infinito: lo que esta dentro de los parentesis del while se ejecuta indefinidamente
    {
        //Selecci�n del canal a convertir
        ADCON0bits.CHS = 0; //Se selecciona el canal AN0 para la conversi�n A/D
        //Inicio de la conversi�n
        ADCON0bits.GO = 1; //Conversi�n A/D en marcha
        //Bucle de espera de final de la conversi�n A/D
        while(ADCON0bits.DONE); //Comprobaci�n del bit GO/DONE hasta que se ponga a '0'
        //Lectura del resultado de la conversi�n de los resgistros ADRESH y ADRESL
        ResultadoConversion = (((unsigned int)ADRESH)<<8)|(ADRESL);

        //Visualizaci�n de los 10 bits del resultado de la conversi�n
        PORTD = ResultadoConversion;
        PORTE = ResultadoConversion >> 8;
    }

    return;
}
//******************************************************************************
//Desarrollo funci�n SetUp(). Configura Fosc y puertos
void SetUp()
{
    OSCCONbits.IRCF = 0b111;    //Oscilador interno a 8 MHz
    TRISD = 0;                  //L�neas puerto D como salidas digitales
    TRISEbits.TRISE0 = 0;       //L�nea RE0 como salida digital
    TRISEbits.TRISE1 = 0;       //L�nea RE1 como salida digital
    return;
}
//******************************************************************************
//Desarrollo funci�n SetUp_ADC(). Configura el conversor A/D
void SetUp_ADC()
{
    //Configuraci�n canales anal�gicos a utilizar
    ADCON1bits.PCFG = 0b1110; //Se trabajar� con el canal AN0(pin 2) los demas canales quedan deshabilitados y se podran trabajar como digitales
    //Configuraci�n  tensi�n de referencia
    ADCON1bits.VCFG = 0b00; //Vref- = Vss, Vref+ = Vdd (0v - 5v)
    //Configuraci�n reloj de conversi�n TAD
    ADCON2bits.ADCS = 0b001; //8*Tosc = 1us (TAD debe ser m�nimo 0,7us)
    //Configuraci�n del tiempo de adquisici�n
    ADCON2bits.ACQT = 0b001; //TACQ = 2TAD = 2us (seg�n Datasheet TACQ min debe ser 1,4us)
    //Configuraci�n del modo de almacenamiento de la conversi�n
    ADCON2bits.ADFM = 1; //Justificado a derechas
    //Activaci�n del conversor
    ADCON0bits.ADON = 1; //Conversor A/D activado

    return;
}
//******************************************************************************
//Sebasti�n Puente Reyes, M.Sc.
//Facultad de Cienc�as B�sicas e Ingenier�a
//Universidad de los Llanos
//******************************************************************************

