/*
 * File:   main.c
 * Author: Sebastián Puente R, M.Sc.
 *
 * Created on 3 de marzo de 2015, 11:48 PM
 */

#include <xc.h> //Libreria siempre necesaria, ya que posee la información del microcontrolador
#include "BitsConfiguracion.h" //Libreria con los Bits de Configuración

void main(void) //Función principal en C
{
    //Configuramos la frecuencia del oscilador interno
    OSCCONbits.IRCF = 0b110; //Oscilador interno a 4MHz

    //Configuramos los puertos digitales
    TRISBbits.TRISB0 = 0; //Línea RB0 del puerto B como salida digital

    while(1) //Bucle infinito
    {
        PORTBbits.RB0 = 1; //Sacamos un ALTO (1) por la línea RB0
    }

    return;
}