/*
 * File:   main.c
 * Author: Sebastian Puente R
 *
 * Created on 13 de marzo de 2015, 07:14 AM
 */

//Directivas de prepocesamiento
#include <xc.h>
#include "BitsConfiguracion.h"

//Prototipos de funciones
void SetUp();

//Variables globales
//Arreglo para el manejo de un display 7 segmentos de c�todo com�n
//                    0    1    2    3    4    5    6    7    8    9
char Disp7SegCC[] = {0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F};

//Funci�n main()
void main(void)
{
    SetUp(); //llamadoa a la funci�n SetUp()

    //bucle infinito
    while(1)
    {
        //Para este caso el display mostrar� n�meros del 0 al 9
        if(PORTB < 10)
        {
            PORTD = Disp7SegCC[PORTB];
        }
        else //para >= a 10 no se muestra nada en el display
        {
            PORTD = 0x00; //se apagan todos los segmentos del display
        }
    }

    return;
}


//Desarrollo funci�n SetUp()
void SetUp(void)
{
    //Configuraci�n frecuencia oscilador interno
    OSCCONbits.IRCF = 0b111; //Oscilador interno a 8 MHz
    ADCON1bits.PCFG = 0b1111; //Canales anal�gicos desactivados

    //Configuraci�n puertos E/S
    TRISB = 0xFF; //Todas las l�neas del puerto B como entradas digitales
    TRISD = 0x00; //Todas las l�neas del puerto D como salidas digitales

    return;
}