/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     ProgrammedIO_01
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           I/O Ports - Unconditional Programmed I/O
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.00
 * Author:          Sebastian Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Marzo de 2018
 *******************************************************************************
 * REQUERIMIENTO
 * Device Tool: Curiosity HPC
 * Oscilador: Interno a 16 MHz, Fosc = 16 MHz.
 * Encender el LED D2 conectado en RA4.
 * -Ver Descripcion.txt
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h>
#include <pic18f45k22.h>
#include "ConfigurationBits.h"

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define D2 PORTAbits.RA4

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/

/*******************************************************************************
 * Variables globales
 ******************************************************************************/

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    //--Configuraciones Iniciales

    //Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)
    //Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b111; //HFINTOSC = 16 MHz, Fosc = 16 MHz

    //Configuraci�n I/O Ports (Capitulo 10 I/O Ports DataSheet)
    LATA = 0; //Inicializar PORTA, tambien es v�lido: PORTA = 0;
    TRISAbits.TRISA4 = 0; //L�nea RA4 como salida

    //Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while(1)
    {
        //Encender el LED D2 conectado en RA4
        D2 = 1; //Nivel alto por RA4
    }
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebastian Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Ciencías B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/