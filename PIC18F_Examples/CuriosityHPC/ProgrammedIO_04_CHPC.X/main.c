/*******************************************************************************
 * FileName:        main.c
 * ProjectName:     ProgrammedIO_04 
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           I/O Ports - Unconditional Programmed I/O
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.00
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Octubre de 2018
 *******************************************************************************
 * REQUERIMIENTO
 * Oscilador: Interno a 8 MHz, Fosc = 32 MHz (Habilitar PLL)
 * Realizar un contador binario de 4 bits, 0 - 15, que se visualize a trav�s
 * de las 4 l�neas m�s significativas del Puerto A (RA<7:4>).
 * El incremento debe ser cada 500 mseg.
 * -Ver Descripci�n.txt
 ******************************************************************************/

/*******************************************************************************
 * Librerias
 ******************************************************************************/
#include <xc.h> //Lib con informaci�n del MCU
#include <stdint.h> //Lib est�ndar para declaraci�n de variables enteras
#include "ConfigurationBits.h" //Bits de configuraci�n

/*******************************************************************************
 * Macros
 ******************************************************************************/
#define _XTAL_FREQ 32000000 //Macro necesario para __delay_ms()

/*******************************************************************************
 * Prototipos de funciones
 ******************************************************************************/
void SetUp(void);

/*******************************************************************************
 * Variables globales
 ******************************************************************************/
uint8_t u8_Contador = 0; //Variable tipo entero sin signo de 8 bits

/*******************************************************************************
 * Funci�n Principal
 ******************************************************************************/
void main(void)
{
    uint8_t u8_i; //Variable tipo entero sin signo de 8 bits usada por el for

    //--Configutaciones Iniciales
    SetUp();

    //--Ciclo Infinito, Tareas que el MCU hace indefinidamente
    while(1)
    {
        for(u8_i = 0; u8_i <= 15; u8_i++)
        {
            u8_Contador = u8_i;
            //Llevar los 8 bits de u8_Contador a las 8 l�neas del PORTA
            u8_Contador = u8_Contador << 4; //Despl. de 4 bits a la izquierda
            PORTA = u8_Contador; 
            __delay_ms(500); //Retardo de medio segundo (500 mseg)
        }
    }
    
    return;
}

/*******************************************************************************
 * FUNCI�N:     SetUp()
 * ENTRADAS:    Ninguna
 * SALIDAS:     Ninguna
 * DESCRIPCI�N: Configuraci�n inicial (oscilador, puertos, etc.)
 ******************************************************************************/
void SetUp(void)
{
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b110; //HFINTOSC = 8 MHz, Fosc = 8 MHz

    //--A.2--Habilitaci�n PLL
    OSCTUNEbits.PLLEN = 1; //Fosc = 4 x 8 MHz = 32 MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports
    LATA = 0; //Inicializar PORTA

    //--B.2--Sentido I/O Ports
    TRISA = 0x0F; //RA<7:4> como salidas y RA<3:0> como entradas digitales
    
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebasti�n Puente Reyes, M.Sc.
 * Grupo de Estudio en Hardware Reconfigurable y Sistemas Embebidos - GEHRSE
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/