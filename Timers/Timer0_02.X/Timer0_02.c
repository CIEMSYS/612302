/*******************************************************************************
 * FileName:        Timer0_02.c
 * ProjectName:     Timer0_01
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           Timer0 Module - Timer0 Test
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.05
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Marzo de 2019
 * *****************************************************************************
 * REQUERIMIENTO
 * Reloj Principal: Fosc = 4 MHz Osc. interno.
 * Se genera una se�al cuadrada por la l�nea RD0 con un periodo de 800 mseg ()
 * con un ciclo de trabajo del 75%. Es decir, la se�al se mantiene en estado
 * ALTO por 600 mseg(0.75*800) y luego pasa a estado BAJO por 200 mseg(0.25*800)
 *  ______________            __________
 * |              |          |          |
 * |              |__________|          |........
 * <---600mseg---> <-200mseg->
 * <------ 800 mseg --------->
 * 
 * Fosc = Frecuencia de trabajo del microcontrolador = 4 MHz
 * Tosc = Periodo trabajo del microcontrolador = 0.25e-6 seg
 * Tcy = Ciclo de instrucci�n = 4*Tosc = 4*(0.25e-6 seg) = 1e-6 seg = 1 useg
 * Td = Tiempo desbordamiento = (Preescaler)(Tcy)(256 - ValorInicialTimer0)
 * Prescaler = 16
 * 
 * Para lograr un tiempo de desbordamiento de 600 mseg el valor inicial para el
 * Timer0 es de: 28036
 * Para lograr un tiempo de desbordamiento de 200 mseg el valor inicial para el
 * Timer0 es de: 53036
 * 
 * Se realiza una funci�n encargada de cargar el Timer0 con dichos valores
 * (el valor debe se entrega como parametro al llamar la funci�n) y esperar el
 * desbordamiento.
 * *****************************************************************************
 */

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>
#include <stdint.h>
#include "ConfigurationBits.h"

/**
 * Secci�n: Macros (definiciones)
 */
#define ONDA    (LATDbits.LD0) // Definici�n para la l�nea RD0

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * @Resumen
 *  A. Configuraci�n Oscilador
 *  B. Configuraci�n Puertos I/O
 */
void SysInitialize(void);
/**
 * @Resumen
 *  Configuraci�n inicial del Timer0
 */
void TMR0_Initialize(void);
/**
 * @Resumen
 *  Carga el Timer0 con el valor adecuado (0-65535)
 *  para el tiempo de desbordamiento deseado
 * @param ValorTimer0: Valor a cargar
 */
void DelayTimer0(uint16_t ValorTimer0);

/**
 * Secci�n: Funci�n Principal
 */
void main(void)
{
    SysInitialize();
    TMR0_Initialize();
    
    while(1) //Ciclo infinito
    {
        //Creamos la se�al digital con T = 800 mseg con ciclo de trabajo de 75%
        ONDA = 1; //Ponemos en alto la l�nea RD0
        DelayTimer0(28036); //Esperamos 600 mseg
        ONDA = 0; //Ponemos en bajo la l�nea RD0
        DelayTimer0(53036); //Esperamos 200 mseg
    }
    return;
}

/**
 * Secci�n: Desarrollo de Funciones
 */

void SysInitialize(void)
{   
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b101; //HFINTOSC = 4 MHz, Fosc = 4 MHz
    
    //--A.2--Habilitaci�n PLL
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports
    LATD = 0; //Inicializar PORTD
 
    /**
     * --B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * ANSELX: PORTX Analog Select Register
     * Necesario cuando se configura dichos pines como entradas digitales
     */
    
    //--B.3--Sentido I/O Ports
    TRISDbits.TRISD0 = 0; //L�nea RD0 como salida digital

    return;
}

void TMR0_Initialize(void)
{
    //Ciclo de instrucci�n = Tcy = 4/4000000 = 1 useg
    T0CONbits.T0CS = 0;     //Modo temporizador
    T0CONbits.T08BIT = 0;   //Timer0 a 16 bits
    T0CONbits.PSA = 0;      //Habilitaci�n pre-escalar
    T0CONbits.T0PS = 0b011; //Pre-esacalar 1:16
    T0CONbits.TMR0ON = 1;   //Timer0 habilitado
    
    return;
}

void DelayTimer0(uint16_t ValorTimer0)
{
    INTCONbits.TMR0IF = 0; //Borramos la bandera de desbordamiento del Timer0
    //Escribir los 16 bits del Timer0. Siempre se debe escribir primero TMR0H
    TMR0H = ValorTimer0>>8;
    TMR0L = ValorTimer0;
    //Esperar a que TMR0IF sea 1, es decir a que se desborde el Timer0 
    while(INTCONbits.TMR0IF == 0);
    
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebast�n Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/