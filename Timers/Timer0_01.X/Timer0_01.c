/*******************************************************************************
 * FileName:        Timer0_01.c
 * ProjectName:     Timer0_01
 * Course:          Dise�o Digital con Microcontroladores PIC de 8 bits
 * Topic:           Timer0 Module - Timer0 Test
 * Dependencies:    See INCLUDES section below
 * Processor:       PIC18F45K22
 * Compiler:        XC8, Ver. 2.05
 * Author:          Sebasti�n Fernando Puente Reyes
 * e-mail:          sebastian.puente@unillanos.edu.co
 * Date:            Marzo de 2019
 * *****************************************************************************
 * REQUERIMIENTO
 * Reloj Principal: Fosc = 4 MHz Osc. interno.
 * Se realiza una temporizaci�n de 5 mseg a traves del desbordamiento del Timer0.
 * En este ejemplo se genera una se�al cuadrada por el pin RD0 con un periodo de
 * 10 mseg, es decir una frecuencia de 100 Hz.
 *  __________            __________
 * |          |          |          |
 * |          |__________|          |........
 * <--5mseg--> <--5mseg-->
 * <----- 10 mseg ------->
 * 
 * Fosc = Frecuencia de trabajo del microcontrolador = 4 MHz
 * Tosc = Periodo trabajo del microcontrolador = 0.25e-6 seg
 * Tcy = Ciclo de instrucci�n = 4*Tosc = 4*(0.25e-6 seg) = 1e-6 seg = 1 useg
 * Td = Tiempo desbordamiento = (Pre-escalar)(Tcy)(256 - ValorInicialTimer0)
 * Td = 5 mseg
 * El pre-escalar ser� de 32.
 * Despejando ValorInicialTimer0.
 * ValorInicialTimer0 = 100
 * -Ver Descripcion.txt
 * *****************************************************************************
 */

/**
 * Secci�n: Archivos Incluidos
 */

#include <xc.h>
#include "ConfigurationBits.h"

/**
 * Secci�n: Prototipos de Funciones
 */

/**
 * @Resumen
 *  A. Configuraci�n Oscilador
 *  B. Configuraci�n Puertos I/O
 */
void SysInitialize(void);
/**
 * @Resumen
 *  Configuraci�n inicial del Timer0
 */
void TMR0_Initialize(void);
/**
 * @Resumen
 *  Retardo de 5 mseg usando el Timer0
 */
void delay_5ms(void);

/**
 * Secci�n: Funci�n Principal
 */
void main(void)
{
    SysInitialize(); //Configuraci�n inicial
    TMR0_Initialize(); //Se configura el modulo Timer0

    //Generaci�n de la se�al cuadrada con un periodo de 10 mseg por la l�nea RD0
    while(1)
    {
        //Se pone en alto la l�nea RD0 durante 5 mseg
        LATDbits.LD0 = 1;
        delay_5ms();
        //Se pone en bajo la l�nea RD0 durante 5 mseg
        LATDbits.LD0 = 0;
        delay_5ms();
    }
}

/**
 * Secci�n: Desarrollo de Funciones
 */

void SysInitialize(void)
{   
    //---A---Configuraci�n Oscilador (Capitulo 2 DataSheet: Oscillator Module)

    //--A.1--Configuraci�n oscilador interno
    OSCCONbits.IRCF = 0b101; //HFINTOSC = 4 MHz, Fosc = 4 MHz
    
    //--A.2--Habilitaci�n PLL
    //OSCTUNEbits.PLLEN = 1; //Fosc = 4 x XX MHz = YY MHz

    //---B---Configuraci�n I/O Ports (Capitulo 10 DataSheet: I/O Ports)

    //--B.1--Inicializaci�n I/O Ports
    LATD = 0; //Inicializar PORTD
 
    /**
     * --B.2--Habilitaci�n buffer entrada digital para las lineas digitales/anal�gicas
     * (Seccion 10.7 Datasheet: Port Analog Control)
     * ANSELX: PORTX Analog Select Register
     * Necesario cuando se configura dichos pines como entradas digitales
     */
    
    //--B.3--Sentido I/O Ports
    TRISDbits.TRISD0 = 0; //L�nea RD0 como salida digital

    return;
}

void TMR0_Initialize(void)
{
    T0CONbits.T0PS = 0b100; //Pre-escalar = 32
    T0CONbits.PSA = 0; //Pre-escalar activado
    T0CONbits.T0CS = 0; //Modo temporizador (reloj: ciclo de instrucci�n)
    T0CONbits.T08BIT = 1; //Modo 8 bits
    
    return;
}

void delay_5ms(void)
{
    TMR0L = 100; //Valor inicial del registro TMR0
    INTCONbits.TMR0IF = 0; //Borramos el bit que nos indica el desbordamiento
    T0CONbits.TMR0ON = 1; //Iniciamos el modulo Timer0
    /* Mientras el bit TMR0IF sea 0 significa que a�n no se desborda el TMR0.
     * Luego, debemos esperar a que se ponga en 1 (es decir que pasen los 5ms */
    while(INTCONbits.TMR0IF == 0);
    T0CONbits.TMR0ON = 0; //Desactivamos el modulo Timer0
    
    return;
}

/*******************************************************************************
 * DISE�O DIGITAL CON MICROCONTROLADORES PIC DE 8 BITS
 * Sebast�n Puente Reyes, M.Sc.
 * - CIEMSYS -
 * Semillero de Investigaci�n en Inteligencia Computacional y Sistemas Embebidos
 * Escuela de Ingenier�a
 * Facultad de Cienc�as B�sicas e Ingenier�a
 * Universidad de los Llanos
 * Villavicencio - Meta, Colombia
 ******************************************************************************/